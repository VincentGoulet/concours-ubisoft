﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Remote_Alert : MonoBehaviour {
    Player_Movement player_movement;
    Water_Controller water;
    Image alert;
    Game_Controller game_controller;

	// Use this for initialization
	void Awake () {
        player_movement = GameObject.Find("Player").GetComponent<Player_Movement>();
        game_controller = GameObject.Find("Game Controller").GetComponent<Game_Controller>();
        water = GameObject.Find("Water").GetComponent<Water_Controller>();
        alert = GetComponent<Image>();
        Utilities.Set_Transparency(alert, 0);
    }
	
	// Update is called once per frame
	void Update () { 
        if (!game_controller.Has_Fruit())
        {
            if (player_movement.has_remote)
            {
                if (player_movement.transform.position.y < water.high.y)
                {
                    if (!player_movement.is_on_raft)
                    {
                        Utilities.Set_Transparency(alert, 1);
                    }
                    else
                    {
                        Utilities.Set_Transparency(alert, 0);
                    }
                }
                else if (player_movement.transform.position.y > water.high.y + .2f)
                {
                    Utilities.Set_Transparency(alert, 0);
                }
            }
        }
        else
        {
            Utilities.Set_Transparency(alert, 0);
        }
    }
}
