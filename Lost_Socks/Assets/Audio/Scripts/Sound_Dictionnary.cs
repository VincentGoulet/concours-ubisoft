﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class Sound_Dictionnary : MonoBehaviour {
    public Dictionary<string, AudioClip> sound_dic;
    public AudioClip[] footsteps;
    public AudioClip[] player_interactions;
    public AudioClip[] drop_textures;
    public AudioClip[] mecanics;
    public AudioClip[] hammer_textures;
    public AudioClip[] socks_sounds;
    public AudioClip[] bird_sounds;
    public AudioClip[] music;

    public Dictionary<string, AudioClip> Mec_dict = new Dictionary<string, AudioClip>();

    private void makeDictWArray(AudioClip[] soundErray,Dictionary<string,AudioClip> p_dicto )
    {
        int arrayLength = soundErray.Length;
        for(int j = 0; j < arrayLength; j++)
        {
             string str = soundErray[j].ToString();
             AudioClip clip = soundErray[j];
             str.Trim();
             print(str);
             p_dicto.Add(str, clip);
        }

    }

}

