using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    //Pitch variables
    private float min_pitch = 0.90f;
    private float max_pitch = 1.15f;
    //Main variables
    private Sound_Dictionnary sound_dicto;
    private Object_Dictionary obj_dicto;


    #region Audioclip variables
    private AudioClip _spray;
    private AudioClip _jump;
    private AudioClip _drop;
    private AudioClip _eating;
    private AudioClip _swing_hammer;
    private AudioClip _hit_hammer;
    private AudioClip _rock_textures;
    private AudioClip _get_socks;
    private AudioClip _acquire_obj;
    private AudioClip _get_music;
    private AudioClip _compo_principal;
    private AudioClip[] _fstepsR;

    public enum collide_object { ground, raft, washer, rock};
    public enum collide_hammer { ground=1, washer, water, tree }
    

    #endregion
    #region AudioSource variables
    private AudioSource player_source;
    
    private AudioSource spray_source;
    private AudioSource geyser_source;
    private AudioSource ambience_source;
    private AudioSource washers_source;
    private AudioSource music_source;
    #endregion
    #region Mixer variables
    public AudioMixer Spray_mixer;
    public string spray_track;

    public AudioMixer footsteps_mixer;
    public string footsteps_track;

    public AudioMixer geyser_mixer;
    public AudioMixerSnapshot[] geyser_pressure;
    public string geyser_track;

    public AudioMixer manette_mixer;
    public AudioMixerSnapshot[] beep_fader;

    public AudioMixer fruit_mixer;

    public AudioMixer hammer_mixer;

    public AudioMixer socks_mixer;

    public AudioMixer bird_mixer;

    public AudioMixer Ambience_mixer;
    public AudioMixerSnapshot Ocean_up;
    public AudioMixerSnapshot Ocean_down;
    public AudioMixerSnapshot Ocean_default;

    public AudioMixer music_mixer;

    float[] j = new float[] { 0f, 1f };
    private string[] drop_mixers =  {"Drop", "Raft_drop", "Washer", "Rock"};
    private string[] mecanics_mixers = { "Spray", "Hammer", "Remote", "Jump" };
    float number;
    #endregion

    Animator anim_player;
    DoneHashIDs hashs;
    

    void Awake()
    {

        obj_dicto = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        sound_dicto = GameObject.Find("SoundManager").GetComponent<Sound_Dictionnary>();   
          
        
    }
    void Start()
    {
        //Get sources
        hashs = GameObject.Find("Game Controller").GetComponent<DoneHashIDs>();
        player_source = obj_dicto.Get_Player().GetComponent<AudioSource>();
        ambience_source = GameObject.Find("Ambience_Audiosource").GetComponent<AudioSource>();
        music_source = GameObject.Find("Music_Audiosource").GetComponent<AudioSource>();
        washers_source = GameObject.Find("Washers").GetComponent<AudioSource>();
        anim_player = obj_dicto.Get_Player().GetComponent<Animator>();
        if(ambience_source.ToString() == "null")
        {
            GameObject.Find("Ambience_Audiosource").AddComponent<AudioSource>();
        }
        if (music_source.ToString() == "null")
        {
            GameObject.Find("Music_Audiosource").AddComponent<AudioSource>();
        }
        if (player_source.ToString() == "null")
        {
            print("An audiosource was created automatically, call your audio integrator....................or else");
            AudioSource a;
            a = obj_dicto.Get_Player().AddComponent<AudioSource>();
            a.outputAudioMixerGroup = footsteps_mixer.outputAudioMixerGroup;
        }
        
        spray_source = obj_dicto.Get_Spray().GetComponent<AudioSource>();
        if (spray_source.ToString() == "null")
        {
            
            print("An audiosource was created automatically, call your audio integrator....................or else");
            AudioSource a;
            a = obj_dicto.Get_Spray().AddComponent<AudioSource>();
            a.outputAudioMixerGroup = Spray_mixer.outputAudioMixerGroup;
        }
        geyser_source = obj_dicto.Get_Geyser().GetComponent<AudioSource>();
        if (geyser_source.ToString() == "null")
        {
            print("An audiosource was created automatically, call your audio integrator....................or else");
            AudioSource a;
            a = obj_dicto.Get_Geyser().AddComponent<AudioSource>();
            a.outputAudioMixerGroup = geyser_mixer.outputAudioMixerGroup;
        }
        geyser_source.Stop();
        geyser_mixer.SetFloat("GeyserVol", -80.00f);

        //Get sounds
        _spray = sound_dicto.mecanics[0];
        _jump = sound_dicto.player_interactions[0];
        _swing_hammer = sound_dicto.mecanics[1];
        _hit_hammer = sound_dicto.mecanics[2];
        _rock_textures = sound_dicto.hammer_textures[0];
        _eating = sound_dicto.player_interactions[1];
        _fstepsR = sound_dicto.footsteps;
        _get_socks = sound_dicto.player_interactions[3];
        _acquire_obj = sound_dicto.player_interactions[4];
        _compo_principal = sound_dicto.music[0];
        _get_music = sound_dicto.music[1];
       
    }
   void Update()
    {
        number = Random.Range(1, 8);
        //Debug.Log(Mathf.Sin(5 * Time.time));
        Ambience_mixer.SetFloat("ConstOcVol", Mathf.Sin(Time.time * 3) * 3);
        
        if(Mathf.Sin(5 * Time.time) > 0)
        {
            Ambience_mixer.SetFloat("Current", Mathf.PingPong(5 * Time.time, 5f) * 5000);
        }
        
        
            
        


    }
    // Player sounds
    public void play_hammer_collide(int col_obj)
    {
        playSmpl(sound_dicto.hammer_textures[col_obj], player_source, hammer_mixer, "Textures", min_pitch, max_pitch, false);
    }
    public void play_rocks_destroy(AudioSource mysource)
    {
        playSmpl(_rock_textures, mysource, hammer_mixer, "Textures", min_pitch, max_pitch, true);
    }
    public void play_swing()
    {
        print(hammer_mixer);
        playSmpl(_swing_hammer, player_source, hammer_mixer, "Hammer", min_pitch, max_pitch, true);
    }
    public void play_hit()
    {
        playSmpl(_hit_hammer, player_source, hammer_mixer, "Hit", min_pitch, max_pitch, false);
    }
    public void play_eating()
    {
        playSmpl(_eating, player_source, fruit_mixer, "Eating", min_pitch, max_pitch, true);
    }
    public void play_jump()
    {
        playSmpl(_jump, player_source, footsteps_mixer,mecanics_mixers[3], min_pitch, max_pitch, false);
    }
    public void play_drop(int col_obj)
    {
        playSmpl(sound_dicto.drop_textures[col_obj], player_source, footsteps_mixer, drop_mixers[col_obj] , min_pitch, max_pitch, false);
    }
    public void play_spray()
    {
        
        playSmpl(_spray, spray_source,Spray_mixer,mecanics_mixers[0], 0.95f, 1.3f, false);
    }

    public void play_fstepsR()
    {
        if (hashs.speedFloat > 0) //&& Time.timeScale > 0.1f)
        {
            playRndSmpl(_fstepsR, player_source, footsteps_mixer, footsteps_track, min_pitch, max_pitch, false);
        }
            
        
        
    }
    public IEnumerator play_manette()
    {
        yield return new WaitForSeconds(0.3f);
        playSmpl(sound_dicto.mecanics[3], player_source, manette_mixer, "manettes", min_pitch, max_pitch, false);

        StopCoroutine(play_manette());
    }
    //Socks sounds
    public void play_socks_sound(AudioSource mysource, int state)
    {
        playSmpl(sound_dicto.socks_sounds[state], mysource, socks_mixer, "Socks", min_pitch, max_pitch, false);
    }


    // Ambient sounds

    public  IEnumerator play_geyser()
    {
        geyser_source.Play(0);
        
        float j= -17.70f;
        geyser_mixer.SetFloat("GeyserVol", j );
        geyser_mixer.TransitionToSnapshots(geyser_pressure, this.j, 5* Time.deltaTime);
        yield return new WaitForSeconds(5);
        this.j[0] = 1f;
        this.j[1] = 0f;
        geyser_mixer.TransitionToSnapshots(geyser_pressure, this.j, 2f);
        yield return new WaitForSeconds(2.3f);
        //print("Im here");
        geyser_source.Stop();
        geyser_mixer.SetFloat("GeyserVol", -80.00f);
        this.j[0] = 0f;
        this.j[1] = 1f;
    }

    public IEnumerator Ocean_Up(float speed)
    {
        Ocean_up.TransitionTo((speed*100)*Time.deltaTime);
        
        yield return new WaitForSeconds(2f);
        Ocean_default.TransitionTo((speed * 200) * Time.deltaTime);

    }
    public IEnumerator Ocean_Down(float speed)
    {
        Ocean_up.TransitionTo((speed * 100) * Time.deltaTime);
        
        yield return new WaitForSeconds(2f);
        Ocean_default.TransitionTo((speed * 200) * Time.deltaTime);


    }
    public void activate_washer()
    {
        AudioClip washer = sound_dicto.player_interactions[2];
        playSmpl(washer, washers_source, manette_mixer, "Washer", min_pitch, max_pitch, false);
    }
    public void play_getSock()
    {
        playSmpl(_get_socks, ambience_source, Ambience_mixer, "GetSocks", min_pitch, max_pitch, false);
    }
    public void play_getObj()
    {
        playSmpl(_acquire_obj, ambience_source, Ambience_mixer, "AcquireObject", min_pitch, max_pitch, false);
    }
    public IEnumerator play_music()
    {
        playSmpl(_get_music, music_source, music_mixer, "OtherMusic", 1, 1, false);
        yield return new WaitForSeconds(0.3f);
        playSmpl(_compo_principal, music_source, music_mixer, "Compo", 1, 1, false);
        music_source.loop = true;
    }
    public void play_playerHurt()
    {
        if (!player_source.isPlaying)
        {
            playSmpl(sound_dicto.player_interactions[GetRandomValue(5, 8)], player_source, footsteps_mixer, "Voice", min_pitch, max_pitch, false);
        }
      
    }
    public void play_playerDie()
    {
        if (!player_source.isPlaying)
        {
            playSmpl(sound_dicto.player_interactions[GetRandomValue(9, 11)], player_source, footsteps_mixer, "Voice", min_pitch, max_pitch, false);
        }
        
    }
    public void play_birdTakeSock(AudioSource mysource)
    {
        playSmpl(sound_dicto.bird_sounds[0], mysource, bird_mixer, "birds", min_pitch, max_pitch, false);
    }
    public void play_birdHurt(AudioSource mysource)
    {
        playSmpl(sound_dicto.bird_sounds[1], mysource, bird_mixer, "birds", min_pitch, max_pitch, false);
    }
    public void play_birdAttack(AudioSource mysource)
    {
        playSmpl(sound_dicto.bird_sounds[2], mysource, bird_mixer, "birds", min_pitch, max_pitch, false);
    }
    public IEnumerator play_birdMusic(GameObject gobject)
    {
        playSmpl(sound_dicto.music[2], music_source, music_mixer, "OtherMusic", 1, 1, false);
        music_source.loop = true;
        
        yield return new WaitWhile(() => gobject.GetComponent<StatePatternBird>().IsAttacking);
        playSmpl(_compo_principal, music_source, music_mixer, "Compo", 1, 1, false);
        music_source.loop = true;
    }
    public void play_explosion()
    {
        playSmpl(sound_dicto.player_interactions[12], ambience_source, Ambience_mixer, "Volcan", 1, 1, false);
    }
    public void play_vortex()
    {
        playSmpl(sound_dicto.player_interactions[13], ambience_source, Ambience_mixer, "Vortex", 0.8f, 0.8f, false);
        ambience_source.loop = true;
    }


    















    #region Utility methods
    private void playRndSmpl(AudioClip[] soundArray, AudioSource source, AudioMixer mixer, string mixername, float pitchmin, float pitchmax, bool is_pan)
    {
        int max = soundArray.Length;

        source.clip = soundArray[Random.Range(0, max)];
        source.outputAudioMixerGroup = mixer.FindMatchingGroups(mixername)[0];
        if (is_pan)
        {
            source.panStereo = Random.Range(-0.3f, 0.3f);
        }
        source.pitch = Random.Range(pitchmin, pitchmax);
        source.loop = false;
        source.Play();
        
    }
    private void playSmpl(AudioClip sample, AudioSource source,AudioMixer mixer, string mixername, float pitchmin, float pitchmax, bool is_panned)
    {
        source.clip = sample;
        if (is_panned)
        {
            source.panStereo = Random.Range(-0.3f, 0.3f);
        }
        source.outputAudioMixerGroup = mixer.FindMatchingGroups(mixername)[0];
        source.pitch = Random.Range(pitchmin, pitchmax);
        source.loop = false;
        source.Play();
    }
    public int GetRandomValue(int min, int max)
    {
        return Random.Range(min, max);
    }
    #endregion














































    #region old code
    /*
    public double waittime = 0;
    public double limit = 100;
    public bool Soundisplaying;
    public float ShotVolMin = 0.8f;
    public float ShotVolMax= 1.2f;
    public float FrequencyMin;
    public float FrequencyMax;
    public float timeBetweenBullets = 0.15f;

    public DarkArtsStudios.SoundGenerator.Composition GunshotPrefab;
    private DarkArtsStudios.SoundGenerator.Module.Output _toneOutput;

    private AudioClip Sound0;

    float timer;

    public AudioSource GunshotAudiosource;
    private AudioClip _gunshotAudioClip;
    private AudioClip _sound_clip;
    public Dictionary<int,AudioClip> DictoAudioClips = new Dictionary<int, AudioClip>();
    private int counter = 0;

    #region Generate sound method

    public void GenerateSound()
    {
        _toneOutput.attribute("Frequency").value = Random.Range(FrequencyMin, FrequencyMax);
        _toneOutput.Generate();
        _sound_clip = _toneOutput.audioClip;
        GunshotAudiosource.loop = false;
    }




    #endregion


    #region Unity Hooks

    void Start()
    {

        GunshotAudiosource = GetComponent<AudioSource>();
        // Retrieve toneOutput Module
        _toneOutput =GunshotPrefab.modules.Find(x => x.GetType() == typeof (DarkArtsStudios.SoundGenerator.Module.Output)) as DarkArtsStudios.SoundGenerator.Module.Output;

        GenerateSound();
        Sound0 = _gunshotAudioClip;
        DictoAudioClips.Add(counter,Sound0);



        GunshotAudiosource.clip = DictoAudioClips[Random.Range(0, DictoAudioClips.Count-1)];

    }




    void Update()
    {

        timer += Time.deltaTime;
        Soundisplaying = GunshotAudiosource.isPlaying;



        if (waittime > limit)
        {
            DictoAudioClips.Remove(counter);
            GenerateSound();
            Sound0 = _gunshotAudioClip;
            DictoAudioClips.Add(counter, Sound0);

        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            GunshotAudiosource.pitch = Random.Range(0.9f, 1.1f);
            GunshotAudiosource.panStereo = Random.Range(-0.3f, 0.3f);
            if (Soundisplaying)
            {
                GunshotAudiosource.loop = true;
                timer = 0;
            }
            else if(!Soundisplaying)
            {
                GunshotAudiosource.PlayOneShot(GunshotAudiosource.clip, Random.Range(ShotVolMin, ShotVolMax));
                GunshotAudiosource.clip = DictoAudioClips[Random.Range(0, DictoAudioClips.Count - 1)];
            }



        }
        waittime += 0.5;
    }







    #endregion
    */
    #endregion
}

