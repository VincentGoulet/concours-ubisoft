﻿using UnityEngine;
using System.Collections;

public class Raft_player_velocity : MonoBehaviour {

    private GameObject player;


	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find("Player");
	}
	
	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Raft")
        {
            Vector3 velocity = player.GetComponent<Rigidbody>().velocity;
            velocity.y = 0;
            player.GetComponent<Rigidbody>().velocity = velocity;
        }
    }
}
