﻿using UnityEngine;
using System.Collections;

public class Bird_Controller : MonoBehaviour {

    public Transform[] zones;
    private bool is_reach = true;
    private Vector3 _target_position;
    private Quaternion _startingAngle = Quaternion.AngleAxis(-40, Vector3.up);
    private Quaternion _stepAngle = Quaternion.AngleAxis(5, Vector3.up);
    private Transform _current_zone;
    public float speed = 20f;
    public float slerpspeed = 20f;
    private GameObject _enemy;
    private NavMeshAgent _agent;

    private CapsuleCollider[] _colliders;
    enum State { 
        FlyPatrol, 
        Landing,
        Landed,
        Attack_Socks,
        Attack_Player,
        Waiting
    };

    State e_state = State.FlyPatrol;
    void Awake()
    {
    }

    void Start()
    {
        _colliders = new CapsuleCollider[3];
        _agent = this.GetComponent<NavMeshAgent>();
        Debug.Log(this.GetComponents<CapsuleCollider>().Length);
        _colliders[0] = this.GetComponents<CapsuleCollider>()[0];
        _colliders[1] = this.GetComponents<CapsuleCollider>()[1];
       // _colliders[2] = this.GetComponents<CapsuleCollider>()[2];
    }

    void Update()
    {
        switch (e_state)
        {
            case State.FlyPatrol:
                Fly_Patrol();
                break;
            case State.Landing:
                _colliders[1].enabled = false;
                Landing();
                break;
            case State.Attack_Socks:
                Attack_Socks();
                break;
            case State.Landed:
                _agent.enabled = true;
               // _colliders[2].enabled = true;
                e_state = State.Attack_Socks;
                break;
            default:
                break;

        }
        Debug.DrawLine(transform.position, _target_position, Color.red);
    }
    void Attack_Socks()
    {
     GameObject sock;

        Debug.Log("Attack socks");
        RaycastHit hit;

      //  _agent.SetDestination(_enemy.transform.position);
        if (Physics.SphereCast(transform.position, 20, Vector3.down, out hit, 50))
        {
            Debug.Log(hit.transform.gameObject);
            if (hit.transform.gameObject.tag == "Socks")
            {
                Destroy(hit.transform.gameObject);
                e_state = State.FlyPatrol;
            }
        }
    }

    void Fly_Patrol()
    {
       // Debug.Log("Distance: " + Vector3.Distance(transform.position, _target_position));
        if (is_reach == true)
        {
            _target_position = Get_New_Position();
            //  Debug.Log("New Pos : " + current_position);
            is_reach = false;
        }
        else
            Move(_target_position);
    }

    void Landing()
    {
        if (Vector3.Distance(_target_position, transform.position) > 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, _target_position, speed * Time.deltaTime);
           // transform.Translate(transform.position * Time.deltaTime);
            //Move(_target_position);
            Vector3 targetDir = _target_position - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, speed * Time.deltaTime, 0.0F);
            transform.rotation = Quaternion.LookRotation(newDir);
        }
        else
        {
            e_state = State.Landed;
        }
    }


    GameObject Detect_objects(int distance)
    {
        RaycastHit hit;
        var angle = transform.rotation * _startingAngle;
        var direction = angle * Vector3.forward;
        var pos = transform.position;
        for (var i = 0; i < 24; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, distance))
            {
                Ray ray = new Ray(pos, direction);
                Debug.DrawLine(ray.origin, hit.point);
                return hit.transform.gameObject;
            }
            direction = _stepAngle * direction;
        }
        return null;
    }

    Vector3 Get_New_Position()
    {
        Transform new_Zone = zones[Random.Range(0, zones.Length)];
        while (_current_zone == new_Zone)
        {
            new_Zone = zones[Random.Range(0, zones.Length)];

        }
        //Debug.Log("Zone selected : " + new_Zone.gameObject.name);
        _current_zone = new_Zone;
        Vector3 new_position = Random.insideUnitSphere * 10 + new_Zone.position;
        new_position.y = 0;
        return (new_position);
    }

    void Move(Vector3 destination)
    {
        destination.y = transform.position.y;
        if (Vector3.Distance(transform.position, destination) < 5)
            is_reach = true;
        if (Detect_objects(5) != null)
        {
         //   destination.y += 50;

         //   Debug.Log("test");
            // get la positioon de l'objet pour obtenir le vecteur de la diagonale
           //transform.position = new Vector3(transform.position.x, transform.position.y + 50, transform.position.z);
            //  transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * smoothFactor);
             transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
        Detect_socks();
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
      //  transform.position = Vector3.Slerp(transform.position, destination, Time.deltaTime * speed);
        Vector3 targetDir = destination - transform.position;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, speed * Time.deltaTime, 0.0F);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    void Detect_socks()
    {
         RaycastHit hit;

        if (Physics.SphereCast(transform.position, 20, Vector3.down, out hit, 50))
        {
            Debug.Log(hit.transform.gameObject.name);
            if (hit.transform.gameObject.tag == "Socks")
            {
                if (e_state == State.FlyPatrol)
                {
                    _target_position = hit.transform.position;
                    e_state = State.Landing;
                    _enemy = hit.transform.gameObject;
                }
            }
        }
    }
   /* void OnTriggerEnter(Collider other)
    {
       if (other.transform.gameObject.tag == "Socks")
       {
           if (e_state == State.FlyPatrol)
           {
               _target_position = other.transform.position;
               e_state = State.Landing;
               _enemy = other.transform.gameObject;
           }
           else if (e_state == State.Attack_Socks)
           {
               _enemy = null;
               Destroy(other.transform.gameObject);
               e_state = State.FlyPatrol;
               _colliders[2].enabled = false;
               _colliders[1].enabled = true;
               _agent.Stop();
               _agent.enabled = false;
           }
          
       }
    }*/
}
