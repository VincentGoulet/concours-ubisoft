﻿using UnityEngine;
using System.Collections;

public class Tree_Controller : MonoBehaviour {
    private Object_Dictionary dictionary;

    public int n_fruits = 3;

    private float spawn_probability;
    public GameObject[] fruits = new GameObject[3];
    public Transform[] fruit_geometry = new Transform[3];
    public string[] fruit_status = new string[3] { "empty", "empty", "empty" };

    public int tree_index;
    private float timing;
    private float spawn_delay;

    // Use this for initialization
    void Start ()
    {
        
        int j = 0;
        foreach (Transform child in gameObject.transform)
        {
            if (child.transform.name.Split('_')[0] == "Fruit")
            {
                fruit_geometry[j] = child;
                j++;
            }
        }
            
            
        
        //dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        InvokeRepeating("Spawn_Fruit", 0.0f, spawn_delay);
    }

    void Spawn_Fruit()
    {
        if (!dictionary.Get_Is_Max_Fruit())
        {
            for (int position_index = 0; position_index < n_fruits; position_index++)
            {
                if ((fruit_status[position_index] == "empty") && (!dictionary.Get_Is_Max_Fruit()) && (Random.value <= spawn_probability))
                {
                    
                    fruits[position_index] = dictionary.Instantiate_Fruit(fruit_geometry[position_index].transform.position, tree_index, position_index);
                    fruit_status[position_index] = "growing";
                }
            }
        }
    }

    public void Reset_Position(int position_index)
    {
        fruit_status[position_index] = "empty";
    }

    public void Setup(int _tree_index, Object_Dictionary _dictionary, float _spawn_probability, float _spawn_delay)
    {
        tree_index = _tree_index;
        dictionary = _dictionary;
        spawn_probability = _spawn_probability;
        spawn_delay = _spawn_delay;
    }
}
