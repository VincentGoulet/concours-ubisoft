﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Signs_Controller : MonoBehaviour {
    enum Signtypes
    {
        water,
        spray,
        fruit,
        birds
    }

    public int threshold;
    Transform target;
    Object_Dictionary dictionary;
    Text[] texts;
    float height;
    float step;
    float max_step = 50;
    Image bubble;
    
    private Bubble_Popper bubble_popper;

    private Transform[] signs = new Transform[4];
    private GameObject[] sign_prefabs = new GameObject[4];
    private GameObject[] sign_bubbles = new GameObject[4];
    private string[] names = new string[4] { "Water", "Spray", "Fruit", "Birds" };

    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        bubble_popper = dictionary.Get_Bubble_Popper();

        for (int i = 0; i < names.Length; i++)
        {
            signs[i] = GameObject.Find("Sign" + names[i]).transform;
            sign_prefabs[i] = (GameObject)Resources.Load("Prefabs/UI/Sign" + names[i], typeof(GameObject));
            sign_bubbles[i] = (GameObject)Instantiate(sign_prefabs[i]);
            sign_bubbles[i].transform.parent = bubble_popper.transform;
            signs[i].GetComponent<Sign_Controller>().Setup(sign_bubbles[i].GetComponent<Image>());
        }
    }

    void Change_Message(string message)
    {
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].text = message;
        }
    }    
}
