﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bubble_Popper : MonoBehaviour {
    private GameObject alert;
    private GameObject poison;
    private GameObject lethal;
    private GameObject stun;

    private Object_Dictionary dictionary;
    public Transform player;
    Image player_image;
    GameObject player_bubble;

    // Use this for initialization
    void Start () {
        dictionary = GameObject.FindGameObjectWithTag("GameController").GetComponent<Object_Dictionary>();

        alert = (GameObject)Resources.Load("Prefabs/Bubbles/Alert", typeof(GameObject));
        poison = (GameObject)Resources.Load("Prefabs/Bubbles/Poison", typeof(GameObject));
        lethal = (GameObject)Resources.Load("Prefabs/Bubbles/Lethal", typeof(GameObject));
        stun = (GameObject)Resources.Load("Prefabs/Bubbles/Stun", typeof(GameObject));

        //Pop_Alert(player.transform);
    }

    // Update is called once per frame
    void LateUpdate () {

	}

    public void Pop_Alert(Transform target)
    {
        Pop(alert, target);
    }

    public void Pop_Poison(Transform target)
    {
        Pop(poison, target);
    }

    public void Pop_Lethal(Transform target)
    {
        Pop(lethal, target);
    }

    public void Pop_Stun(Transform target)
    {
        Pop(stun, target);
    }

    private void Pop(GameObject prefab, Transform target)
    {
        GameObject bubble = (GameObject)Instantiate(prefab);
        bubble.transform.parent = transform;
        bubble.GetComponent<Bubble_Manager>().Setup(target, dictionary);
    }
}
