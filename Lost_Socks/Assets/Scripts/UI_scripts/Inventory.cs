﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private float fade_time;

    private UI_Manager ui_manager;
    private Game_Controller game_controller;
    private Image[] images;
    private Color[] max_colors;

    private Image[] acts = new Image[6];
    private Text[] scores_text = new Text[3];
    private int[] sock_numbers = new int[6] { 0, 0, 0, 0, 0, 0 };
    public int[] max = new int[3] { 30, 30, 30 };

    // Use this for initialization
    void Awake()
    {
        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        ui_manager = gc.GetComponent<UI_Manager>();
        game_controller = gc.GetComponent<Game_Controller>();

        fade_time = ui_manager.fade_time;
        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];

        for (int i = 0; i < images.Length; i++)
        {
            max_colors[i] = images[i].color;
        }

        int j = 0;
        foreach (Transform child in GameObject.Find("Act").transform)
        {
            acts[j] = child.gameObject.GetComponent<Image>();
            j++;
        }

        j = 0;
        foreach (Transform child in GameObject.Find("Scores").transform)
        {
            scores_text[j] = child.gameObject.GetComponent<Text>();
            j++;
        }
    }

    void Start()
    {
        Inventory_Manager inventory = GameObject.Find("Player").GetComponent<Inventory_Manager>();
        int[] max_scores = inventory.Get_Max_Socks();

        max[0] = max_scores[0];
        max[1] = max_scores[1];
        max[2] = max_scores[2];

        for (int i = 0; i < scores_text.Length; i++)
        {
            scores_text[i].text = sock_numbers[i].ToString() + " / " + max[i].ToString();
        }
    }

    public void Unlock_Sock(string type)
    {
        //print(acts[Utilities.Type2Int(type)]);
        Update_Max_Colors(type);
        Utilities.Set_Transparency(acts[Utilities.Type2Int(type)], 1);
    }

    private void Update_Max_Colors(string type)
    {
        //print("2");
        //print(images);
        for (int i = 0; i < images.Length; i++)
        {
            if (images[i].transform.name == type)
            {
                max_colors[i] = new Color(images[i].color.r, images[i].color.g, images[i].color.b, 1);
            }
        }
    }

    public void Increment_Score(string type)
    {
        int i = Utilities.Type2Int(type);
        sock_numbers[i]++;
        if (i < 3)
        {
            scores_text[i].text = sock_numbers[i].ToString() + " / " + max[i].ToString();
        }
    }

    void OnEnable()
    {
        StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors));
    }

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
