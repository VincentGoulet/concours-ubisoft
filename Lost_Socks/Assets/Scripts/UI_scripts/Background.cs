﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Background : MonoBehaviour {
    private Game_Controller game_controller;
    private UI_Manager ui_manager;
    private Image background;
    private float fade_time;
    private Color color;
    private float max_alpha;
    private Image[] images;
    private Color[] max_colors;

    void Awake()
    {
        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        game_controller = gc.GetComponent<Game_Controller>();
        ui_manager = gc.GetComponent<UI_Manager>();

        fade_time = ui_manager.fade_time;

        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];

        for (int i = 0; i < images.Length; i++)
        {
            max_colors[i] = images[i].color;
        }

        /*background = GetComponentInChildren<Image>();
        color = background.color;
        max_alpha = background.color.a;
        background.color = new Color(color.r, color.g, color.b, 0.0f);*/
    }

    /*public IEnumerator Disable()
    {
        float end_time = Time.unscaledTime + fade_time;
        float t;
        float alpha;

        while (Time.unscaledTime < end_time)
        {
            t = (end_time - Time.unscaledTime) / (float)fade_time;
            alpha = Mathf.Lerp(0.0f, max_alpha, t);
            background.color = new Color(color.r, color.g, color.b, alpha);
            yield return null;
        }

        background.color = new Color(color.r, color.g, color.b, 0.0f);
        Time.timeScale = 1;
        game_controller.Set_State("game");
        gameObject.SetActive(false);
        yield return null;
    }*/

    void OnEnable()
    {
        StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors));
    }

    /*IEnumerator Fade_In()
    {
        float end_time = Time.unscaledTime + fade_time;
        float t;
        float alpha;

        while (Time.unscaledTime < end_time)
        {
            t = 1 - (end_time - Time.unscaledTime) / (float)fade_time;
            alpha = Mathf.Lerp(0.0f, max_alpha, t);
            background.color = new Color(color.r, color.g, color.b, alpha);
            yield return null;
        }

        background.color = new Color(color.r, color.g, color.b, max_alpha);
        yield return null;
    }*/

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
