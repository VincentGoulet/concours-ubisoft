﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Minimap_Controller : MonoBehaviour {
    private Image[] isle_images = new Image[4];
    private Image arrow;
    private Image hammer_location;
    private Image remote_location;
    private Object_Dictionary dictionary;
    private Player_Movement player_movement;

    private float x;
    private float z;
    private float angle;

    private float x0 = -37.8f;
    private float z0 = 112.5f;
    private float angle0 = 0.0f;

    private float xb = 133;
    private float zb = -82;
    private float angleb = 180;

    private float m = -.55f;

    private Vector2 arrow_position;
    private Vector3 arrow_rotation;

    private Color low = new Color(1.0f, 1.0f, 1.0f, .3f);
    

    // Use this for initialization
    void Start () {
        dictionary = GameObject.FindGameObjectWithTag("GameController").GetComponent<Object_Dictionary>();
        player_movement = dictionary.Get_Player_Movement();
        //player_movement.Get_Map_Position(out x0, out z0, out angle0);

        for (int i = 0; i < isle_images.Length; i++)
        {
            isle_images[i] = transform.FindChild("Isle" + i.ToString()).GetComponent<Image>();
        }
        arrow = transform.FindChild("Arrow").GetComponent<Image>();
        hammer_location = transform.FindChild("Hammer_location").GetComponent<Image>();
        remote_location = transform.FindChild("Remote_location").GetComponent<Image>();

    }

    // Update is called once per frame
    void LateUpdate () {
        player_movement.Get_Map_Position(out x, out z, out angle);

        x = m * (x - x0) + x0 + xb;
        z = m * (z - z0) + z0 + zb;
        angle = - (angle - angle0) + angleb;
        
        arrow_position = new Vector2(x, z);
        arrow.rectTransform.anchoredPosition3D = arrow_position; //GLITCH HERE
        arrow.rectTransform.rotation = Quaternion.Euler(0.0f, 0.0f, angle);

        Update_Highlight();
	}

    void Update_Highlight()
    {
        for (int i = 0; i < isle_images.Length; i++)
        {
            isle_images[i].color = low;

            if (isle_images[i].name == player_movement.isle_name)
            {
                isle_images[i].color = Color.white;
            }
        }
    }
    public void Hammer_Location()
    {
        Utilities.Set_Transparency(hammer_location, 1); 
    }
    public void Destroy_Hammer_Location()
    {
        Utilities.Set_Transparency(hammer_location, 0);
    }

    public void Remote_Location()
    {
        Utilities.Set_Transparency(remote_location, 1);
    }
    public void Destroy_Remote_Location()
    {
        Utilities.Set_Transparency(remote_location, 0);
    }
}
