﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Plain_Manager : MonoBehaviour {
    public int threshold;
    Transform target;
    Object_Dictionary dictionary;
    Text[] texts;
    float height;
    float step;
    float max_step = 50;
    float[] alphas;
    string message;
    Image blank;

    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        target = GameObject.Find("Player").transform;
    }
    
    void LateUpdate()
    {
        Vector3 wantedpos = Camera.main.WorldToScreenPoint(target.position + Camera.main.transform.rotation * Vector3.up * 2);
        GetComponent<RectTransform>().position = wantedpos;
    }
}
