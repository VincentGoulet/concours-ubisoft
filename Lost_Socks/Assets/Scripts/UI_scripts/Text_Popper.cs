﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Text_Popper : MonoBehaviour {
    private GameObject text;

    private Object_Dictionary dictionary;
    public Transform player;
    Image player_image;
    GameObject player_bubble;

    public int[] washer_numbers = new int[] { 100, 500, 2000, 3000 };
    int hammer_number = 1500;
    int remote_number = 5000;

    Text_Manager[] washer_text_objects = new Text_Manager[4];
    Text_Manager hammer_text_object;
    Text_Manager remote_text_object;

    bool[] washer_unlock = new bool[] { false, false, false, false };
    bool hammer_unlock = false;
    bool remote_unlock = false;

    // Use this for initialization
    void Start () {
        dictionary = GameObject.FindGameObjectWithTag("GameController").GetComponent<Object_Dictionary>();
        text = (GameObject)Resources.Load("Prefabs/Text", typeof(GameObject));

        for (int i = 0; i < washer_numbers.Length; i++)
        {
            washer_text_objects[i] = Pop_Text(dictionary.Get_Washer_Controller(i).transform, washer_numbers[i].ToString());
        }
        hammer_text_object = Pop_Text(GameObject.Find("Hammer_Box_").transform, hammer_number.ToString());
        remote_text_object = Pop_Text(GameObject.Find("Remote_Box_").transform, remote_number.ToString());
    }

    public void Update_Numbers(int score)
    {
        for (int i = 0; i < washer_numbers.Length; i++)
        {
            if (score >= washer_numbers[i])
            {
                if (!washer_unlock[i])
                {
                    washer_unlock[i] = true;
                    washer_text_objects[i].Unlock();
                }
            }
        }

        if (score >= hammer_number)
        {
            if (!hammer_unlock)
            {
                hammer_text_object.Unlock();
                hammer_unlock = true;
            }
        }

        if (score >= remote_number)
        {
            if (!remote_unlock)
            {
                remote_text_object.Unlock();
                remote_unlock = true;
            }
        }
    }

    private Text_Manager Pop_Text(Transform target, string message)
    {
        GameObject bubble = (GameObject)Instantiate(text);
        bubble.transform.parent = transform;
        bubble.GetComponent<Text_Manager>().Setup(target, dictionary, message);
        return bubble.GetComponent<Text_Manager>();
    }
}
