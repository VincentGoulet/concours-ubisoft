﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour {
    public float fade_time = 0.1f;
    public float extended_fade_time = 1.5f;

    private Checkpoint_Manager save_manager;

    private GameObject ui_background;
    private GameObject ui_game;
    private GameObject ui_menu;
    private GameObject ui_inventory;
    private GameObject ui_options;
    private GameObject ui_controls;
    private GameObject ui_reload;
    private GameObject ui_exit;
    [HideInInspector]
    public GameObject ui_bubbles;
    private GameObject ui_gameover;
    private GameObject ui_goreload;
    private GameObject ui_text;
    private GameObject ui_highscore;

    private Menu_Controller menu_controller;
    private Background background_script;
    private Inventory inventory_script;
    private Options options_script;
    private Controls controls_script;
    private Validate reload_script;
    private Validate exit_script;
    private Validate goreload_script;
    private Bubble_Popper bubble_popper;
    private Game_Over gameover_script;
    private Highscore_Manager highscore_script;
    private Image black;
    private Text_Popper text_popper;

    private GameObject icon_action;

    private Game_Controller game_controller;
    private string state = "game";

    private GameObject active_gameobject;
    private Image[] active_images;
    private Color[] active_colors;

    private bool is_validating;
    private bool is_done_loading;

    private Text sock_text;
    private Text sock_subtext;

    /*private Vector3 back_fullscale;
    private Vector3 hilite_fullscale;*/
    private Image[] images_b = new Image[3];
    private Image[] images_a = new Image[3];
    private Image[] images_x = new Image[3];
    private Image[] images_y = new Image[3];

    private GameObject jump;
    private GameObject washer;
    private GameObject fruit;
    private GameObject eat;
    private GameObject throww;
    private GameObject drop;
    private GameObject remote;
    private GameObject hammer;

    private GameObject fruit_mode;

    private Object_Dictionary dictionary;
    private string control_mode = "";
    GameObject plain_text;

    GameObject save_bubble;

    // Use this for initialization
    void Awake()
    {
        dictionary = GameObject.FindGameObjectWithTag("GameController").GetComponent<Object_Dictionary>();

        sock_text = GameObject.Find("Sock Text").GetComponent<Text>();
        //sock_subtext = GameObject.Find("Sock Subtext").GetComponent<Text>();
        save_manager = GetComponent<Checkpoint_Manager>();

        Transform canvases;
        try
        {
            canvases = GameObject.Find("UI canvases").transform;
        }
        catch
        {
            throw new System.Exception("Il manque probablement le prefab 'UI canvases' dans la hierarchie.");
        }

        ui_background = canvases.Find("Background").gameObject;
        ui_controls = canvases.Find("Controls").gameObject;
        ui_menu = canvases.Find("Start Menu").gameObject;
        ui_inventory = canvases.Find("Inventory").gameObject;
        ui_background = canvases.Find("Background").gameObject;
        ui_options = canvases.Find("Options").gameObject;
        ui_game = canvases.Find("HUDCanvas").gameObject;
        ui_reload = canvases.Find("Reload").gameObject;
        ui_exit = canvases.Find("Exit").gameObject;
        ui_bubbles = canvases.Find("Bubbles").gameObject;
        ui_gameover = canvases.Find("Game Over").gameObject;
        ui_goreload = canvases.Find("Game Over Reload").gameObject;
        ui_text = canvases.Find("Texts").gameObject;
        ui_highscore = canvases.Find("Highscore").gameObject;

        save_bubble = ui_bubbles.transform.FindChild("Save").gameObject;
        save_bubble.transform.localScale = Vector3.zero;

        black = GameObject.Find("Void").GetComponent<Image>();
        Utilities.Set_Transparency(black, 0);

        //icon_action = ui_game.transform.Find("Buttons_ABXY").Find("Hilite_B").gameObject;
        sock_text.text = 0.ToString();
        //sock_subtext.text = 0.ToString();

        Setup_Buttons();

        game_controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game_Controller>();
        menu_controller = ui_menu.GetComponent<Menu_Controller>();
        background_script = ui_background.GetComponent<Background>();
        inventory_script = ui_inventory.GetComponent<Inventory>();
        options_script = ui_options.GetComponent<Options>();
        controls_script = ui_controls.GetComponent<Controls>();
        reload_script = ui_reload.GetComponent<Validate>();
        exit_script = ui_exit.GetComponent<Validate>();
        goreload_script = ui_exit.GetComponent<Validate>();
        highscore_script = ui_highscore.GetComponent<Highscore_Manager>();
        bubble_popper = ui_bubbles.GetComponent<Bubble_Popper>();
        text_popper = ui_text.GetComponent<Text_Popper>();

        plain_text = (GameObject)Resources.Load("Prefabs/PlainText", typeof(GameObject));
    }

    void Start()
    {
        ui_background.SetActive(false);
        ui_controls.SetActive(false);
        ui_inventory.SetActive(false);
        ui_options.SetActive(false);
        ui_menu.SetActive(false);
        ui_reload.SetActive(false);
        ui_exit.SetActive(false);
        ui_gameover.SetActive(false);
        ui_goreload.SetActive(false);
        ui_highscore.SetActive(false);
        //Time.timeScale = 0;
        StartCoroutine(Toggle_Controls("Default"));
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (state != "game" && state != "gameover" && state != "go_reload")
            {
                Cancel();
            }
        }
        else if (Input.GetButtonDown("Back"))
        {
            if (state == "game")
            {
                Toggle("inventory_select");
            }
            else if (state == "inventory_select")
            {
                Toggle("game");
            }
        }
    }

    void Setup_Buttons()
    {
        images_b[0] = GameObject.Find("Back_B").GetComponent<Image>();
        images_b[1] = GameObject.Find("Hilite_B").GetComponent<Image>();
        images_b[2] = GameObject.Find("Frame_B").GetComponent<Image>();

        images_a[0] = GameObject.Find("Back_A").GetComponent<Image>();
        images_a[1] = GameObject.Find("Hilite_A").GetComponent<Image>();
        images_a[2] = GameObject.Find("Frame_A").GetComponent<Image>();

        images_x[0] = GameObject.Find("Back_X").GetComponent<Image>();
        images_x[1] = GameObject.Find("Hilite_X").GetComponent<Image>();
        images_x[2] = GameObject.Find("Frame_X").GetComponent<Image>();

        images_y[0] = GameObject.Find("Back_Y").GetComponent<Image>();
        images_y[1] = GameObject.Find("Hilite_Y").GetComponent<Image>();
        images_y[2] = GameObject.Find("Frame_Y").GetComponent<Image>();

        jump = GameObject.Find("Jump Icon");
        washer = GameObject.Find("Washer Icon");
        fruit = GameObject.Find("Fruit Icon");
        eat = GameObject.Find("Eat Icon");
        throww = GameObject.Find("Throw Icon");
        drop = GameObject.Find("Drop Icon");
        remote = GameObject.Find("Remote Icon");
        hammer = GameObject.Find("Hammer Icon");

        Utilities.Set_Transparency(jump.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(washer.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(fruit.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(eat.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(throww.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(remote.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(hammer.GetComponent<Image>(), 0);
        Utilities.Set_Transparency(drop.GetComponent<Image>(), 0);

        fruit_mode = GameObject.Find("Fruit Mode");
        Utilities.Set_Transparency(fruit_mode.GetComponent<Image>(), 0);

        for (int i = 0; i < images_a.Length; i++)
        {
            Utilities.Set_Transparency(images_b[i], 0);
            Utilities.Set_Transparency(images_a[i], 0);
            Utilities.Set_Transparency(images_x[i], 0);
            Utilities.Set_Transparency(images_y[i], 0);
        }
    }

    public void Unlock_Sock(string type)
    {
        inventory_script.Unlock_Sock(type);
    }

    public Inventory Get_Inventory_Script()
    {
        return inventory_script;
    }

    public void Update_Socks(int sock_number)
    {
        StartCoroutine(Highlight_Text());

        sock_text.text = sock_number.ToString();
        //sock_subtext.text = sock_number.ToString();
    }

    float highlight_time = 0.2f;
    IEnumerator Highlight_Text()
    {
        sock_text.fontStyle = FontStyle.Normal;
        //sock_subtext.fontStyle = FontStyle.Normal;

        yield return null;

        sock_text.color = Color.yellow;
        //sock_subtext.color = Color.yellow;

        sock_text.fontStyle = FontStyle.Bold;
        //sock_subtext.fontStyle = FontStyle.Bold;

        yield return new WaitForSeconds(highlight_time);
        
        sock_text.color = Color.white;
        //sock_subtext.color = Color.white;

        sock_text.fontStyle = FontStyle.Normal;
        //sock_subtext.fontStyle = FontStyle.Normal;
    }

    private float pop_interval = 0.1f;
    private float inout_interval = 0.5f; 
    public IEnumerator Toggle_Controls(string mode)
    {
        StartCoroutine(Pop_Out_All());
        yield return new WaitForSeconds(inout_interval);

        switch (mode)
        {
            case ("Default"):
                StartCoroutine(Pop_In("Jump"));
                yield return new WaitForSeconds(inout_interval);

                if (dictionary.Get_Player_Movement().Has_Remote())
                {
                    StartCoroutine(Pop_In("Remote"));
                }
                yield return new WaitForSeconds(pop_interval);

                if (dictionary.Get_Player_Movement().Has_Hammer())
                {
                    StartCoroutine(Pop_In("Hammer"));
                }
                yield return new WaitForSeconds(pop_interval);

                if (dictionary.Get_Game_Controller().Is_B_Available())
                {
                    string type = dictionary.Get_Game_Controller().Get_B_Action();
                    StartCoroutine(Pop_In(type));
                }
                break;

            case ("Fruit"):
                StartCoroutine(Pop_In("Fruit Mode"));

                StartCoroutine(Pop_In("Jump"));
                yield return new WaitForSeconds(pop_interval);
                
                StartCoroutine(Pop_In("Eat"));
                yield return new WaitForSeconds(pop_interval);
                
                StartCoroutine(Pop_In("Throw"));
                yield return new WaitForSeconds(pop_interval);

                StartCoroutine(Pop_In("Drop"));
                break;

            default:
                throw new System.Exception("Le mode '" + mode + "' n'est pas un mode de controle connu.");
        }
    }

    IEnumerator Pop_Out_All()
    {
        StartCoroutine(Pop_Out("Fruit Mode"));
        StartCoroutine(Pop_Out("Jump"));
        yield return new WaitForSeconds(pop_interval);
        
        StartCoroutine(Pop_Out("Remote"));
        StartCoroutine(Pop_Out("Eat"));
        yield return new WaitForSeconds(pop_interval);
        
        StartCoroutine(Pop_Out("Hammer"));
        StartCoroutine(Pop_Out("Throw"));
        yield return new WaitForSeconds(pop_interval);

        StartCoroutine(Pop_Out("Drop"));
        StartCoroutine(Pop_Out("Washer"));
        yield return StartCoroutine(Pop_Out("Pick"));
    }

    public void Display_Action(string type, bool is_active = true)
    {
        if (is_active)
        {
            StartCoroutine(Pop_In(type));
        }
        else
        {
            StartCoroutine(Pop_Out(type));
        }
    }

    private float zoom_time = 0.15f;
    private float resorb_time = 0.05f;
    float min_size = 0.1f;
    float max_size = 1.3f;

    IEnumerator Pop_In(string type)
    {
        string button_name = Get_Button(type);
        Image[] images = Get_Button_Images(button_name);
        GameObject icon = Get_Icon(type);

        for (int i = 0; i < images.Length; i++)
        {
            float alpha = 1;
            if (i == 2) { alpha = 96f / 255f * alpha; }
            Utilities.Set_Transparency(images[i], alpha);
        }

        Utilities.Set_Transparency(icon.GetComponent<Image>(), 1);
        //else if (icon.GetComponentInChildren<Material>() != null) { Utilities.Set_Transparency(icon.GetComponentInChildren<Material>(), 1); }

        float timer = 0.0f;
        float new_size;
        Resize_Images(images, min_size);
        icon.transform.localScale = Vector3.one * min_size;

        while (timer <= zoom_time)
        {
            yield return null;
            timer += Time.deltaTime;

            new_size = min_size + (timer) * (max_size - min_size) / (zoom_time);
            Resize_Images(images, new_size);
            icon.transform.localScale = Vector3.one * new_size;
        }

        Resize_Images(images, max_size);
        icon.transform.localScale = Vector3.one * max_size;
        yield return null;

        timer = 0.0f;
        while (timer <= resorb_time)
        {
            yield return null;
            timer += Time.deltaTime;

            new_size = max_size + (timer) * (1 - max_size) / (resorb_time);
            Resize_Images(images, new_size);
            icon.transform.localScale = Vector3.one * new_size;
        }

        Resize_Images(images, 1.0f);
        icon.transform.localScale = Vector3.one;
        yield return null;
    }

    IEnumerator Pop_Out(string type)
    {
        string button_name = Get_Button(type);
        Image[] images = Get_Button_Images(button_name);
        GameObject icon = Get_Icon(type);

        float timer = 0.0f;
        float new_size;

        timer = 0.0f;
        while (timer <= resorb_time)
        {
            yield return null;
            timer += Time.deltaTime;

            new_size = 1 + (timer) * (max_size - 1) / (resorb_time);
            Resize_Images(images, new_size);
            icon.transform.localScale = Vector3.one * new_size;
        }

        Resize_Images(images, max_size);
        yield return null;

        while (timer <= zoom_time)
        {
            yield return null;
            timer += Time.deltaTime;

            new_size = max_size + (timer) * (min_size - max_size) / (zoom_time);
            Resize_Images(images, new_size);
            icon.transform.localScale = Vector3.one * new_size;
        }

        for (int i = 0; i < images.Length; i++)
        {
            Utilities.Set_Transparency(images[i], 0);
        }
        Utilities.Set_Transparency(icon.GetComponent<Image>(), 0);
        yield return null;
    }

    private void Resize_Images(Image[] images, float size)
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].rectTransform.localScale = Vector3.one * size;
        }
    }

    public void Cancel()
    {
        if (state == "go_reload")
        {
            game_controller.Exit();
        }
        else
        {
            Toggle(Get_Cancel_State());
        }
    }

    float void_fade = 0.5f;
    IEnumerator Reload()
    {
        float timer = 0.0f;
        float new_alpha = black.color.a;
        while (timer < void_fade)
        {
            yield return null;
            new_alpha = timer / void_fade;
            Utilities.Set_Transparency(black, new_alpha);
            timer += Time.unscaledDeltaTime;
        }

        Utilities.Set_Transparency(black, 1);
        yield return null;
        
        ui_menu.SetActive(false);
        ui_reload.SetActive(false);
        ui_exit.SetActive(false);
        ui_goreload.SetActive(false);
        ui_background.SetActive(false);
        ui_gameover.SetActive(false);
        save_manager.Load();
        //StartCoroutine(save_manager.Deep_Load(is_done_loading));

        Time.timeScale = 1;
        state = "game";
        yield return new WaitForSeconds(1);
        
        timer = 0.0f;
        new_alpha = black.color.a;
        while (timer < void_fade)
        {
            yield return null;
            new_alpha = 1 - timer / void_fade;
            Utilities.Set_Transparency(black, new_alpha);
            timer += Time.unscaledDeltaTime;
        }
        
        Utilities.Set_Transparency(black, 0);
        yield return null;
    }

    public void Validate()
    {
        switch (state)
        {
            case ("reload"):
                StartCoroutine(Reload());
                break;

            case ("exit"):
                game_controller.Exit();
                break;

            case ("go_reload"):
                StartCoroutine(Reload());
                break;

            default:
                throw new System.Exception("State '" + state + "' has no known validation effect.");
        }
    }

    private IEnumerator Disable(GameObject ui_object, Image[] images, Color[] colors)
    {
        float end_time = Time.unscaledTime + fade_time;
        float t;
        float alpha;

        while (Time.unscaledTime < end_time)
        {
            for (int i = 0; i < images.Length; i++)
            {
                t = (end_time - Time.unscaledTime) / (float)fade_time;
                alpha = Mathf.Lerp(0.0f, colors[i].a, t);
                images[i].color = new Color(colors[i].r, colors[i].g, colors[i].b, alpha);
            }

            yield return null;
        }

        for (int i = 0; i < images.Length; i++)
        {
            images[i].color = new Color(colors[i].r, colors[i].g, colors[i].b, 0.0f);
        }

        ui_object.SetActive(false);
    }

    public IEnumerator Fade_In(GameObject other_gameObject, Image[] images, Color[] colors, bool is_extended = false)
    {
        float effective_fade_time;
        effective_fade_time = is_extended ? extended_fade_time : fade_time;
        float end_time = Time.unscaledTime + effective_fade_time;
        
        float t;
        float alpha;

        while (Time.unscaledTime < end_time)
        {
            for (int i = 0; i < images.Length; i++)
            {
                t = 1 - (end_time - Time.unscaledTime) / (float)effective_fade_time;
                alpha = Mathf.Lerp(0.0f, colors[i].a, t);
                images[i].color = new Color(colors[i].r, colors[i].g, colors[i].b, alpha);
            }

            yield return null;
        }

        for (int i = 0; i < images.Length; i++)
        {
            images[i].color = colors[i];
        }
    }

    public void Toggle(string new_state)
    {
        GameObject new_display = Get_Display(new_state);
        StartCoroutine(Toggle_Coroutine(new_display, new_state));
    }

    private IEnumerator Toggle_Coroutine(GameObject new_display, string new_state)
    {
        if (new_state == "game" && ((state == "reload") || (state == "exit") || (state == "go_reload")))
        {
            yield return StartCoroutine(Disable(ui_menu, menu_controller.Get_Images(), menu_controller.Get_Colors()));
        }
        if (state != "game" && new_state != "reload" && new_state != "exit" && new_state != "go_reload" && new_state != "highscore")
        {
            yield return StartCoroutine(Disable(active_gameobject, active_images, active_colors));
        }
        else if (state == "game")
        {
            if (new_state != "gameover")
            {
                Time.timeScale = 0;
            }
            ui_background.SetActive(true);
        }

        if (new_state != "game")
        {
            new_display.SetActive(true);
        }
        else if (new_state == "game")
        {
            StartCoroutine(Disable(ui_background, background_script.Get_Images(), background_script.Get_Colors()));
            Time.timeScale = 1;
        }

        state = new_state;
        Set_Validating();
        Set_Active_Parameters();
    }

    private void Set_Active_Parameters()
    {
        active_gameobject = Get_Display(state);

        switch (state)
        {
            case ("menu"):
                active_images = menu_controller.Get_Images();
                active_colors = menu_controller.Get_Colors();
                break;

            case ("options"):
                active_images = options_script.Get_Images();
                active_colors = options_script.Get_Colors();
                break;

            case ("controls"):
                active_images = controls_script.Get_Images();
                active_colors = controls_script.Get_Colors();
                break;

            case ("inventory"):
                active_images = inventory_script.Get_Images();
                active_colors = inventory_script.Get_Colors();
                break;

            case ("inventory_select"):
                active_images = inventory_script.Get_Images();
                active_colors = inventory_script.Get_Colors();
                break;

            case ("reload"):
                active_images = reload_script.Get_Images();
                active_colors = reload_script.Get_Colors();
                break;

            case ("exit"):
                active_images = gameover_script.Get_Images();
                active_colors = gameover_script.Get_Colors();
                break;

            case ("game"):
                active_images = new Image[0];
                active_colors = new Color[0];
                break;

            case ("gameover"):
                active_images = exit_script.Get_Images();
                active_colors = exit_script.Get_Colors();
                break;

            case ("go_reload"):
                active_images = goreload_script.Get_Images();
                active_colors = goreload_script.Get_Colors();
                break;

            case ("highscore"):
                active_images = highscore_script.Get_Images();
                active_colors = highscore_script.Get_Colors();
                break;

            default:
                throw new System.Exception("L'etat '" + state + "' est invalide dans Set_Active_Parameters");
        }
    }

    private GameObject Get_Display(string state)
    {
        switch (state)
        {
            case ("inventory"):
                return ui_inventory;

            case ("inventory_select"):
                return ui_inventory;

            case ("menu"):
                return ui_menu;

            case ("options"):
                return ui_options;

            case ("controls"):
                return ui_controls;

            case ("game"):
                return ui_game;

            case ("reload"):
                return ui_reload;

            case ("gameover"):
                return ui_gameover;

            case ("go_reload"):
                return ui_goreload;

            case ("highscore"):
                return ui_highscore;

            default:
                throw new System.Exception("State '" + state + "' unknown.");
        }
    }

    private string Get_Cancel_State()
    {
        switch (state)
        {
            case ("inventory"):
                return "menu";

            case ("inventory_select"):
                return "game";

            case ("menu"):
                return "game";

            case ("options"):
                return "menu";

            case ("controls"):
                return "menu";

            case ("game"):
                return "";

            case ("reload"):
                return "menu";

            default:
                throw new System.Exception("State '" + state + "' unknown.");
        }
    }

    public string Get_State()
    {
        return state;
    }

    public void Set_Validating()
    {
        is_validating = false;
        is_validating |= state == "reload";
        is_validating |= state == "exit";
        is_validating |= state == "go_reload";
    }

    public bool Get_Validating()
    {
        return is_validating;
    }

    public Bubble_Popper Get_Bubble_Popper()
    {
        return bubble_popper;
    }

    private Image[] Get_Button_Images(string button_name)
    {
        switch (button_name)
        {
            case ("B"):
                return images_b;

            case ("A"):
                return images_a;

            case ("X"):
                return images_x;

            case ("Y"):
                return images_y;

            case ("Fruit Mode"):
                return new Image[1] { fruit_mode.GetComponent<Image>() };

            default:
                throw new System.Exception("Le bouton '" + button_name + "' n'existe pas.");
        }
    }

    private string Get_Button(string type)
    {
        switch (type)
        {
            case ("Jump"):
                return "A";

            case ("Washer"):
                return "B";

            case ("Pick"):
                return "B";

            case ("Remote"):
                return "X";

            case ("Hammer"):
                return "Y";

            case ("Eat"):
                return "X";

            case ("Throw"):
                return "Y";

            case ("Drop"):
                return "B";

            case ("Fruit Mode"):
                return "Fruit Mode";

            default:
                throw new System.Exception("Le type d'action '" + type + "' n'est associe a aucun bouton.");
        }
    }

    private GameObject Get_Icon(string type)
    {
        switch (type)
        {
            case ("Jump"):
                return jump;

            case ("Washer"):
                return washer;

            case ("Pick"):
                return fruit;

            case ("Remote"):
                return remote;

            case ("Hammer"):
                return hammer;

            case ("Eat"):
                return eat;

            case ("Throw"):
                return throww;

            case ("Drop"):
                return drop;

            case ("Fruit Mode"):
                return fruit_mode;

            default:
                throw new System.Exception("Le type d'action '" + type + "' n'est associe a aucune icone.");
        }
    }

    public Text_Popper Get_Text_Popper()
    {
        return text_popper;
    }
    
    public IEnumerator Pop_Text(string message)
    {
        //print("popping text");
        GameObject temp_text = (GameObject)Instantiate(plain_text);
        temp_text.GetComponent<Text>().text = message;
        temp_text.transform.parent = ui_text.transform;
        float popin_time = 0.2f;
        float popout_time = 0.5f;
        
        StartCoroutine(Utilities.Fade(temp_text.GetComponent<Text>(), popin_time, 0, 1));
        yield return StartCoroutine(Utilities.Resize(temp_text.transform, popin_time, 0, 1));
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(Utilities.Fade(temp_text.GetComponent<Text>(), popout_time, 1, 0));
        StartCoroutine(Utilities.Move(temp_text.GetComponent<Text>().rectTransform, popout_time, 0, 50));
        yield return null;
    }

    public IEnumerator Pop_Save()
    {
        yield return StartCoroutine(Utilities.Resize(save_bubble.transform, 0.6f, 0, 1));
        //throw new System.Exception("Mange mon bat");
        yield return new WaitForSeconds(1.4f);
        StartCoroutine(Utilities.Resize(save_bubble.transform, 0.6f, 1, 0));
    }

    public IEnumerator Fade_To_Black(float time)
    {
        float timer = 0.0f;
        float new_alpha = black.color.a;

        while (timer < time)
        {
            Utilities.Set_Transparency(black, new_alpha);
            yield return null;
            timer += Time.unscaledDeltaTime;
            new_alpha = timer / time;
        }

        Utilities.Set_Transparency(black, 1);
        Time.timeScale = 0;
        yield return null;
    }

    public IEnumerator Fade_From_Black(float time)
    {
        float timer = 0.0f;
        Transform first_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_1");
        float new_alpha = 1;

        while (timer < time)
        {
            Utilities.Set_Transparency(black, new_alpha);
            yield return null;
            timer += Time.unscaledDeltaTime;
            new_alpha = 1 - timer / time;
        }
        Utilities.Set_Transparency(black, 0);
        //Time.timeScale = 0;			
        yield return null;
    }

    IEnumerator Ending_Animation()
    {
        Transform first_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_1");
        Transform last_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_2");

        //yield return StartCoroutine();
        yield return StartCoroutine(Utilities.Lerp_Camera(first_cam, last_cam, 0.7f));
    }

    IEnumerator Suicide_Ending()
    {
        Transform first_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_2");
        Transform last_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_3");

        yield return StartCoroutine(Utilities.Lerp_Camera(first_cam, last_cam, 0.7f));
    }

    IEnumerator Suicide_Ending_2()
    {
        Transform first_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_3");
        Transform last_cam = GameObject.Find("Final Animation").transform.FindChild("Cam_4");

        yield return StartCoroutine(Utilities.Lerp_Camera(first_cam, last_cam, 0.7f));
    }

    public IEnumerator End()
    {
        yield return StartCoroutine(Fade_To_Black(.3f));
        
        /*yield return StartCoroutine(Ending_Animation());
        yield return StartCoroutine(Suicide_Ending());
        yield return StartCoroutine(Suicide_Ending_2());
        yield return StartCoroutine(Fade_To_Black(.3f));*/
        Toggle("highscore");
        //ui_game.SetActive(false);
    }
}