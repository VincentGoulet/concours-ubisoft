﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bubble_Manager : MonoBehaviour {
    Transform target;
    Object_Dictionary dictionary;
    Image this_image;
    Vector3 max_scale;
    Vector3 min_scale;
    float height;
    float pop_time = 0.25f;
    float drop_time = 0.25f;
    float time = 2.0f;
    float step;
    float max_step = 10;

    // Update is called once per frame
    void Update() {
        if (target == null)
        {
            Destroy(gameObject);
        }
    }

    void LateUpdate()
    {
        Vector3 wantedpos = Camera.main.WorldToScreenPoint(target.position + Camera.main.transform.up * height);
        Vector3 effect = new Vector3(0.0f, step, 0.0f);
        this_image.rectTransform.position = wantedpos + effect;

        transform.localScale = Vector3.one;
        float distance = (target.transform.position - dictionary.Get_Player().transform.position).magnitude;
        if ( distance > 3 && distance < 5)
        {
            Vector3 new_scale = Vector3.one;
            transform.localScale = new_scale * (1 - (distance - 3) / (float)(5 - 3));
        }
        if (distance > 5)
        {
            transform.localScale = Vector3.zero;
        }
    }

    IEnumerator Pop()
    {
        transform.localScale = max_scale / 10.0f;
        float timer = 0.0f;

        while (timer < pop_time)
        {
            yield return null;
            transform.localScale = Vector3.Lerp(min_scale, max_scale, timer/pop_time);
            timer += Time.deltaTime;
        }
        transform.localScale = max_scale;

        while (timer - pop_time < drop_time)
        {
            yield return null;
            step = - max_step / (drop_time * drop_time) * (timer - pop_time) * (timer - pop_time) + max_step;
            timer += Time.deltaTime;
        }
        step = 0.0f;

        yield return new WaitForSeconds(time - pop_time);

        timer = 0.0f;
        while (timer < pop_time)
        {
            yield return null;
            transform.localScale = Vector3.Lerp(min_scale, max_scale, (pop_time - timer) / pop_time);
            timer += Time.deltaTime;
        }

        Destroy(gameObject);
    }

    public void Setup (Transform _target, Object_Dictionary _dictionary)
    {
        target = _target;
        dictionary = _dictionary;

        this_image = GetComponent<Image>();
        max_scale = transform.localScale *.6f;
        min_scale = max_scale / 10.0f;
        height = Get_Height();
        step = max_step;
        
        StartCoroutine(Pop());
    }

    private float Get_Height()
    {
        switch (target.tag)
        {
            case ("Socks"):
                return 1;

            case ("Bird"):
                return 1.3f;

            case ("Player"):
                return 2;

            case ("Fruits"):
                return 0.7f;

            default:
                throw new System.Exception("Il n'y a pas de hauteur associee au type " + target.tag);
        }
    }
}
