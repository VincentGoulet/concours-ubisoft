﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Text_Manager : MonoBehaviour {
    public int threshold;
    Transform target;
    Object_Dictionary dictionary;
    Text[] texts;
    float height;
    float step;
    float max_step = 50;
    float[] alphas;
    string message;
    Image blank;

    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        //target = GameObject.Find("Washer0").transform;

        texts = GetComponentsInChildren<Text>();
        //print(name);
        if (transform.FindChild("Blank") != null)
        {
            blank = transform.FindChild("Blank").GetComponent<Image>();
        }


        alphas = new float[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            alphas[i] = texts[i].color.a;
        }

        Change_Message(message);
    }

    void Change_Message(string message)
    {
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].text = message;
        }
    }

    float alpha;
    float min = 3f;
    float max = 5f;
    void LateUpdate()
    {
        Vector3 wantedpos = Camera.main.WorldToScreenPoint(target.position + Camera.main.transform.rotation * Vector3.up * 3);
        //Vector3 effect = new Vector3(0.0f, max_step, 0.0f);
        GetComponent<RectTransform>().position = wantedpos;// + effect;


        if (blank != null) Utilities.Set_Transparency(blank, 1);

        for (int i = 0; i < texts.Length; i++)
        {
            Utilities.Set_Transparency(texts[i], alphas[i]);
        }
        float distance = (target.transform.position - dictionary.Get_Player().transform.position).magnitude;
        if ( distance > min && distance < max)
        {
            if (blank != null) Utilities.Set_Transparency(blank, (1 - (distance - min) / (float)(max - min)));
            for (int i = 0; i < texts.Length; i++)
            {

                alpha = alphas[i] * (1 - (distance - min) / (float)(max - min));
                Utilities.Set_Transparency(texts[i], alpha);
            }
        }

        if (distance > 5)
        {
            Utilities.Set_Transparency(texts, 0);
            if (blank != null) Utilities.Set_Transparency(blank, 0);
        }
    }

    public void Unlock()
    {
        Change_Message("✔");
        if (blank != null) blank.color = new Color(4/255f, 189/255f, 50/255f); ;
    }

   public void Setup(Transform _target, Object_Dictionary _dictionary, string _message)
    {
        target = _target;
        dictionary = _dictionary;
        message = _message;

        step = max_step;
    }

    private float Get_Height()
    {
        switch (target.tag)
        {
            case ("Socks"):
                return 1;

            case ("Bird"):
                return 1.3f;

            case ("Player"):
                return 2;

            case ("Fruits"):
                return 0.7f;

            default:
                throw new System.Exception("Il n'y a pas de hauteur associee au type " + target.tag);
        }
    }
}
