﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sign_Controller : MonoBehaviour {
    Image bubble;
    Vector3 min_scale = Vector3.one/10f;
    Vector3 max_scale = Vector3.one;

    public void Setup(Image _bubble)
    {
        bubble = _bubble;

        /*print(gameObject.name);
        print(bubble);
        print(_bubble);*/
        Utilities.Set_Transparency(bubble, 0);
    }

    void LateUpdate()
    {
        Vector3 wantedpos = Camera.main.WorldToScreenPoint(transform.position + Camera.main.transform.rotation * Vector3.up * 3f);
        //Vector3 effect = new Vector3(0.0f, step, 0.0f);
        bubble.rectTransform.position = wantedpos;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Pop());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Unpop());
        }
    }
    
    float pop_time = 0.25f;
    float drop_time = 0.25f;
    IEnumerator Pop()
    {
        /*print("in pop");
        print(gameObject.name);
        print(bubble);*/
        //print("pop");
        Utilities.Set_Transparency(bubble, 1);
        yield return null;
    }

    IEnumerator Unpop()
    {
        Utilities.Set_Transparency(bubble, 0);
        yield return null;
    }
}
