﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Validate : MonoBehaviour
{
    private float fade_time;

    private UI_Manager ui_manager;
    private Game_Controller game_controller;
    private Image[] images;
    private Color[] max_colors;
    
    public string default_state;
    public string state = "no";

    private Image act_yes;
    private Image act_no;

    // Use this for initialization
    void Awake()
    {
        default_state = state;

        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        ui_manager = gc.GetComponent<UI_Manager>();
        game_controller = gc.GetComponent<Game_Controller>();
        
        act_yes = transform.Find("Yes").Find("Act Yes").GetComponent<Image>();
        act_no = transform.Find("No").Find("Act No").GetComponent<Image>();

        fade_time = ui_manager.fade_time;
        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];

        for (int i = 0; i < images.Length; i++)
        {
            max_colors[i] = images[i].color;
        }
    }

    void Update()
    {
        if (Input.GetAxis("Horizontal") >= 0.5 && state == "yes")
        {
            act_no.color = Color.white;
            act_yes.color = Utilities.transparent;
            state = "no";
        }
        else if (Input.GetAxis("Horizontal") <= -0.5 && state == "no")
        {
            act_yes.color = Color.white;
            act_no.color = Utilities.transparent;
            state = "yes";
        }
        
        if (Input.GetButtonDown("Submit"))
        {
            if (state == "yes")
            {
                ui_manager.Validate();
            }
            if (state == "no")
            {
                ui_manager.Cancel();
            }
            state = default_state;
        }
    }

    void OnEnable()
    {
        StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors));
    }

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
