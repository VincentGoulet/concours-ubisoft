﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game_Over : MonoBehaviour
{
    private float fade_time;
    private float pause_time = 1.0f;

    private UI_Manager ui_manager;
    private Game_Controller game_controller;
    private Image[] images;
    private Color[] max_colors;

    // Use this for initialization
    void Awake()
    {
        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        ui_manager = gc.GetComponent<UI_Manager>();
        game_controller = gc.GetComponent<Game_Controller>();

        fade_time = ui_manager.fade_time;
        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];

        for (int i = 0; i < images.Length; i++)
        {
            max_colors[i] = images[i].color;
        }
    }

    void OnEnable()
    {
        StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors, true));
        StartCoroutine(Reload());
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(pause_time);
        ui_manager.Toggle("go_reload");
    }

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
