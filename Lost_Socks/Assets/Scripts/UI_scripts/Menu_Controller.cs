﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu_Controller : MonoBehaviour {
    private float fade_time;

    private GameObject[] deact = new GameObject[6];
    private GameObject[] act = new GameObject[6];
    private Image[] texts = new Image[6];

    // Selection variables
    private int n_buttons = 6;
    private bool is_new;
    private int[] angles = new int[7];
    private int[] default_angles = new int[7] { -180, -110, -70, 0, 70, 110, 180 };
    public int damp = 10;
    int i_selected = 0;
    float theta;
    float h;
    float v;

    private UI_Manager ui_manager;
    private Game_Controller game_controller;
    private Image[] images;
    private Color[] max_colors;

    // Use this for initialization
    void Awake () {
        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        ui_manager = gc.GetComponent<UI_Manager>();
        game_controller = gc.GetComponent<Game_Controller>();

        fade_time = ui_manager.fade_time;
        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];
        
        for (int i = 0; i < images.Length; i++)
        {
            max_colors[i] = images[i].color;
        }

        Transform temp_transform;
        Transform buttons_transform = transform.FindChild("Buttons");
        Transform texts_transform = transform.FindChild("Texts");
        for (int i = 0; i < n_buttons; i++)
        {
            temp_transform = buttons_transform.FindChild("Button" + i.ToString());
            act[i] = temp_transform.FindChild("Act").gameObject;
            deact[i] = temp_transform.FindChild("Deact").gameObject;

            texts[i] = texts_transform.FindChild("Text" + i.ToString()).GetComponent<Image>();
            
            SetActive(false, i);
        }

        angles = Utilities.DeepCopy(default_angles);
	}

    void OnEnable ()
    {
        StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors));
    }

    void Update ()
    {
        if (!ui_manager.Get_Validating())
        {
            h = Input.GetAxis("Horizontal");
            v = Input.GetAxis("Vertical");
            theta = Mathf.Atan2(v, h) * Mathf.Rad2Deg;

            if (new Vector2(h, v).magnitude >= .5)
            {
                if (!is_new)
                {
                    SetActive(false, i_selected);
                    angles[i_selected] = default_angles[i_selected] - damp;
                    angles[i_selected + 1] = default_angles[i_selected + 1] + damp;
                }

                is_new = false;

                for (int i = 0; i < n_buttons; i++)
                {
                    if ((theta >= angles[i] && theta <= angles[i + 1]) || (theta >= angles[i] + 360 && theta <= angles[i + 1] + 360) || (theta >= angles[i] - 360 && theta <= angles[i + 1] - 360))
                    {
                        i_selected = i;
                    }
                }

                SetActive(true, i_selected);
            }
            else
            {
                if (!is_new)
                {
                    is_new = true;
                    angles = Utilities.DeepCopy(default_angles);
                    SetActive(false, i_selected);
                    i_selected = -1;
                }
            }

            if (i_selected != -1)
            {
                if (Input.GetButtonDown("Submit"))
                {
                    Submit();
                }
            }
        }
    }
	
    private void Submit()
    {
        switch (i_selected)
        {
            case (0):
                ui_manager.Toggle("inventory");
                break;

            case (1):
                //i_selected = -1;
                game_controller.Exit();
                break;

            case (2):
                SceneManager.LoadScene(0);
                ui_manager.Toggle("options");
                break;

            case (3):
                ui_manager.Toggle("controls");
                break;

            case (4):
                ui_manager.Toggle("game");
                break;

            case (5):
                //i_selected = -1;
                ui_manager.Toggle("reload");
                break;

            default:
                throw new System.Exception("Selected action index '" + i_selected.ToString() + "' undefined.");
        }
    }

    private void SetActive(bool is_active, int i)
    {
        Color color;
        if (is_active)
        {
            color = Color.white;
        }
        else
        {
            color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        }

        foreach (RectTransform child in act[i].transform)
        {
            child.GetComponent<Image>().color = color;
        }
        texts[i].color = color;
    }

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
