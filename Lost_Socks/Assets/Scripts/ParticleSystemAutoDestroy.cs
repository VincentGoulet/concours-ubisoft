﻿using UnityEngine;
using System.Collections;

public class ParticleSystemAutoDestroy : MonoBehaviour
{
    private ParticleSystem ps;
    float end_time;

    public void Start()
    {
        ps = GetComponent<ParticleSystem>();
        end_time = Time.time + ps.startLifetime + ps.duration;
    }

    public void Update()
    {
        if (Time.time >= end_time)
        {
            Destroy(gameObject);
        }
    }
}