﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawn_Dictionnary : MonoBehaviour
{
    public int num_facile;
    public int num_medium;
    public int num_hard;
    public List<Transform> wait_list;
    //[HideInInspector]
    public int sock_counterF, sock_counterM, sock_counterH;
    public bool is_busy;
    private Vector3 entree_position;
    private Zone_System zone_system;

    void Start()
    {
        is_busy = false;
        entree_position = transform.FindChild("Zone_entree").position;
        wait_list = new List<Transform>();
        zone_system = GameObject.Find("Zone_System 3").GetComponent<Zone_System>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            if (other.GetComponent<Socks_Controller>().transport_fruit)
            {
                //Enter_Drop_Zone(other.transform);
            }
        }
        if (other.gameObject.tag == "Waypoint")
        {
            Attribution_To_Zone();
        }
    }

    void Attribution_To_Zone()
    {
        Transform zone = zone_system.Get_Closer_Zone(transform.position);
        zone.GetComponent<Zone_Controller>().hideout_zone = true;
        zone.GetComponent<Zone_Controller>().hideout_transform = transform.FindChild("Zone_entree").transform;
    }

    /*void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            Exit_Drop_Zone();
        }
    }
    
    public void Enter_Drop_Zone(Transform sock_transform)
    {
        if (!is_busy)
        {
            is_busy = true;
        }
        else
        {
            sock_transform.GetComponent<Socks_Controller>().nav.SetDestination(sock_transform.position);
            //sock_transform.GetComponent<Socks_Controller>().wait_to_drop = true;
            wait_list.Add(sock_transform);
        }
    }

    public void Exit_Drop_Zone()
    {
        if (wait_list.Count > 0)
        {
            wait_list[0].GetComponent<Socks_Controller>().nav.SetDestination(entree_position);
            // wait_list[0].GetComponent<Socks_Controller>().wait_to_drop = false;
        }
        else
        {
            is_busy = false;
        }
    }*/
}