﻿using UnityEngine;
using System;

public class Save_Load : MonoBehaviour {
    public int index;
    private Checkpoint_Manager save_manager;
    
	// Use this for initialization
	void Awake ()
    {
        save_manager = GameObject.Find("Game Controller").GetComponent<Checkpoint_Manager>();	
	}

    void OnTriggerEnter(Collider other)
    {    
        // Deep save    
        if (other.tag == "Player")
        {
            /*string a = gameObject.name[0].ToString();
            save_manager.Save(Int32.Parse(a));*/
            //gameObject.SetActive(false);
            save_manager.Set_Checkpoint(index);
        }
    }
}
