﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Geyser_Controller : MonoBehaviour {
    private int index;
    private Object_Dictionary dictionary;
    private ParticleSystem particle_system;
    private ParticleSystem.Particle[] particles;
    private ParticleSystem.Particle particle;
    private Transform raft;
    private Vector3 raft_origin;

    public float height = 5;
    private float v0;
    private float g;
    private Rigidbody rb_raft;
    private float max_y;
    bool is_squirting = false;
    private float duration;
    private Transform water_transform;

    public GameObject ps_prefab;
    private ParticleSystem ps;

    private SoundManager sounds;

    public bool geyser_is_on_volcan = false;

    // Use this for initialization
    void Start () {
        water_transform = dictionary.Get_Water().transform;
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        //particle_system = gameObject.GetComponentInChildren<ParticleSystem>();
        //particles = new ParticleSystem.Particle[particle_system.maxParticles];
        raft = transform.Find("Raft");
        raft_origin = raft.position;
        rb_raft = raft.GetComponent<Rigidbody>();
        //rb_raft.isKinematic = true;
        max_y = 15 * 1.05f;

        /*v0 = Mathf.Sqrt(2 * g * height);
        v0 = particle_system.startSpeed;*/

        //ps_prefab = (GameObject)Resources.Load("Prefabs/Jet", typeof(GameObject));
        ps = ps_prefab.GetComponent<ParticleSystem>();
        g = Mathf.Abs(Physics.gravity.y) * ps.gravityModifier;
        duration = ps.duration + ps.startLifetime - 1.1f;
    }

    public IEnumerator Stop()
    {
        //print("Stop Squirt");
        if (ps != null)
        {
            ps.Stop();
            yield return new WaitForSeconds(ps.startLifetime - 1.0f);
            rb_raft.isKinematic = false;
        }
    }

    public void Actually_Squirt()
    {
        StartCoroutine(sounds.play_geyser());
        Vector3 new_velocity = Vector3.zero;
        ps = ((GameObject)Instantiate(ps_prefab, transform.position, ps_prefab.transform.rotation)).GetComponent<ParticleSystem>();
        //new_velocity.y = Mathf.Sqrt(2 * g * (max_y - raft.position.y));
        new_velocity.y = Mathf.Sqrt(2 * g * height);
        //print(max_y - raft.position.y);
        //print(new_velocity.y);
        rb_raft.velocity = new_velocity;
    }

    public IEnumerator Squirt()
    {
        //print(ps);
        //yield return new WaitForSeconds(1.0f);
        if (ps == null)
        {
            
            rb_raft.isKinematic = false;
            float start_time = Time.time;
            Actually_Squirt();

            float last_y = raft.position.y - 1.0f;
            while (last_y <= raft.position.y)
            {
                last_y = raft.position.y;
                yield return null;                
            }

            rb_raft.isKinematic = true;

            yield return new WaitForSeconds(start_time + ps.duration + ps.startLifetime - 1.0f - Time.time);
            rb_raft.isKinematic = false;
            yield return null;
        }
        else if (!ps.isPlaying)
        {
            rb_raft.isKinematic = false;
            float start_time = Time.time;
            Actually_Squirt();

            float last_y = raft.position.y - 1.0f;
            while (last_y <= raft.position.y)
            {
                last_y = raft.position.y;
                yield return null;
            }
            Vector3 new_position = raft.position;
            new_position.y = last_y;
            raft.position = new_position;
            rb_raft.isKinematic = true;

            //print(start_time + ps.duration + ps.startLifetime - 1.0f - Time.time);
            yield return new WaitForSeconds(start_time + ps.duration + ps.startLifetime - 1.0f - Time.time);
            rb_raft.isKinematic = false;
            yield return null;
        }

        /*float timer = 0.0f;
        //StartCoroutine(sounds.play_geyser());
        while (!rb_raft.isKinematic)
        {
            y = v0 * timer - .5f * g * timer * timer;
            raft.position.y
            if (raft.position.y < max_y - .001)
            {
            }

            timer += Time.deltaTime;
            yield return null;
        }

        timer += Time.deltaTime;

        /*rb_raft.isKinematic = false;
        max_y = raft_origin.y - 1.0f;

        Vector3 velocity = new Vector3(0.0f, ps.startSpeed * .5f, 0.0f);
        raft.GetComponent<Rigidbody>().velocity = velocity;

        float timer = 0.0f;
        //;
        while (!rb_raft.isKinematic)
        {

            max_y = Mathf.Max(max_y, raft.position.y);

            if (raft.position.y < max_y - .001)
            {
                raft.position = new Vector3(raft.position.x, max_y, raft.position.z);
                rb_raft.isKinematic = true;
            }

            timer += Time.deltaTime;
            yield return null;
        }

        timer += Time.deltaTime;
        yield return new WaitForSeconds(duration - timer);
        
        rb_raft.isKinematic = false;

        while (raft.position.y > raft_origin.y)
        {
            yield return null;
        }
        raft.position = raft_origin;
        rb_raft.isKinematic = true;*/
        //StopCoroutine(sounds.play_geyser());*/
        //yield return new WaitForSeconds(6.0f);
    }

    public void Setup (int i, Object_Dictionary _dictionary)
    {
        index = i;
        dictionary = _dictionary;
    }
}
