﻿using UnityEngine;
using System.Collections;

public class Spray_Controller : MonoBehaviour {

    private ParticleSystem spray_particule;
    private float spray_trigger;
    private float spray_timer;
    private bool spray_used;

    public float spray_cooldown = 2.0f;
    



    //Audio variables
    private SoundManager sounds;
    


    // Use this for initialization
    void Start ()
    {
        
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        spray_particule = GetComponent<ParticleSystem>();
        spray_used = false;
        spray_timer = 0.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        spray_trigger = Input.GetAxis("Spray");

        if (spray_timer >= 0.0f)
        {
            spray_timer -= Time.deltaTime;
        }
        if (spray_timer <= 0.0f)
        {
            if (spray_trigger == 0)
            {
                spray_used = false;
            }
            if (spray_trigger != 0 && spray_used == false)
            {
                StartCoroutine(WaitSpray());
                /*spray_particule.Play();
                spray_used = true;
                spray_timer = spray_cooldown;
                sounds.play_spray();*/
            }
        }
	}
    IEnumerator WaitSpray()
    {
        yield return new WaitForSeconds(.1f);
        spray_particule.Play();
        spray_used = true;
        spray_timer = spray_cooldown;
        sounds.play_spray();
    }
}
