﻿using UnityEngine;
using System.Collections;

public class Object_Dictionary : MonoBehaviour
{
    // Major objects
    private GameObject player;
    private GameObject trees;
    private GameObject washers;
    private GameObject terriers;
    private GameObject geysers;
    private GameObject water;
    private GameObject spray;
    private GameObject rafts;
    GameObject checkpoints;
    private Camera cam;

    // Objects lists
    private Tree_Controller[] tree_controllers;
    private Washer_Controller[] washer_controllers;
    private Geyser_Controller[] geyser_controllers;
    private Raft_Controller[] raft_controllers;
    private StatePatternBird[] bird_state_machines;
    private GameObject[] spawn_points;
    private GameObject[] check_points;
    private GameObject[] fruits;
    private GameObject geyser;
    private Game_Controller gc;
    private Water_Controller water_controller;
    private Player_Movement player_movement;
    private Player_Health player_health;
    private Animator player_anim;
    private Inventory_Manager inventory;
    private Checkpoint_Manager save_manager;
    private SpawnManager spawn_manager;
    private GameObject birds;
    private GameObject raft;
    private Zone_System zone_system;

    //Counters
    private int n_trees;
    private int n_washers;
    private int n_terriers;
    private int n_geysers;
    private int n_checkpoints;
    private int next_fruit_index = 0;
    private int n_birds;
    private int n_rafts;

    //Switches
    private bool is_max_fruit = false;

    // Clone lists
    private GameObject[] tree_clone_list;

    // Prefabs
    private GameObject prefab_fruit;
    private GameObject prefab_smallRock;

    void Awake()
    {
        birds = GameObject.Find("Birds");
        player = GameObject.Find("Player");
        trees = GameObject.Find("Trees");
        washers = GameObject.Find("Washers");
        geysers = GameObject.Find("Geysers");
        terriers = GameObject.Find("Terriers");
        water = GameObject.Find("Water");
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        spray = player.transform.Find("Spray Collider").gameObject;
        checkpoints = GameObject.Find("Checkpoints");
        rafts = GameObject.Find("Rafts");
        zone_system = GameObject.Find("Zone_System 3").GetComponent<Zone_System>();

        prefab_fruit = (GameObject)Resources.Load("Prefabs/Fruit", typeof(GameObject));

        prefab_smallRock = (GameObject)Resources.Load("Prefabs/SmallRock", typeof(GameObject));

        player_movement = player.GetComponent<Player_Movement>();
        player_health = player.GetComponent<Player_Health>();
        
        inventory = player.GetComponent<Inventory_Manager>();
        
        gc = gameObject.GetComponent<Game_Controller>();
        spawn_manager = gc.GetComponent<SpawnManager>();
        save_manager = gc.GetComponent<Checkpoint_Manager>();
        water_controller = water.GetComponent<Water_Controller>();
        fruits = new GameObject[gc.max_fruit];
        player_anim = player.GetComponent<Animator>();

        int i = 0;
        // Setup Geysers
        n_geysers = geysers.transform.childCount;
        geyser_controllers = new Geyser_Controller[n_geysers];
        
        foreach (Transform child in geysers.transform)
        {
            geyser = child.gameObject;
            geyser_controllers[i] = child.GetComponent<Geyser_Controller>();
            geyser_controllers[i].Setup(i, this);
            i++;
        }
        // Setup Birds
        n_birds = birds.transform.childCount;
       
        bird_state_machines = new StatePatternBird[n_birds];

        GameObject bird;
        i = 0;
        foreach (Transform child in birds.transform)
        {
            bird = child.gameObject;
            bird_state_machines[i] = child.GetComponent<StatePatternBird>();
            i++;
        }

        // Setup Water
        water_controller.Setup(this);

        // Setup trees
        n_trees = trees.transform.childCount;
        tree_controllers = new Tree_Controller[n_trees];

        i = 0;
        foreach (Transform child in trees.transform)
        {
            tree_controllers[i] = child.GetComponent<Tree_Controller>();
            tree_controllers[i].Setup(i, this, gc.spawn_probability, gc.spawn_delay);
            i++;
        }

        // Setup washers
        n_washers = washers.transform.childCount;
        washer_controllers = new Washer_Controller[n_washers];

        i = 0;
        foreach (Transform child in washers.transform)
        {
            washer_controllers[i] = child.GetComponent<Washer_Controller>();
            washer_controllers[i].Setup(i, this);
            i++;
        }

        //Setup Terrier
        n_terriers = terriers.transform.childCount;
        spawn_points = new GameObject[n_terriers];

        i = 0;
        foreach (Transform child in terriers.transform)
        {
            spawn_points[i] = child.gameObject;
            i++;
        }
        spawn_manager.Setup(spawn_points);

        //Setup Checkpoints
        n_checkpoints = checkpoints.transform.childCount;
        check_points = new GameObject[n_checkpoints];

        i = 0;
        foreach (Transform child in checkpoints.transform)
        {
            check_points[i] = child.gameObject;

            i++;
        }

        //Setup Rafts
        n_rafts = rafts.transform.childCount;
        raft_controllers = new Raft_Controller[n_rafts];
        i = 0;
        foreach (Transform child in rafts.transform)
        {
            raft = child.gameObject;
            raft_controllers[i] = child.GetComponent<Raft_Controller>();
            raft_controllers[i].Setup(i, this);
            i++;
        }
    }
    public GameObject[] Get_Checkpoints()
    {
        return check_points;
    }
    public Washer_Controller Get_Washer_Controller(int index)
    {
        return washer_controllers[index];
    }
    public GameObject Instantiate_smallRock(Transform t)
    {
        GameObject new_rock = prefab_smallRock;
        new_rock.transform.position = t.position;
        return new_rock;
    }
    public GameObject Instantiate_Fruit(Vector3 position, int tree_index, int position_index)
    {
        GameObject new_fruit = (GameObject)Instantiate(prefab_fruit, position, Quaternion.identity);
        if (fruits[next_fruit_index] == null)
        {
            fruits[next_fruit_index] = new_fruit;
        }
        else
        {
            // DO NOT REMOVE! These only appear when there is an error.
            print("Next fruit index:");
            print(next_fruit_index);
            print("Fruit object references:");
            for (int i = 0; i < fruits.Length; i++)
            {
                print(fruits[i]);
            }

            throw new System.Exception("It tried to instantiate over a fruit that already exists");
        }

        fruits[next_fruit_index].GetComponent<Fruit_Controller>().Setup(next_fruit_index, tree_index, position_index, gc.spawn_delay, this);

        is_max_fruit = true;

        for (int i = 0; i < gc.max_fruit; i++)
        {
            if (fruits[i] == null)
            {
                next_fruit_index = i;
                is_max_fruit = false;
                break;
            }
        }

        return new_fruit;
    }

    public bool Get_Is_Max_Fruit()
    {
        return is_max_fruit;
    }

    public void Destroy_Fruit(int world_index, int tree_index, int position_index)
    {
        Destroy(fruits[world_index]);
        fruits[world_index] = null;
        next_fruit_index = world_index;
        is_max_fruit = false;
    }

    /*public Climb_Controller Get_Climb_Controller(int index)
    {
        return climb_controllers[index];
    }*/

    public GameObject Get_Player()
    {
        return player;
    }

    public Tree_Controller[] Get_Tree_Controllers()
    {
        return tree_controllers;
    }

    public Tree_Controller Get_Tree_Controller(int tree_index)
    {
        return tree_controllers[tree_index];
    }

    public Camera Get_Camera()

    {
        return cam;
    }

    public Water_Controller Get_Water_Controller()
    {
        return water_controller;
    }

    public Player_Movement Get_Player_Movement()
    {
        return player_movement;
    }
    public Player_Health Get_Player_Health()
    {
        return player_health;
    }

    public Inventory_Manager Get_Inventory()
    {
        return inventory;
    }
    public GameObject Get_Spray()
    {
        return spray;
    }

    public GameObject[] Get_Spawn_Points()
    {
        return spawn_points;
    }

    public Geyser_Controller[] Get_Geyser_Controllers()
    {
        return geyser_controllers;
    }

    public GameObject Get_Water()
    {
        return water;
    }
    public GameObject Get_Geyser()
    {
        return geyser;
    }
    public Fruit_Controller Get_Fruit_Controller(int i)
    {
        return fruits[i].GetComponent<Fruit_Controller>();
    }

    public Game_Controller Get_Game_Controller()
    {
        return gc;
    }
    
    public Raft_Controller[] Get_Raft_Controller()
    {
        return raft_controllers;
    }

    public Bubble_Popper Get_Bubble_Popper()
    {
        return gc.ui_manager.Get_Bubble_Popper();
    }
    public SpawnManager Get_Spawn_Manager()
    {
        return spawn_manager;
    }
    public UI_Manager Get_UI_Manager()
    {
        return gc.ui_manager;
    }
    public Zone_System Get_Zone_System()
    {
        return zone_system;
    }

    public void Crash_Rock(Collider other)
    {
        StartCoroutine(other.GetComponent<Rock_Controller>().Crash_Rock());
    }

    public Checkpoint_Manager Get_Checkpoint_Manager()
    {
        return save_manager;
    }
}

