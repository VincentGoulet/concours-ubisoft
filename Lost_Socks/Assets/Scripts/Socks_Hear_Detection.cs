﻿using UnityEngine;
using System.Collections;

public class Socks_Hear_Detection : MonoBehaviour
{
    private Socks_Controller socks_controller;

    void OnTriggerEnter(Collider other)
    {
        GameObject object_detected = other.gameObject;
        if(object_detected.tag == ("Socks"))
        {
            socks_controller = object_detected.GetComponent<Socks_Controller>();
            if(socks_controller.is_alive)
            {
                //socks_controller.state = Socks_Controller.State.is_fleeing;
                socks_controller.Set_Fleeing_Destination(transform.position);
            }
        }
    }
}
