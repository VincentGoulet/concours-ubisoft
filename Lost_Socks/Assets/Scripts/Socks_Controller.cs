﻿using UnityEngine;
using System.Collections;

public class Socks_Controller : MonoBehaviour
{
    public string type;
    public enum State
    {
        is_fleeing,
        is_idle,
        is_walking,
        look_for_fruit,
        retriever_fruits,
        is_stunned
    }

    public State state;
    public bool is_alive;
    public bool transport_fruit;
    public bool wait_to_drop;
    public bool is_moving;
    public bool is_poisoned;
    public bool is_taken;

    public float walking_speed = 3.5f;
    public float transport_speed = 2.0f;
    public float running_speed = 7.0f;
    public float poisoned_acceleration = 1.0f;
    public float poisoned_run_speed = 4.5f;
    public float poisoned_walk_speed = 2.0f;
    public float walking_acceleration = 3.0f;
    public float running_acceleration = 6.0f;
    public Transform present_zone;
    public Transform next_zone;
    public Vector3 next_destination;
    public Vector3 sock_position;

    [HideInInspector]
    public float player_view_field;
    private Transform fruit_target;
    private string fruit_target_name;

    public float poisoned_time = 60.0f;
    public float fleeing_time = 10.0f;
    public float drop_fruit_time = 2.0f;
    public float take_fruit_time = 1.5f;
    public float stunned_time = 2.0f;
    public float drowing_time = 10.0f;

    private float drowing_timer;
    private float poisoned_timer;
    private float fleeing_timer;
    public float drop_fruit_timer;
    private float take_fruit_timer;
    private float stunned_timer;
    private float time_to_rest;

    // private Vector3 flee_direction;
    public Vector3 flee_destination;
    //private Vector3 position_sock;
    [HideInInspector]
    public NavMeshAgent nav;

    private Zone_System zone_system;
    private Inventory_Manager inventory;
    public Animator socks_animation;

    private Vector3 vector_zero = Vector3.zero;

    private float float_zero = 0.0f;

    private GameObject renderer_sock;

    private Object_Dictionary dictionary;

    private SoundManager sounds;
    AudioSource audiosource;
    private enum socks_state { Alerted, Fleeing, Dying}

    //private float time_variation = 0.0f;

    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        audiosource = GetComponent<AudioSource>();
        nav = GetComponent<NavMeshAgent>();
        socks_animation = GetComponent<Animator>();
        zone_system = GameObject.Find("Zone_System 3").GetComponent<Zone_System>();
        inventory = GameObject.Find("Player").GetComponent<Inventory_Manager>();

        present_zone = zone_system.Get_Closer_Zone(transform.position);

        time_to_rest = Random.Range(0, 5);

        player_view_field = 50.0f;

        poisoned_timer = poisoned_time;
        fleeing_timer = fleeing_time;
        drop_fruit_timer = drop_fruit_time;
        take_fruit_timer = take_fruit_time;

        wait_to_drop = false;

        state = State.is_idle;
        socks_animation.SetBool("is_idle", true);
		socks_animation.SetBool("is_dead", false);
        is_alive = true;
        is_moving = false;
        is_poisoned = false;

        spawn_points = dictionary.Get_Spawn_Points();

        StartCoroutine("Check_Position");
    }

    public void Set_Present_Zone(Transform waypoint)
    {
        present_zone = waypoint;
    }

    IEnumerator Check_Position()
    {
        for (;;)
        {
            sock_position = transform.position;
            yield return new WaitForSeconds(5.5f);
            if (sock_position == transform.position && state == State.is_walking)
            {
                time_to_rest = 0;
                state = State.is_idle;
            }
            if (sock_position == transform.position && state == State.is_fleeing)
            {
                Set_New_Fleeing_Destination();
            }
        }
    }

    public GameObject[] spawn_points;

    // Update is called once per frame
    void Update()
    {
        
        if (is_alive)
        {
            Debug.DrawRay(transform.position, transform.forward, Color.green);
            if (is_poisoned)
            {
                poisoned_timer -= Time.deltaTime;
                if (poisoned_timer < 0)
                {
                    socks_animation.SetBool("is_poisoned", false);
                    is_poisoned = false;
                    poisoned_timer = poisoned_time;
                }
            }
            switch (state)
            {
                case State.is_walking:
                    StopCoroutine(play_fleeing_sound());
                    Debug.DrawLine(transform.position, next_destination);
                    if (nav.remainingDistance == 0)
                    {
                        socks_animation.SetBool("is_walking", false);
                        socks_animation.SetBool("is_idle", true);
                        is_moving = false;
                        Random_Rest();
                        state = State.is_idle;
                    }
                    break;

                case State.look_for_fruit:
                    StopCoroutine(play_fleeing_sound());
                    if (!fruit_target.GetComponent<Fruit_Controller>().Is_Pickable() && !transport_fruit )
                    {
                        //check animation
                        socks_animation.SetBool("is_walking", false);
                        socks_animation.SetBool("is_idle", true);
                        fruit_target = null;
                        Random_Rest();
                        state = State.is_idle;
                        break;
                    }
					if (is_moving && nav.remainingDistance == 0)
                    {

                        take_fruit_timer -= Time.deltaTime;
                        if (!transport_fruit)
                        {
                            fruit_target.transform.parent = transform;
                            socks_animation.SetTrigger("take_fruit");
                            fruit_target.GetComponent<Fruit_Controller>().is_take = true;
                            transport_fruit = true;
                        }
                        if (take_fruit_timer <= 1.0f)
                        {
                            //Debug.Log("Enter");
                            fruit_target.transform.localPosition = Vector3.Lerp(transform.FindChild("Fruit_Anchor").transform.localPosition, fruit_target.transform.localPosition, take_fruit_timer / (take_fruit_time - 0.5f));
                        }

                        if (take_fruit_timer <= 0)
                        {
                            if (fruit_target.GetComponent<Fruit_Controller>().is_Poisoned)
                            {
                                is_poisoned = true;
                                socks_animation.SetBool("is_poisoned", true);
                                GetComponentInChildren<Renderer>().material.SetColor("_Color", new Color(0.5f, .85f, 0.5f));
                                dictionary.Get_Bubble_Popper().Pop_Poison(transform);
                            }
                            
                            fruit_target.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                            //fruit_target.tag = "Untagged";
                            fruit_target.transform.localPosition = transform.FindChild("Fruit_Anchor").transform.localPosition;
                            socks_animation.SetBool("is_walking", true);
                            //next_waypoint = zone_system.Get_Hideout(transform.position);
                            // Vector3 hideout_position = next_waypoint.GetComponent<Zone_Controller>().hideout_transform.position;
                            Vector3 hideout_position = spawn_points[my_terrier].transform.position;
                            //   zone_system.Get_Hideout(transform.position);
                            //Debug.Log(transform.gameObject.name + " " + hideout_position);
                            nav.SetDestination(hideout_position - 1.0f * Vector3.Normalize(hideout_position - transform.position));
                            nav.speed = transport_speed;
                            take_fruit_timer = take_fruit_time;
                            state = State.retriever_fruits;
                        }
                    }
                    else
                    {
                        nav.SetDestination(fruit_target.position - 0.75f * Vector3.Normalize(fruit_target.position - transform.position));
                        is_moving = true;
                    }
                    break;

                case State.retriever_fruits:
                    StopCoroutine(play_fleeing_sound());
                    if (nav.remainingDistance <= 1)
                    {
                        //    Debug.Break();
                        if (fruit_target == null)
                            transport_fruit = false;
                        else
                        {
                            //fruit_target.tag = "Fruits";
                            //transform.FindChild(fruit_target.name).transform.parent = null;
                            fruit_target.parent = null;
                            //nav.Stop();
                            nav.SetDestination(transform.position);
                            socks_animation.SetTrigger("drop_fruit");

                            if (transport_fruit)//&& drop_fruit_timer <= 2.0f)
                            {
                                //       fruit_target.transform.localPosition 

                                fruit_target.transform.localPosition = Vector3.Lerp(fruit_target.transform.position, spawn_points[my_terrier].transform.position, 0.8f * Time.deltaTime);
                                if (fruit_target.localScale.x > 0.1 && fruit_target.localScale.y > 0.1 && fruit_target.localScale.z > 0.1)
                                    fruit_target.localScale -= new Vector3(0.01F, 0.01f, 0.01f) *5;

                                if (Vector3.Distance(fruit_target.position, spawn_points[my_terrier].transform.position) < 0.1f)
                                {
                                    StartCoroutine(fruit_target.GetComponent<Fruit_Controller>().destroy_fruit());
                                    transport_fruit = false;
                                }
                            }
                            NavMeshAgent bob;
                        }
                        if (transport_fruit)
                        {
                            socks_animation.SetBool("is_walking", false);
                        }
                        else
                        {
                            //nav.Resume();
                            state = State.is_idle;
                        }
                        /*   
                               
                            }*/
                        //  drop_fruit_timer -= Time.deltaTime;
                        /* if (drop_fruit_timer <= 0)
                         {
                             Debug.Log(gameObject.name);
                             Debug.Break();
                             socks_animation.SetBool("is_idle", true);
                             Random_Rest();
                             drop_fruit_timer = drop_fruit_time;
                             state = State.is_idle;
                             Drop_Fruit();
                         }
                         if (transport_fruit )//&& drop_fruit_timer <= 2.0f)
                         {
                             Debug.Log(gameObject.name);
                             Debug.Break();
                             fruit_target.transform.localPosition = Vector3.Lerp(spawn_points[my_terrier].transform.position, transform.FindChild("Fruit_Anchor").transform.position, take_fruit_timer / (take_fruit_time - 0.5f));
                         }
                     }*/
                    }
                    break;

                case State.is_idle:
                    StopCoroutine(play_fleeing_sound());
                    time_to_rest -= Time.deltaTime;
                    if (time_to_rest <= 0)
                    {
                        next_zone = zone_system.Get_Socks_Next_Zone(present_zone, false);
                        next_destination = Random.insideUnitSphere * next_zone.GetComponent<Zone_Controller>().zone_size + next_zone.position;
                        //next_destination.y = 0;
                        if (next_zone.GetComponent<Zone_Controller>().cave_zone)
                        {
                            next_destination = Position_Ground_Height_Up(next_destination);
                        }
                        else
                        {
                            next_destination = Position_Ground_Height_Down(next_destination);
                        }

                        nav.SetDestination(next_destination);
                        is_moving = true;
                        socks_animation.SetBool("is_walking", true);
                        socks_animation.SetBool("is_idle", false);
                        state = State.is_walking;
                        Set_Speed_Acceleration();
                    }
                    break;

                case State.is_fleeing:
                    
                    #region Fleeing
                    Debug.DrawLine(transform.position, flee_destination);
                    fleeing_timer -= Time.deltaTime;
                    if (fleeing_timer <= 0 && nav.remainingDistance == 0)
                    {
                        player_view_field = 50.0f;
                        fleeing_timer = fleeing_time;
                        socks_animation.SetBool("is_fleeing", false);
                        socks_animation.SetBool("is_idle", true);
                        state = State.is_idle;
                        
                        break;
                    }

                    if (present_zone.GetComponent<Zone_Controller>().hideout_zone)
                    {
                        nav.SetDestination(present_zone.GetComponent<Zone_Controller>().hideout_transform.position);
                        if (is_moving && nav.remainingDistance == 0)
                        {
                            //nav.SetDestination(present_waypoint.GetComponent<Zone_Controller>().hideout_transform.position);
                            present_zone.GetComponent<Zone_Controller>().Del_Socks();
                            
                            Destroy(gameObject);
                            break;
                        }
                    }
                    if (is_moving && nav.remainingDistance == 0)
                    {
                        
                        Set_New_Fleeing_Destination();
                    }
                    break;
                #endregion

                case State.is_stunned:
                    /*stunned_timer -= Time.deltaTime;
                    if (stunned_timer <= 0)
                    {
                        stunned_timer = stunned_time;
                        state = State.is_idle;
                        time_to_rest = 0;
                        
                    }*/
                    break;
            }

        }
    }

    //public void Stun_Sock()

    Vector3 Position_Ground_Height_Down(Vector3 initial_position)
    {
        Vector3 ray_start = new Vector3(initial_position.x, 1000, initial_position.z);
        RaycastHit[] hits;
        //Ray vertical_ray = new Ray(ray_start, Vector3.up);
        hits = Physics.RaycastAll(ray_start, Vector3.down);
        Vector3 maximal_height = new Vector3(initial_position.x, 0, initial_position.z);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag == "Ground")
            {
                if (hits[i].point.y > maximal_height.y)
                {
                    maximal_height = hits[i].point;
                }
            }
        }
        return maximal_height;
    }

    Vector3 Position_Ground_Height_Up(Vector3 initial_position)
    {
        Vector3 ray_start = new Vector3(initial_position.x, 0, initial_position.z);
        RaycastHit[] hits;
        //Ray vertical_ray = new Ray(ray_start, Vector3.up);
        hits = Physics.RaycastAll(ray_start, Vector3.up);
        Vector3 minimal_height = new Vector3(initial_position.x, 1000, initial_position.z);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag == "Ground")
            {
                if (hits[i].point.y < minimal_height.y)
                {
                    minimal_height = hits[i].point;
                }
            }
        }
        return minimal_height;
    }

    public void Set_Fruit_Target(Transform fruit_transform)
    {
        fruit_target = fruit_transform;
        socks_animation.SetBool("is_walking", false);
        state = State.look_for_fruit;
    }

    /*public IEnumerator Drowning()
    {
        for (;;)
        {
            drowing_timer -= Time.deltaTime;
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, drowing_timer / drowing_time);
            yield return new WaitForEndOfFrame();
            if (drowing_timer <= 0)
            {
                Destroy(gameObject);
            }
        }
    }*/

    void Set_Speed_Acceleration()
    {
        if (is_poisoned)
        {
            nav.acceleration = poisoned_acceleration;
            
            if (state == State.is_fleeing)
            {
                nav.speed = poisoned_run_speed;
            }
            if (state == State.is_walking)
            {
                nav.speed = poisoned_walk_speed;
            }
        }
        else
        { 
            if (state == State.is_fleeing)
            {
                nav.speed = running_speed;
                nav.acceleration = running_acceleration;
            }
            if (state == State.is_walking)
            {
                nav.speed = walking_speed;
                nav.acceleration = walking_acceleration;
            }
        }
    }

    void Drop_Fruit()
    {
        if (transport_fruit && fruit_target != null)
        {
            //fruit_target.tag = "Fruits";
            fruit_target.parent = null;
            //transform.FindChild(fruit_target.name).transform.parent = null;
            fruit_target.GetComponent<Fruit_Controller>().is_take = false;
            fruit_target.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            transport_fruit = false;
        }
    }

	public void Set_Fleeing_Destination(Vector3 enemy_position)
	{
		if (is_alive /*&& state != State.is_fleeing*/ && state != State.is_stunned) 
		{
			socks_animation.SetBool("is_walking", false);
			socks_animation.SetBool("is_idle", false);
			socks_animation.SetBool("is_fleeing", true);
			socks_animation.SetTrigger("fleeing");
			Drop_Fruit();
			Set_Speed_Acceleration();
			dictionary.Get_Bubble_Popper().Pop_Alert(gameObject.transform);
			sounds.play_socks_sound(audiosource, (int)socks_state.Alerted);
			
			Vector3 flee_direction = (transform.position - enemy_position);
			float random_angle = (2* Random.value - 1) * 90;
			Quaternion vector_rotation = Quaternion.Euler(0f,random_angle, 0f);
			Vector3 new_flee_direction = vector_rotation * flee_direction;
			flee_destination = (transform.position + (new_flee_direction.normalized * 5));

			nav.SetDestination(flee_destination);

			player_view_field = 360.0f;
			is_moving = true;
			state = State.is_fleeing;
			StartCoroutine(play_fleeing_sound());
		}
	}


    public void Set_New_Fleeing_Destination()
    {
        if (is_alive)
        {
            if (present_zone.GetComponent<Zone_Controller>().hideout_zone)
            {
                nav.SetDestination(present_zone.GetComponent<Zone_Controller>().hideout_transform.position);
            }
            else
            {
                next_zone = zone_system.Get_Socks_Next_Zone(present_zone, true);
                flee_destination = Random.insideUnitSphere * next_zone.GetComponent<Zone_Controller>().zone_size + next_zone.position;
                //flee_destination.y = 0;
                if (next_zone.GetComponent<Zone_Controller>().cave_zone)
                {
                    flee_destination = Position_Ground_Height_Up(flee_destination);
                }
                else
                {
                    flee_destination = Position_Ground_Height_Down(flee_destination);
                }
                nav.SetDestination(flee_destination);
            }
        }
    }

    void Random_Rest()
    {
        int take_a_rest = Mathf.RoundToInt(Random.Range(0.0f, 1.0f));
        if (take_a_rest == 1)
        {
            time_to_rest = Mathf.Round(Random.Range(1.0f, 5.0f));
        }
        else
        {
            time_to_rest = 0;
        }
    }
    /*
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Spray")
        {
            if (is_alive)
            {
                is_alive = false;
                nav.SetDestination(transform.position);
                socks_animation.SetTrigger("is_dead");
                if (transport_fruit)
                {
                    Drop_Fruit();
                }
            }
        }
    }
    */

    void OnCollisionEnter(Collision other)
    {
        /*print("oncollision");
        print("other.tag");*/
        if (other.gameObject.tag == "Player") 
		{
			if (!is_alive) 
			{
				present_zone.GetComponent<Zone_Controller> ().Del_Socks ();
				inventory.Add_Sock (type);
                sounds.play_getSock();
				Destroy (gameObject);
			}
		}
        if (other.gameObject.tag == "Terrier")
        {
            if (state == State.is_fleeing)
            {
                present_zone.GetComponent<Zone_Controller>().Del_Socks();
                Destroy(gameObject);
            }
        }
        if (other.gameObject.tag == "Birds")
        {
            /*    if (is_alive)
                {
                    nav.SetDestination(transform.position);
                    state = State.is_stunned;
                }
                if (!is_alive)
                {
                    present_waypoint.GetComponent<Zone_Controller>().Del_Socks();
                    Destroy(gameObject);
                }*/
        }
    }

    public int my_terrier;
    
    public void Set_myTerrier(int nb_terrier)
    {
        my_terrier = nb_terrier;
    }

    public int Get_myTerrier()
    {
        return my_terrier;
    }

    public void Is_Dying()
    {
        is_alive = false;
        dictionary.Get_Bubble_Popper().Pop_Lethal(gameObject.transform);
        nav.SetDestination(transform.position);
        socks_animation.SetTrigger("dying");
		socks_animation.SetBool("is_dead", true);
        StopCoroutine(play_fleeing_sound());
        sounds.play_socks_sound(audiosource, (int)socks_state.Dying);
        if (transport_fruit)
        {
            Drop_Fruit();
        }
    }

    float stun_force = 5000;
    private IEnumerator play_fleeing_sound()
    {
        while (state == State.is_fleeing)
        {
            yield return new WaitForSeconds(1f);
            if (!audiosource.isPlaying)
            {
                sounds.play_socks_sound(audiosource, (int)socks_state.Fleeing);

            }
            yield return null;
        }
        StopCoroutine(play_fleeing_sound());
    }



	public void Pop_And_Stun(/*Vector3 direction*/)
	{
		StartCoroutine(Pop_And_Stun_Coroutine());
	}
	
	public IEnumerator Pop_And_Stun_Coroutine()
	{
		
		nav.SetDestination(transform.position);
		state = State.is_stunned;
		socks_animation.SetBool ("is_idle", false);
		socks_animation.SetBool ("is_walking", false);
		socks_animation.SetBool ("is_fleeing", false);
		socks_animation.SetTrigger("stunned");
		Drop_Fruit ();
        dictionary.Get_Bubble_Popper().Pop_Stun(gameObject.transform);
        yield return new WaitForSeconds(stunned_time);
		time_to_rest = 0;
		socks_animation.SetBool ("is_idle", true);
		state = State.is_idle;
		
	}
}