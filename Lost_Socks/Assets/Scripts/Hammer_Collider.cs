﻿using UnityEngine;
using System.Collections;

public class Hammer_Collider : MonoBehaviour {

	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Rock")
        {
            print("destroy rock");
            GameObject.Find("Game Controller").GetComponent<Object_Dictionary>().Crash_Rock(col.gameObject.GetComponent<Collider>());
            //StartCoroutine(col.gameObject.GetComponent<Rock_Controller>().Crash_Rock());
        }
    } 
}
