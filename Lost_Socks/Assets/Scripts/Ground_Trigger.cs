﻿using UnityEngine;
using System.Collections;

public class Ground_Trigger : MonoBehaviour {
    private Player_Movement player_movement;

    void Start()
    {
        player_movement = GetComponentInParent<Player_Movement>();
    }

    void OnTriggerStay(Collider other)
    {
        switch (other.tag)
        {
            case ("Ground"):
                player_movement.Set_Grounded(true);
                break;

            case ("Raft"):
                player_movement.Set_Grounded(true);
                transform.parent.parent = other.transform;
                break;

            case ("Washer"):
                player_movement.Set_Grounded(true);
                break;
            /*
        case ("Slide"):
            player_movement.MovementManagement(0, 0);
            player_movement.this_rigidbody.AddForce(0, player_movement.slide_speed, 0);
            break;
            */

            default:
                break;
        }
       /* if (other.tag == "Ground")
        {
            player_movement.Set_Grounded(true);
        }
        else if (other.tag == "Raft")
        {
            print("sur ratofjwo");
            player_movement.Set_Grounded(true);
            transform.parent.parent = other.transform;
        } */
    }

    int i = 0;
    void OnTriggerExit (Collider other)
    {
        bool is_ground;
        if (other.tag == "Ground")
        {
            is_ground = player_movement.Set_Grounded(false);
            print(i);
            print(is_ground);
            i++;
        }
        else if (other.tag == "Raft")
        {
            is_ground = player_movement.Set_Grounded(false);
            print(i);
            print(is_ground);
            i++;
            transform.parent.parent = null;
        }
        else if (other.tag == "Washer")
        {
            is_ground = player_movement.Set_Grounded(false);
            print(i);
            print(is_ground);
            i++;
        }
        
    }
}
