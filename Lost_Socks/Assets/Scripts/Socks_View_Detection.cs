﻿using UnityEngine;
using System.Collections;

public class Socks_View_Detection : MonoBehaviour {

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        GameObject object_detected = other.gameObject;

        if (object_detected.tag == ("Socks"))
        {
            Socks_Controller socks_controller = object_detected.GetComponent<Socks_Controller>();
            if (socks_controller.is_alive)
            {
                float socks_view_field = socks_controller.player_view_field;
                Vector3 socks_direction = transform.position - object_detected.transform.position;
                float socks_angle = Vector3.Angle(socks_direction, object_detected.transform.forward);
                //Debug.Log(socks_angle);
                //Debug.Log(socks_view_field);

                if (socks_angle < socks_view_field)
                {

                    //socks_controller.state = Socks_Controller.State.is_fleeing;
                    socks_controller.Set_Fleeing_Destination(transform.position);
                }
            }
        }
    }

}
