﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class TestEvent : MonoBehaviour {

    UnityEvent m_MyEvent;

    void Start()
    {
        if (m_MyEvent == null)
            m_MyEvent = new UnityEvent();

        m_MyEvent.AddListener(Ping);
        m_MyEvent.AddListener(Ping2);

    }

    void Update()
    {
        if (Input.anyKeyDown && m_MyEvent != null)
        {
            m_MyEvent.Invoke();
        }
    }

    void Ping()
    {
        Debug.Log("Ping");
    }
    void Ping2()
    {
        Debug.Log("Ping2");
    }
}
