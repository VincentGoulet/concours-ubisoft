﻿using UnityEngine;
using System.Collections;

public class Hammer_detection_ground : MonoBehaviour
{
    private Transform spawn_dust;
    private SphereCollider sphere_collider;
    private GameObject player;
    GameObject dust;
    SoundManager sounds;
    bool is_particles = false;
    private Object_Dictionary dictionnary;
    private SpawnManager spawn_manager;

    void Start()
    {
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        player = GameObject.Find("Player");
        dust = (GameObject)Resources.Load("Prefabs/HammerDust", typeof(GameObject));
        spawn_dust = transform.FindChild("Spawn_Dust");
        dictionnary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        spawn_manager = GameObject.Find("Game Controller").GetComponent<SpawnManager>();
    }

    /*void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag != "Untagged")
        {

            Instantiate(dust, spawn_dust.position, Quaternion.Euler(-90, 0, 0));
            transform.parent = null;
            StartCoroutine(Thor_Hammer());
        }
    }*/

   /* void OnTriggerEnter(Collider other)
    {
            if (other.tag == "Rock")
            {
                is_particles = true;
                Transform particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.Euler(0, 0, 90) * spawn_dust.rotation)).transform;
                transform.parent = null;
                StartCoroutine(Rock_Hammer());
            }

            if (other.tag == "Water")
            {
                Else_Hammer();
            }

            if (other.tag == "Tree")
            {
                is_particles = true;
                Transform particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.Euler(0, 0, 90) * spawn_dust.rotation)).transform;
                transform.parent = null;
                Else_Hammer();
            }

            if (other.tag == "Ground")
            {
                print(other.name);
                //Debug
                is_particles = true;
                Transform particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.Euler(0, 0, 90) * spawn_dust.rotation)).transform;
                transform.parent = null;
                Else_Hammer();
            }

            if (other.tag == "Wall")
            {
                is_particles = true;
                Transform particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.Euler(0, 0, 90) * spawn_dust.rotation)).transform;
                transform.parent = null;
                Else_Hammer();
            }

            if (other.tag == "Rock")
            {

            }


            // Don't delete cest à JS
            /*if(other.tag == "Untagged")
            {
                Debug.Log("default sound");
                sounds.play_hammer_collide((int)SoundManager.collide_hammer.ground);
            }
            if (other.name.Split('_')[0] == "Washer")
            {
                Debug.Log("washer sound");
                sounds.play_hammer_collide((int)SoundManager.collide_hammer.washer);
            }
            if(other.tag == "Water")
            {
                Debug.Log("water sound");
                sounds.play_hammer_collide((int)SoundManager.collide_hammer.water);
            }
            if (other.name.Split('_')[0] == "Mangrove")
            {
                Debug.Log("tree sound");
                sounds.play_hammer_collide((int)SoundManager.collide_hammer.tree);
            }

        
    }*/
    

    void OnCollisionEnter(Collision col)
    {
        Transform particles;
        print(col.gameObject.name);
         switch (col.gameObject.tag)
        {
            case ("Rock"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                StartCoroutine(Rock_Hammer());
                dictionnary.Crash_Rock(col.gameObject.GetComponent<Collider>());
                break;

            case ("Tree"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Washer"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Bird"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Ground"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Sock"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Wall"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Water"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Fruit"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Raft"):
                is_particles = true;
                particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Terrier"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                StartCoroutine(spawn_manager.HammerSpawn(col.transform));
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Sign"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;

            case ("Nest"):
                is_particles = true;
                //particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                transform.parent = null;
                Else_Hammer();
                break;
        }
       /*
            if (col.gameObject.tag != "Untagged")
            {
                is_particles = true;
                Transform particles = ((GameObject)Instantiate(dust, spawn_dust.position, Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal))).transform;
                
                transform.parent = null;
                StartCoroutine(Rock_Hammer());
                if(col.gameObject.tag == "Rock")
                {
                    dictionnary.Crash_Rock(col.gameObject.GetComponent<Collider>());
                }
            }
            */
       
    }

    IEnumerator Rock_Hammer()
    {
        yield return new WaitForSeconds(1f);
        //print("destroy");
        print(gameObject.name);
        Destroy(gameObject);
        
        //player.GetComponent<Animator>().Play("Idle",-1);
        is_particles = false;
    }
    void Else_Hammer()
    {
        is_particles = false;
    }

    public void Hammer_collide()
    {

    }
}
