﻿using UnityEngine;
using System.Collections;

public class Washer_Controller : MonoBehaviour {
    public bool level;
    private int washer_index;
    private Object_Dictionary dictionary;
    public bool is_elu = false;
    public bool up = false;
    public bool down = false;
    public bool both = false;

    void Start ()
    {
        tag = "Untagged";
    }

    public void Setup(int _washer_index, Object_Dictionary _dictionary)
    {
        washer_index = _washer_index;
        dictionary = _dictionary;
    }

    public int Get_Index()
    {
        return washer_index;
    }

    public bool Get_Level()
    {
        if (up)
        {
            return level;
        }
        if (down)
        {
            return level;
        }
        else // both
        {
            level = !level;
            return level;
        }
    }
}
