﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;
using LitJson;


public class Checkpoint_Manager : MonoBehaviour {
    public Slider loading_bar;
    public GameObject load_screen;
    private bool is_button_down = false;

    private int last_checkpoint;
    private string last_position;
    GameObject water;
    GameObject[] socks;
    GameObject[] spawn_points;
    GameObject player;
    Player_Movement player_mov;
    Inventory_Manager player_inventory;
    SpawnManager spawn_manager;
    Object_Dictionary obj_dict;
    Spawn_Dictionnary spawn_dict;

    Sock[] sock_array;
    Terrier[] terriers;
    Player character;

    Socks_Controller s_controller;
    Water_Controller w_controller;
    Zone_System z_system;

    JsonData socksJson;
    JsonData terriersJson;
    JsonData playerJson;
    JsonData player_data;
    JsonData socks_data;
    JsonData terrier_data;

    string player_string;
    string socks_string;
    string terrier_string;
    JsonData itemJson;

    Vector3 rotation = new Vector3(0, 0, 0);
    bool is_alive;
    bool is_poisoned;
    GameObject rb_sock;
    public bool cr_running;

    int n_checkpoints = 7;
    int checkpoint_reached = 0;
    Transform[] reload_geometries = new Transform[7];
    int[] reload_tides = new int[] { 1, 1, 1, 1, 1, 0, 0};

    void Awake()
    {
        player = GameObject.Find("Player");
        int i = 0;
        foreach (Transform child in GameObject.Find("Reloads").transform)
        {
            reload_geometries[i] = child;
            i++;
        }

        obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        water = GameObject.Find("Water");
    }

    void Start()
    {
        z_system = obj_dict.Get_Zone_System();
        player = obj_dict.Get_Player();
        spawn_manager = obj_dict.Get_Spawn_Manager();
        player_mov = obj_dict.Get_Player_Movement();
        w_controller = obj_dict.Get_Water_Controller();
        player_inventory = obj_dict.Get_Inventory();
        spawn_points = obj_dict.Get_Spawn_Points();
    }

    void Update()
    {
        if (Input.GetButtonDown("RB"))
        {
            if (!is_button_down)
            {                
                //StartCoroutine(Load());
                is_button_down = true;
            }


        }
        else if (is_button_down)
        {
            is_button_down = false;
        }
    }

    public void LoadLevel()
    {
        StartCoroutine(LevelCoroutine());
    }
    IEnumerator LevelCoroutine()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("main");
        load_screen.SetActive(true);

        while (!async.isDone)
        {
            loading_bar.value = async.progress / 0.9f;
            yield return null;
        }
        if (async.isDone)
        {
            //Load();
        }
        if (async.isDone)
        {
            load_screen.SetActive(false);
        }
    }


    public void Save (int checkpoint_index )
    {
        //Save Player
        last_checkpoint = checkpoint_index;
        last_position = player.transform.position.ToString();
        character = new Player(last_position, player_mov.Has_Hammer(), player_mov.Has_Remote(),player_inventory.Get_Inventaire(), w_controller.level, checkpoint_index);
        playerJson = JsonMapper.ToJson(character);
        File.WriteAllText(Application.dataPath + "/Player.json", playerJson.ToString());
        
        //Save Socks
        
        socks = GameObject.FindGameObjectsWithTag("Socks");
        socksJson = "";
        if (socks.Length != 0)
        {
            sock_array = new Sock[socks.Length];
            
            socksJson = null;
            for (int i = 0; i < socks.Length; i++)
            {   
                s_controller = socks[i].GetComponent<Socks_Controller>();
                sock_array[i] = new Sock(s_controller.Get_myTerrier(), s_controller.is_alive, s_controller.is_poisoned, socks[i]);
                
            }
            socksJson = JsonMapper.ToJson(sock_array);
        }
        File.WriteAllText(Application.dataPath + "/Socks.json", socksJson.ToString());

        //Save Terrier
        terriersJson = "";

        terriers = new Terrier[spawn_points.Length];
        for(int j =0; j< spawn_points.Length; j++)
        {
            spawn_dict = spawn_points[j].GetComponent<Spawn_Dictionnary>();
            terriers[j] = new Terrier(spawn_dict.sock_counterF, spawn_dict.sock_counterM, spawn_dict.sock_counterH);
            
        }
        terriersJson = JsonMapper.ToJson(terriers);
        File.WriteAllText(Application.dataPath + "/Terriers.json", terriersJson.ToString());
    }

    public void Save_Inventory()
    {
        character = new Player(last_position, player_mov.Has_Hammer(), player_mov.Has_Remote(), player_inventory.Get_Inventaire(), w_controller.level, last_checkpoint);
        playerJson = JsonMapper.ToJson(character);
        File.WriteAllText(Application.dataPath + "/Player.json", playerJson.ToString());
    }

    public void Set_Checkpoint(int i)
    {
        if (i > checkpoint_reached)
        {
            //print("check reached");
            checkpoint_reached = i;
            StartCoroutine(obj_dict.Get_UI_Manager().Pop_Save());
        }
    }

    public void Load()
    {
        player.GetComponent<Player_Health>().Set_Health(12);
        player.transform.position = reload_geometries[checkpoint_reached].position;
        player.transform.rotation = reload_geometries[checkpoint_reached].rotation;
        player.GetComponent<Player_Movement>().enabled = true;
        Camera.main.GetComponent<Camera_Controller>().Reload();
    }

    public IEnumerator Deep_Load(bool is_done)
    {
        string d_str;
        // player_string = Utilities.init_splitter(File.ReadAllText(Application.dataPath + "/Player.json"),0);
        player_string= File.ReadAllText(Application.dataPath + "/Player.json");
        player_data = JsonMapper.ToObject(player_string);

        //Load Checkpoint state
        int current_checkpoint = Int32.Parse(player_data[1].ToString());
        GameObject[] checkpoints = obj_dict.Get_Checkpoints();
        for(int i = 0; i < current_checkpoint; i++)
        {
            checkpoints[i].SetActive(false);
        }
        yield return new WaitForSeconds(1);
        //Load inventaire du joeur
        int[] inventaire_joueur = obj_dict.Get_Inventory().Get_Inventaire();
        
        for (int j = 0; j < inventaire_joueur.Length; j++)
        {
            inventaire_joueur[j] = Int32.Parse(player_data[2][j].ToString());
        }
        obj_dict.Get_Inventory().score = 0;
        for (int k = 0; k < inventaire_joueur.Length; k++)
        {
            obj_dict.Get_Inventory().score += player_inventory.Get_Score("none", k) * inventaire_joueur[k];
        }
        obj_dict.Get_Game_Controller().ui_manager.Update_Socks(obj_dict.Get_Inventory().score);
        player_mov.Set_Hammer(Convert.ToBoolean(player_data[3].ToString()));
        player_mov.Set_Remote(Convert.ToBoolean(player_data[4].ToString()));
        
        //Load player attributes
        player.transform.position = Utilities.string2vector(player_data[0].ToString());

        player_mov.present_zone = z_system.Get_Closer_Zone(player.transform.position);
        player_mov.enabled = true;
        player.GetComponent<Player_Health>().Set_Health(12);

        //Load tide level
        w_controller.level = Convert.ToBoolean(player_data[5].ToString());
        if (w_controller.level == false)
        {
            water.transform.position= new Vector3(0, 0, 0);
        }
        if (w_controller.level)
        {
            water.transform.position = new Vector3(0, 1.5f, 0);
        }
        //Load Socks
        socks = GameObject.FindGameObjectsWithTag("Socks");
        GameObject sock;
        for(int y = 0; y < socks.Length; y++)
        {
            sock = socks[y];
            Destroy(sock);

        }
        
        yield return new WaitForSeconds(1);
        socks_string = File.ReadAllText(Application.dataPath + "/Socks.json");
        socks_data = JsonMapper.ToObject(socks_string);
        for (int m = 0; m < socks_data.Count; m++)
        {            
                is_alive = Convert.ToBoolean(socks_data[m][1].ToString());
                is_poisoned = Convert.ToBoolean(socks_data[m][2].ToString());
                d_str = socks_data[m][4].ToString();
                rb_sock = Instantiate(spawn_manager.sock_type(socks_data[m][3].ToString()), Utilities.string2vector(d_str), Quaternion.Euler(rotation)) as GameObject;
                s_controller = rb_sock.GetComponent<Socks_Controller>();
                s_controller.Set_myTerrier(Int32.Parse(socks_data[m][0].ToString()));
                s_controller.is_alive = is_alive;
                s_controller.is_poisoned = is_poisoned;
                s_controller.Set_Present_Zone(z_system.Get_Closer_Zone(rb_sock.transform.position));
                s_controller.state = Socks_Controller.State.is_idle;
        }
        
        //Load Terriers
        terrier_string = File.ReadAllText(Application.dataPath + "/Terriers.json");
        terrier_data = JsonMapper.ToObject(terrier_string);

        for(int l = 0; l < terrier_data.Count; l++)
        {
            spawn_dict = spawn_points[l].GetComponent<Spawn_Dictionnary>();
            for (int i = 0; i< terrier_data[l].Count; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            spawn_dict.sock_counterF = Int32.Parse(terrier_data[l][i][0].ToString());
                            break;
                        case 1:
                            spawn_dict.sock_counterM = Int32.Parse(terrier_data[l][i][1].ToString());
                            break;
                        case 2:
                            spawn_dict.sock_counterH = Int32.Parse(terrier_data[l][i][2].ToString());
                            break;
                    }
                }                
            }
        }
        is_done = true;
        yield return is_done;
    }

    IEnumerator Load_Socks()
    {
        Debug.Log("I'm In");
        cr_running = true;
        yield return new WaitForSeconds(1);
        Debug.Log("Im Out");
        cr_running = false;
        
    }

}



 class Sock
{
    public int myterrier;
    public bool is_alive;
    public bool is_poisoned;
    public int type;
    public string position;
    

    public Sock(int my_terrier, bool alive, bool poisoned, GameObject sock)
    {
        myterrier =  my_terrier;
        is_alive = alive;
        is_poisoned = poisoned;
        position = sock.transform.position.ToString();
        type = Utilities.Type2Int(sock.GetComponent<Socks_Controller>().type);
    }
}

 class Player
{
    
    public string position;
    public int checkpoint_num;
    public int[] inventory;
    public bool has_hammer;
    public bool has_remote;
    public bool tide_state;

    public Player(string player_posi, bool has_hammy, bool has_remmy, int[] inventaire, bool tide, int num = -1)
    {     
        checkpoint_num = num;
        position = player_posi;
        has_hammer = has_hammy;
        has_remote = has_remmy;
        inventory = new int[inventaire.Length];
        
        for(int i =0; i < inventaire.Length; i++)
        {
            inventory[i] = inventaire[i];
        }
        tide_state = tide;
    }
}
class Terrier
{
    public int[] terrier;

    public Terrier(int num_facile, int num_med, int num_hard)
    {
        terrier = new int[3];
        terrier[0] = num_facile;
        terrier[1] = num_med;
        terrier[2] = num_hard;
    }
}



