﻿using UnityEngine;
using System.Collections;

public class Rock_on_raft_controller : MonoBehaviour
{
    public Raft_Controller raft_controller;
    
    void OnDestroy()
    {
        raft_controller.Rock_on = false;
    }
}
