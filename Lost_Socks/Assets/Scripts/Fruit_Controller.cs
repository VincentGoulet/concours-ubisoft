﻿using UnityEngine;

using System.Collections;
using UnityEngine.UI;

public class Fruit_Controller : MonoBehaviour
{
    public float spawn_scale = 0.2f;
    public float growth_time = 10.0f;
    public float despawn_time = 40.0f;
    private float remaining_time;
    private float prefab_scale;
    private float growth_speed;
    private float spawn_delay;
    private float time_offset;

    public int world_index;
    private int tree_index;
    private int position_index;

    private Object_Dictionary dictionary;
    private Vector3 scale;
    private string status = "growing";

    public bool is_Poisoned = false;
    public bool is_take = false;
    public bool is_grounded = false;

    private Animator anim_fruit;
    private Animator anim_player;
    private DoneHashIDs hash;
    private GameObject player;

    //Prendre fruit dans les mains
    bool transport_fruit = false;
    float take_fruit_timer;
    float default_pickup_time = .08f;
    float pickup_time;

    //variable health
    private Player_Health player_health;
    public int health_recover = 1;
    private bool is_health = false;
    private bool is_remote_down = false;

    Player_Movement player_movement;

    public void Setup(int _world_index, int _tree_index, int _position_index, float _spawn_delay, Object_Dictionary _dictionary)
    {
        world_index = _world_index;
        tree_index = _tree_index;
        position_index = _position_index;
        spawn_delay = _spawn_delay;
        dictionary = _dictionary;
        player_movement = dictionary.Get_Player_Movement();
    }

    // Use this for initialization

    void Start()
    {
        time_offset = (Random.value - 0.5f) * spawn_delay;
        remaining_time = despawn_time + time_offset;
        growth_time += time_offset;

        prefab_scale = transform.localScale.x;
        scale = Vector3.one * prefab_scale * spawn_scale;
        transform.localScale = scale;

        growth_speed = prefab_scale * (1.0f - spawn_scale) / growth_time;

        //anim_fruit = GetComponent<Animator>();
        player = dictionary.Get_Player();
        anim_player = player.GetComponent<Animator>();
        hash = GameObject.Find("Game Controller").GetComponent<DoneHashIDs>();
        player_health = dictionary.Get_Player_Health();
        
    }

    public bool Is_Pickable()
    {
        return !is_take && (status == "ripe");
    }

    // Update is called once per frame
    void Update()
    {
        if (status == "growing")
        {
            scale += Vector3.one * growth_speed * Time.deltaTime;

            if (scale.x > prefab_scale)
            {
                scale = Vector3.one * prefab_scale;
                status = "ripe";
                gameObject.AddComponent<Rigidbody>();
                gameObject.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Continuous;
                dictionary.Get_Tree_Controller(tree_index).Reset_Position(position_index);
            }

            transform.localScale = scale;
        }

        if (status == "ripe")
        {
            if (is_grounded)
            {
                if (transform.parent != null)
                {
                    if (!dictionary.Get_Game_Controller().do_anim_pick)
                    {
                        if (transform.parent.tag == "Player")
                        {


                            //print(Input.GetButtonDown("Remote"));
                            if (Input.GetButtonDown("Remote"))
                            {
                                //Manger le fruit
                                //print(is_remote_down);
                                if (!is_remote_down)
                                {
                                    //print("Appel la fonction Health");
                                    Health();
                                    StartCoroutine(destroy_fruit());
                                    
                                    is_remote_down = true;
                                }
                            }
                            else
                            {
                                is_remote_down = false;
                            }

                            if (Input.GetButton("Hammer"))
                            {
                                //Lancer le fruit
                                StartCoroutine(Disable_anim_throw());
                            }
                        }
                    }
                }
            }
            else
            {
                remaining_time -= Time.deltaTime;

                if (remaining_time <= 0.0f)
                {
                    dictionary.Destroy_Fruit(world_index, tree_index, position_index);
                }
            }
        }
    }

    public int Get_Index()
    {
        return world_index;
    }

    IEnumerator Disable_anim_throw()
    {
        yield return new WaitForSeconds(0.5f);
        //anim_fruit.SetBool(hash.pickBool, false);
        //anim_fruit.SetBool(hash.throwBool, false);
    }

    public IEnumerator destroy_fruit()
    {
        yield return new WaitForSeconds(0.1f);
        dictionary.Destroy_Fruit(world_index, tree_index, position_index);
    }

    public void Set_is_Take(bool a)
    {
        if (is_take == true && a == false)
        {
            remaining_time = despawn_time;
        }
        is_take = a;

    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Ground")
        {
            is_grounded = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Spray")
        {
            is_Poisoned = true;
            GetComponentInChildren<Renderer>().material.SetColor("_Color", Color.blue);
            dictionary.Get_Bubble_Popper().Pop_Poison(gameObject.transform);
        }
    }

    public IEnumerator Take_Fruit(Transform fruit_transform)
    {
        dictionary.Get_Player_Movement().Freeze(true);
        take_fruit_timer = 0.0f;
        Vector3 final_position;
        Vector3 initial_position;
        float anchor_height = player.transform.FindChild("Fruit_Anchor").transform.localPosition.y;
        float speed = anchor_height / default_pickup_time;
        gameObject.tag = "Untagged";

        if (is_take)
        {
            /*if (!player_movement.is_taking)
            {*/
                yield return new WaitForSeconds(.50f);
                StartCoroutine(dictionary.Get_UI_Manager().Toggle_Controls("Fruit"));
            dictionary.Get_Game_Controller().has_fruit = true;

                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<SphereCollider>().isTrigger = true;
                GetComponent<BoxCollider>().isTrigger = true;
                final_position = dictionary.Get_Player().transform.FindChild("Fruit_Anchor").position;
                initial_position = transform.position;


                pickup_time = (final_position - initial_position).magnitude / speed;
                while (Vector3.Distance(fruit_transform.position, final_position) < 0.003f)
                {
                    yield return null;
                    //take_fruit_timer += Time.deltaTime;
                    fruit_transform.position = Vector3.MoveTowards(fruit_transform.position, final_position, speed * Time.deltaTime);
                    pickup_time -= Time.deltaTime;

                }
                fruit_transform.position = final_position;
                transform.parent = player.transform;
            //}
        }
        else
        {
            GetComponent<BoxCollider>().isTrigger = false;
            GetComponent<SphereCollider>().isTrigger = false;
            gameObject.tag = "Fruits";
            StartCoroutine(dictionary.Get_UI_Manager().Toggle_Controls("Default"));
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down + player.transform.forward * 0.3f, out hit, (anchor_height)))
            {
                //print("physic");
                final_position = hit.point;
            }
            else
            {
                //print("else physic");
                final_position = transform.position - (anchor_height - .3f) * Vector3.up + player.transform.forward * 0.3f;
            }
            //print(final_position);
            initial_position = transform.position;

            take_fruit_timer = 0f;
            while (pickup_time > take_fruit_timer)
            {
                yield return null;
                take_fruit_timer += Time.deltaTime;
                fruit_transform.position = Vector3.MoveTowards(fruit_transform.position, final_position, speed * Time.deltaTime);
               
            }
            fruit_transform.position = final_position;
            transform.parent = null;

            //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            GetComponent<Rigidbody>().isKinematic = false;
            /*
            transform.parent = transform;
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
            is_take = true;
            */
        }
        dictionary.Get_Player_Movement().Freeze(false);
        yield return null;
    }
    
    void Health()
    {
        if (player_health.current_health < 12 && (!is_Poisoned) && is_take)
        {
            //print("fruit non empoisonner");
            player_health.Take_Health(4);
        }
        if (is_Poisoned && is_take)
        {
            //print("fruit empoisonner");
            player_health.TakeDamage(health_recover);
        }
        else
        {
            //print("trop de vie");
        }
    }
}