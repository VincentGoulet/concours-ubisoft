﻿using UnityEngine;
using System.Collections;

public class Death_Collider_Script : MonoBehaviour {

    private Player_Health player_health;

    void Start()
    {
        player_health = GameObject.Find("Player").GetComponent<Player_Health>();
    }

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player_health.Set_Health(0);
        }
    }
}
