﻿using UnityEngine;
using System.Collections;

public class Socks_Poison_Spread : MonoBehaviour {

    private Object_Dictionary dictionary;
    private Socks_Controller socks_controller;

	// Use this for initialization
	void Start () 
	{
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        socks_controller = GetComponentInParent<Socks_Controller>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Socks")
        {
            Socks_Controller other_socks_controller = other.gameObject.transform.GetComponent<Socks_Controller>();
            if (socks_controller.is_poisoned && socks_controller.is_alive)
            {
                if(!other_socks_controller.is_poisoned && other_socks_controller.is_alive)
				other_socks_controller.is_poisoned = true;
                other_socks_controller.socks_animation.SetBool("is_poisoned", true);
                other.gameObject.transform.GetComponentInChildren<Renderer>().material.SetColor("_Color", new Color(0.5f, 0.85f, 0.5f));
                dictionary.Get_Bubble_Popper().Pop_Poison(other.gameObject.transform);
            }
		}
	}
}

