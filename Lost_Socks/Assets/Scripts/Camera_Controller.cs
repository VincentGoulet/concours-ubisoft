﻿using UnityEngine;
using System.Collections;

public class Camera_Controller : MonoBehaviour
{
    public float altitude_min = -10.0f;
    public float altitude_max = 50.0f;
    public float distance;// = 5.0f;
    public float min_distance = 3.0f;
    public float max_distance = 7.0f;
    public float altitude_sensitivity = 6.0f;
    public float azimut_sensitivity = 4.0f;

    public float azimut = 0.0f;
    public float altitude = 0.0f;

    public Transform look_at;
    public Transform cam_transform;
    private Camera cam;
    private Object_Dictionary dictionary;
    private GameObject player;
    Vector3 dir;

    private void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        player = dictionary.Get_Player();

        cam_transform = transform;
        cam = Camera.main;
        azimut = player.transform.rotation.eulerAngles.y;

        dir = new Vector3(0, 0, -Distance());
        Quaternion rotation = Quaternion.Euler(altitude, azimut, 0);
        cam_transform.position = look_at.position + rotation * dir;
        cam_transform.LookAt(look_at.position);
    }

    public void Reload()
    {
        altitude = 0.0f;
        player = GameObject.Find("Player");
        azimut = player.transform.rotation.eulerAngles.y;
        dir = new Vector3(0, 0, -Distance());

        Quaternion rotation = Quaternion.Euler(altitude, azimut, 0);
        cam_transform.position = look_at.position + rotation * dir;
        cam_transform.LookAt(look_at.position);
    }

    private float Distance()
    {
        distance = (altitude - altitude_min) / (altitude_max - altitude_min) * (max_distance - min_distance) + min_distance;
        return distance;
    }

    bool is_focusing;

    Quaternion rotation;
    private void Update()
    {
        if (!is_focusing)
        {
            if (Input.GetAxis("Focus") != 0)
            {
                is_focusing = true;
                StartCoroutine(Focus());
            }
            else
            {
                azimut += Input.GetAxis("Camera X") * altitude_sensitivity;
                altitude += Input.GetAxis("Camera Y") * azimut_sensitivity;

                azimut = azimut % 360;
                altitude = Mathf.Clamp(altitude, altitude_min, altitude_max);
                dir = new Vector3(0, 0, -Distance());
                rotation = Quaternion.Euler(altitude, azimut, 0);
                cam_transform.position = look_at.position + rotation * dir;
                cam_transform.LookAt(look_at.position);
                Adjust();
            }
        }
    }

    RaycastHit hit;
    void Adjust()
    {
        if (Physics.Raycast(look_at.position + transform.right, - look_at.position + transform.position, out hit, Distance()))
        {
            if (hit.transform.tag == "Ground")
            {
                cam_transform.position = hit.point;
                cam_transform.LookAt(look_at.position);
            }
        }

        
        if ((transform.position.y < dictionary.Get_Water().transform.position.y + 1.0f) && (azimut >= 0f))
        {
            cam_transform.position = new Vector3(transform.position.x, dictionary.Get_Water().transform.position.y + 1.0f, transform.position.z);
            cam_transform.LookAt(look_at.position);
        }
    }

    private Vector2 Tweak_DeltaAzimut(float min_azimut, float max_azimut)
    {
        float delta = max_azimut - min_azimut;
        print(delta);
        if (delta > 180)
        {

            min_azimut += 360;
        }
        else if (delta < -180)
        {
            min_azimut -= 360;
        }

        return new Vector2(min_azimut, max_azimut);
    }

    IEnumerator Focus()
    {
        float focus_time = .5f;
        float distance_init = Distance();

        float min_azimut = azimut;
        float max_azimut = player.transform.rotation.eulerAngles.y;
        //print("before");
        //print(min_azimut);
        //print(max_azimut);
        Vector2 tweaked = Tweak_DeltaAzimut(min_azimut, max_azimut);
        //print("after");
        min_azimut = tweaked.x;
        max_azimut = tweaked.y;
        print(min_azimut);
        print(max_azimut);

        float min_altitude = altitude;
        float max_altitude = 0;

        altitude = max_altitude;
        azimut = max_azimut;
        float distance_final = Distance();
        altitude = min_altitude;
        azimut = min_azimut;
        

        Quaternion initial_rotation = transform.rotation;
        Quaternion final_rotation = Quaternion.Euler(max_altitude, max_azimut, 0f);
        Quaternion new_rotation;

        float new_distance;
        float timer = 0f;
        float t;

        while (timer < focus_time )
        {
            yield return null;
            t = (focus_time - timer) / focus_time;
            t = 1 - t * t;

            altitude = Mathf.Lerp(min_altitude, max_altitude, t);
            azimut = Mathf.Lerp(min_azimut, max_azimut, t);

            new_rotation = Quaternion.Euler(altitude, azimut, 0);
            new_distance = Mathf.Lerp(distance_init, distance_final, t);
            dir = new Vector3(0, 0, -new_distance);
            cam_transform.position = look_at.position + new_rotation * dir;
            cam_transform.LookAt(look_at.position);

            Adjust();
            timer += Time.deltaTime;
        }

        altitude = max_altitude;
        azimut = max_azimut;
        dir = new Vector3(0, 0, -Distance());
        cam_transform.position = look_at.position + final_rotation * dir;
        cam_transform.LookAt(look_at.position);
        is_focusing = false;
        
        Adjust();
        yield return null;
    }
}