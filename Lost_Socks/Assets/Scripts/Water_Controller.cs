﻿using UnityEngine;
using System.Collections;

public class Water_Controller : MonoBehaviour {
    public float tide_size = 1.0f;
    public float tide_time = 2.0f;
    private float tide_speed;
    public bool level = false;
    private bool is_adjusting = false;
    private float start_time;
    private float end_time;
    private Vector3 low;
    [HideInInspector]
    public Vector3 high;
    public bool has_remote;

    private IEnumerator tide_coroutine;
    private Object_Dictionary dictionary;
    private Geyser_Controller[] geyser_controllers;
    private Zone_System zone_system;
    private GameObject[] spawn_points;
    private SoundManager sounds;

    // Use this for initialization
    void Start () {
        spawn_points = dictionary.Get_Spawn_Points();
        zone_system = dictionary.Get_Zone_System();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        if (level)
        {
            high = transform.position;
            low = new Vector3(high.x, high.y - tide_size, high.z);
        }
        else
        {
            low = transform.position;
            high = new Vector3(low.x, low.y + tide_size, low.z);
        }

        tide_speed = (high.y - low.y) / (float)tide_time;
        tide_coroutine = Set_Level(level);
        geyser_controllers = dictionary.Get_Geyser_Controllers();
    }

    public void Toggle_level()
    {  
        if(tide_coroutine != null)
        {
            StopCoroutine(tide_coroutine);
        }      
        
        tide_coroutine = Set_Level(!level);
        StartCoroutine(tide_coroutine);
    }

    public IEnumerator Set_Level(bool new_level)
    {
        if (new_level && !level)
        {
            Trigger_Geysers();
           
            StartCoroutine(sounds.Ocean_Up(tide_speed));
            while (transform.position.y <= high.y)
            {
                transform.position = transform.position + Vector3.up * tide_speed * Time.deltaTime;                
                yield return null;
            }
            zone_system.Change_Water_Level("high");

            transform.position = high;
        }
        else if (!new_level && level)
        {
            Stop_Geysers();
            StartCoroutine(sounds.Ocean_Down(tide_speed));
            while (transform.position.y >= low.y)
            {
                transform.position = transform.position - Vector3.up * tide_speed * Time.deltaTime;
                yield return null;
            }
            zone_system.Change_Water_Level("low");
            transform.position = low;
        }

        level = new_level;
        yield return null;
    }

    void Trigger_Geysers()
    {
        for (int i = 0; i < geyser_controllers.Length; i++)
        {
            StartCoroutine(geyser_controllers[i].Squirt());
        }
    }

    void Stop_Geysers()
    {
        for (int i = 0; i < geyser_controllers.Length; i++)
        {
            if (geyser_controllers[i].geyser_is_on_volcan)
            {
                StartCoroutine(geyser_controllers[i].Squirt());
            }
            else
            {
                StartCoroutine(geyser_controllers[i].Stop());
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            //StartCoroutine(other.gameObject.GetComponent<Socks_Controller>().Drowning());
            string[] a = other.gameObject.name.Split('_');
            switch (a[1])
            {
                case "Verte":
                    spawn_points[other.gameObject.GetComponent<Socks_Controller>().Get_myTerrier()].GetComponent<Spawn_Dictionnary>().sock_counterF -= 1;
                    break;
                case "Jaune":
                    spawn_points[other.gameObject.GetComponent<Socks_Controller>().Get_myTerrier()].GetComponent<Spawn_Dictionnary>().sock_counterM -= 1;
                    break;
                case "Rouge":
                    spawn_points[other.gameObject.GetComponent<Socks_Controller>().Get_myTerrier()].GetComponent<Spawn_Dictionnary>().sock_counterH -= 1;
                    break;
                default:
                    Debug.Log("This shit doesn't work " + a[1]);
                    break;
            }
            
            Destroy(other.gameObject);
        }

        else if (other.tag == "fruits")
        {
            Destroy(other.gameObject);
        }
    }

    public void Setup(Object_Dictionary _dictionary)
    {
        dictionary = _dictionary;
    }

    /*public void Reload(int i)
    {
        if (i == 0)
        {

        }
    }*/
}
