﻿using UnityEngine;
using System.Collections;

public class Lava_Controller : MonoBehaviour {

    Object_Dictionary dictionary;

    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
    }
	
	// Update is called once per frame
	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
        {
            dictionary.Get_Player_Movement().is_lava = true;
            dictionary.Get_Player().transform.FindChild("camera_target").transform.parent = null;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name.Contains("Player"))
        {
            dictionary.Get_Player_Movement().is_lava = false;
            GameObject.Find("camera_target").transform.parent = dictionary.Get_Player().transform;
        }
    }
}
