﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class Highscore_Manager : MonoBehaviour
{
    private float fade_time;

    int[] max_socks = new int[6];
    int[] sock_scores = new int[6];
    public static float max_score = 0;

    private UI_Manager ui_manager;
    private Game_Controller game_controller;
    private Image[] images;
    private Color[] max_colors;

    int max_players = 10;
    Inventory_Manager inventory;
    Highscore[] highscores;
    int[] ranks;

    Text hey;
    Text inputname;
    Text thanks;

    Text ranks_header;
    Text users_header;
    Text scores_header;
    Image[] socks_header = new Image[6];
    //Text completion_header;

    Text ranks_text;
    Text users_text;
    Text scores_text;
    Text[] socks_text = new Text[6];
    Text completion_text;

    void Awake()
    {
        ranks_header = transform.Find("Table").Find("Ranks").GetComponent<Text>();
        users_header = transform.Find("Table").Find("Usernames").GetComponent<Text>();
        scores_header = transform.Find("Table").Find("Scores Header").GetComponent<Text>();

        int i = 0;
        foreach (Transform child in transform.FindChild("Table").FindChild("Socks"))
        {
            socks_header[i] = child.GetComponent<Image>();
            i++;
        }

        ranks_text = transform.Find("Table").Find("All Ranks").GetComponent<Text>();
        users_text = transform.Find("Table").Find("All Usernames").GetComponent<Text>();
        scores_text = transform.Find("Table").Find("All Scores").GetComponent<Text>();

        i = 0;
        foreach (Transform child in transform.Find("Table").FindChild("All Socks"))
        {
            socks_text[i] = child.GetComponent<Text>();
            i++;
        }
        completion_text = transform.Find("Table").Find("All Completion").GetComponent<Text>();

        highscores = new Highscore[max_players];
        ranks = new int[max_players];

        GameObject gc = GameObject.FindGameObjectWithTag("GameController");
        ui_manager = gc.GetComponent<UI_Manager>();
        game_controller = gc.GetComponent<Game_Controller>();

        fade_time = ui_manager.fade_time;
        images = GetComponentsInChildren<Image>();
        max_colors = new Color[images.Length];

        for (int j = 0; j < images.Length; j++)
        {
            max_colors[j] = images[j].color;
        }

        hey = transform.FindChild("Hey").GetComponent<Text>();
        inputname = transform.FindChild("Username").GetComponent<Text>();
        thanks = transform.FindChild("Thanks").GetComponent<Text>();
    }

    List<Highscore> testlist;

    // Use this for initialization
    void Start()
    {
        highscores = new Highscore[max_players];
        testlist = new List<Highscore>();
        inventory = GameObject.Find("Player").GetComponent<Inventory_Manager>();
        max_socks = inventory.Get_Max_Socks();
        sock_scores = inventory.Get_Sock_Scores();
        Set_Max_Score();

        print("about to load");
        Load_Highscores();
        //Debug.Log("Print highscore " + highscores.Length);
        foreach (Highscore h in testlist)
        {
            
            //Debug.Log("Highscore");
            /*if (h == null)
                print("null");
            else */
              //  print(h.username);
        }
    }


    void Load_Highscores()
    {
        string temp_username;
        int temp_score;
        float temp_completion;
        int[] temp_socks = new int[6];

        string key;
        //print("load");
        for (int i = 0; i < max_players; i++)
        {
            key = "Joueur" + i.ToString();
            //print(key + " " + PlayerPrefs.GetString(key + "name"));
            //Debug.Log(key);
            if (PlayerPrefs.HasKey(key + "name"))
            {
                //Debug.Log("Key is ok");
                temp_username = PlayerPrefs.GetString(key + "name");
                temp_score = PlayerPrefs.GetInt(key + "score");
                temp_completion = PlayerPrefs.GetFloat(key + "completion");
                //print(key + " " + temp_username);
                temp_socks[0] = PlayerPrefs.GetInt(key + "Green");
                temp_socks[1] = PlayerPrefs.GetInt(key + "Yellow");
                temp_socks[2] = PlayerPrefs.GetInt(key + "Red");
                temp_socks[3] = PlayerPrefs.GetInt(key + "Dark");
                temp_socks[4] = PlayerPrefs.GetInt(key + "Gold");
                temp_socks[5] = PlayerPrefs.GetInt(key + "Filthy");

                highscores[i] = new Highscore(temp_username, temp_score, temp_socks, temp_completion);
                //testlist.Add(new Highscore(temp_username, temp_score, temp_socks, temp_completion));
            }
            else
            {
                highscores[i] = new Highscore();
            }
        }
        // Debug.Log()
        //Highscore.Sort(highscores);
    }

    void Save()
    {
        string key;
        //print("save");
        for (int i = 0; i < max_players; i++)
        {
            key = "Joueur" + i.ToString();

            //Debug.Log("NAME = " + highscores[i].username);
            PlayerPrefs.SetString(key + "name", highscores[i].username);
            //print(PlayerPrefs.GetString(key));
            PlayerPrefs.SetInt(key + "score", highscores[i].score);
            PlayerPrefs.SetFloat(key + "completion", highscores[i].completion);

            PlayerPrefs.SetInt(key + "Green", highscores[i].socks[0]);
            PlayerPrefs.SetInt(key + "Yellow", highscores[i].socks[1]);
            PlayerPrefs.SetInt(key + "Red", highscores[i].socks[2]);
            PlayerPrefs.SetInt(key + "Dark", highscores[i].socks[3]);
            PlayerPrefs.SetInt(key + "Gold", highscores[i].socks[4]);
            PlayerPrefs.SetInt(key + "Filthy", highscores[i].socks[5]);

           // print(key + PlayerPrefs.HasKey(key));
        }
        //  PlayerPrefs.Save();
    }

    void Set_Max_Score()
    {
        for (int i = 0; i < 6; i++)
        {
            max_score += sock_scores[i] * max_socks[i];
        }
        print("Max_score :" + max_score);
    }

    IEnumerator Set_High_Score()
    {
        yield return StartCoroutine(Get_Name());
        Highscore new_highscore = new Highscore(new_username, inventory.Get_Player_Score(), inventory.Get_Sock_Inventory());

        Highscore[] temp_highscores = new Highscore[max_players];

        int old_i = 0;
        int new_i = 0;

        foreach (Highscore highscore in highscores)
        {
            //print(highscore);
        }


        foreach (Highscore highscore in highscores)
        {
            if (new_i < 10)
            {
                if (highscore.Get_Score() > new_highscore.Get_Score())
                {
                    temp_highscores[new_i] = highscores[old_i];
                    new_i++;
                    old_i++;
                }
                else
                {
                    if (new_i == old_i)
                    {
                        temp_highscores[new_i] = new_highscore;
                        new_i++;
                    }
                    else
                    {
                        temp_highscores[new_i] = highscores[old_i];
                        new_i++;
                        old_i++;
                    }
                }
            }
        }

     /*   print("highscore");
        for (int i = 0; i < highscores.Length; i++)
        {
            print(highscores[i].username);
        }

        print("temp_highscore");
        for (int i = 0; i < highscores.Length; i++)
        {
            print(temp_highscores[i].username);
        }*/
        highscores = temp_highscores; 
        //print("about to save");


        Save();

        users_text.text = "";
        scores_text.text = "";
        for (int i = 0; i < socks_text.Length; i++)
        {
            socks_text[i].text = "";
        }
        completion_text.text = "";

        for (int i = 0; i < max_players; i++)
        {
            if (highscores[i].score != 0)
            {
                users_text.text += highscores[i].username + "\n";
                scores_text.text += highscores[i].score + "\n";
                for (int j = 0; j < socks_text.Length; j++)
                {
                    socks_text[j].text += highscores[i].socks[j] + "\n";
                }
                completion_text.text += highscores[i].completion + " %\n";
            }
            else
            {
                users_text.text += highscores[i].username + "\n";
                scores_text.text += "---\n";
                for (int j = 0; j < socks_text.Length; j++)
                {
                    socks_text[j].text += "--\n";
                }
                completion_text.text += "--\n";
            }
        }

        //print("sort de set highscore");
    }

    string new_username = "";
    IEnumerator Get_Name()
    {
        float timer = 0f;
        while (!Input.GetKeyDown(KeyCode.Return))
        {
            yield return null;

            foreach (char c in Input.inputString)
            {
                if (c == "\b"[0])
                {
                    if (new_username.Length != 0)
                    {
                        new_username = new_username.Substring(0, new_username.Length - 1);
                    }
                }
                else
                {
                    if (c == "\n"[0] || c == "\r"[0])
                    {
                        //print("User entered his name: " + new_username);
                    }
                    else
                    {
                        if (new_username.Length < 10)
                            new_username += c;
                    }
                }
            }

            inputname.text = new_username;
            if (new_username.Length < 10)
                inputname.text += "_";

            timer += Time.unscaledDeltaTime;
        }

        //print(new_username);
    }

    class Highscore
    {
        int rank;
        public string username;
        public int score;
        public int[] socks = new int[6];
        public float completion;

        public Highscore()
        {
          //  username = "----------";
            score = 0;
            socks = new int[6] { 0, 0, 0, 0, 0, 0 };
            completion = 0;
            Set_Completion();
        }

        public Highscore(string _name, int _score, int[] _socks, float _completion = 0)
        {
          //  print("Hello i am lindsey lohan");
            username = _name;
            score = _score;
            socks = _socks;
            completion = _completion;

            if (completion == 0)
            {
                Set_Completion();
            }
        }

        void Set_Completion()
        {
            completion = Mathf.Round(score * 100 / max_score);
        }

        public int Get_Score()
        {
            return score;
        }
    }

    void OnEnable()
    {
        StartCoroutine(Execute());
    }

    IEnumerator Execute()
    {
        yield return StartCoroutine(ui_manager.Fade_In(gameObject, images, max_colors, true));
        StartCoroutine(Camera_Lerps());
        yield return StartCoroutine(Set_High_Score());
        //Save();
        // print("we wann display");

        //print(ranks_text.text);
        //print(users_text.text);
        //print(scores_text.text);

        StartCoroutine(Display_Highscores());
    }

    public Transform[] starts;
    public Transform[] ends;

    public float time_in = 1;
    public float time_lerp = 14;
    public float time_out = 1;

    IEnumerator Camera_Lerps()
    {
        int i = 0;
        while (true)
        {
            print("new view");
            StartCoroutine(ui_manager.Fade_From_Black(time_in));
            StartCoroutine(Utilities.Lerp_Camera(starts[i], ends[i], time_lerp));
            yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(time_lerp - time_out));
            yield return StartCoroutine(ui_manager.Fade_To_Black(time_out));

            i++;
            i %= starts.Length;
        }
    }

    float fade_time2 = 0.5f;
    float alpha = 91f / 255f;
    IEnumerator Display_Highscores()
    {
        StartCoroutine(Utilities.Fade(hey, fade_time2, alpha, 0));
        StartCoroutine(Utilities.Fade(inputname, fade_time2, alpha, 0));
        yield return StartCoroutine(Utilities.Fade(thanks, fade_time2, alpha, 0));

        StartCoroutine(Utilities.Fade(ranks_header, fade_time2, 0, alpha));
        StartCoroutine(Utilities.Fade(users_header, fade_time2, 0, alpha));
        StartCoroutine(Utilities.Fade(scores_header, fade_time2, 0, alpha));
        foreach (Image image in socks_header)
        {
            StartCoroutine(Utilities.Fade(image, fade_time2, 0, alpha));
        }
        StartCoroutine(Utilities.Fade(completion_text, fade_time2, 0, alpha));

        StartCoroutine(Utilities.Fade(ranks_text, fade_time2, 0, alpha));
        StartCoroutine(Utilities.Fade(users_text, fade_time2, 0, alpha));
        StartCoroutine(Utilities.Fade(scores_text, fade_time2, 0, alpha));
        foreach (Text text in socks_text)
        {
            StartCoroutine(Utilities.Fade(text, fade_time2, 0, alpha));
        }
        StartCoroutine(Utilities.Fade(completion_text, fade_time2, 0, alpha));
        //Fade thank you message and name
        // Display highscore header
        // display scores 1 by 1
        // Highlight player score (display after if not in top ten
        yield return null;
    }

    public Image[] Get_Images()
    {
        return images;
    }

    public Color[] Get_Colors()
    {
        return max_colors;
    }
}
