﻿using UnityEngine;
using System.Collections;

public class Spray_Event : MonoBehaviour {
    private Spray spray;

	// Use this for initialization
	void Start () {
        spray = transform.parent.GetComponentInParent<Spray>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
    {
        spray.Trigger_Spray(other);
    }
}
