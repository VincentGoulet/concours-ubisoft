﻿using UnityEngine;
using System.Collections;

public class Rotate_object : MonoBehaviour {

    public float rotate_speed = 75f;
    GameObject particule_unlock;
    GameObject New_item;

    void Start()
    {
        particule_unlock = (GameObject)Resources.Load("Prefabs/NewItem", typeof(GameObject));
    }

	void Update ()
    {
        transform.Rotate(Vector3.up, rotate_speed * Time.unscaledDeltaTime);
	}
}
