﻿using System.Collections;
using UnityEngine;


public class SpawnManager : MonoBehaviour
{
    public GameObject socks_ez;
    public GameObject socks_med;
    public GameObject socks_hard;
    public GameObject socks_doree;
    public GameObject socks_dark;
    public GameObject socks_filthy;
    public float spawn_time = 1f;
    public float time_2_spawn;

    public string[] sock_types;
    private GameObject[] spawn_points;
    private Object_Dictionary obj_dict;


    private float waitime = 2f;
    private Spawn_Dictionnary[] spawn_dictionaries;
    private string sockType;
    private GameObject rb_sock;
    private Player_Health playerHealth;       // Reference to the player's heatlh.
    private Zone_System zone_system;
    private GameObject water;
    
    int spawnPointIndex;
    void Awake()
    {
        obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        spawn_points = obj_dict.Get_Spawn_Points();
    }

    void Start ()
    {
        playerHealth = obj_dict.Get_Player_Health();
        zone_system = obj_dict.Get_Zone_System();
        water = obj_dict.Get_Water();
        /*spawn_dictionaries = new Spawn_Dictionnary[spawn_points.Length];
        for(int i = 0; i<spawn_points.Length; i++)
        {
            spawn_dictionaries[i] = spawn_points[i].GetComponent<Spawn_Dictionnary>(); 
        }*/
       
        StartCoroutine(SpawnCoroutine());
    }

    public void Setup(GameObject[] _spawn_points)
    {
        spawn_points = _spawn_points;
        spawn_dictionaries = new Spawn_Dictionnary[spawn_points.Length];
        for (int i = 0; i < spawn_points.Length; i++)
        {
            spawn_dictionaries[i] = spawn_points[i].GetComponent<Spawn_Dictionnary>();
        }
    }
   
    public int[] Get_Max_Socks()
    {
        int[] max_scores = new int[6] { 0, 0, 0, 1, 1, 1 };

        for (int i = 0; i < spawn_points.Length; i++)
        {
            max_scores[0] += spawn_dictionaries[i].num_facile;
            max_scores[1] += spawn_dictionaries[i].num_medium;
            max_scores[2] += spawn_dictionaries[i].num_hard;
        }

        return max_scores;
    }

    void Spawn (bool hammered= false, Transform point = null)
    {
        bool not_spawned = false;
        while (!not_spawned)
        {
            sockType = sock_types[Random.Range(0, sock_types.Length)];
            spawnPointIndex = Random.Range(0, spawn_points.Length);
            if(spawn_dictionaries[spawnPointIndex].is_busy || spawn_dictionaries[spawnPointIndex].transform.position.y < water.transform.position.y)
            {
                Spawn();
                not_spawned = true;
            }
            if (hammered)
            {
                for (int i = 0; i < spawn_points.Length; i++)
                {
                    if (spawn_points[i].transform == point)
                    {
                        spawnPointIndex = i;
                    }
                }
                
            }
            if (!(spawn_dictionaries[spawnPointIndex].sock_counterH != spawn_dictionaries[spawnPointIndex].num_hard) && !(spawn_dictionaries[spawnPointIndex].sock_counterM != spawn_dictionaries[spawnPointIndex].num_medium) && !(spawn_dictionaries[spawnPointIndex].sock_counterF != spawn_dictionaries[spawnPointIndex].num_facile))
            {
                not_spawned = true;
            }
            switch (sockType)
            {
                case "Facile":
                    // Find a random index between zero and one less than the number of spawn points.

                    if (spawn_dictionaries[spawnPointIndex].sock_counterF != spawn_dictionaries[spawnPointIndex].num_facile)
                    {
                        rb_sock = Instantiate(socks_ez, spawn_points[spawnPointIndex].transform.position, spawn_points[spawnPointIndex].transform.rotation) as GameObject;
                        rb_sock.GetComponent<Socks_Controller>().Set_myTerrier(spawnPointIndex);
                        rb_sock.transform.parent = GameObject.Find("Socks").transform;

                        not_spawned = true;
                        if (hammered)
                        {                       
                            
                            StartCoroutine(sock_translate(rb_sock));
                            
                        }
                        rb_sock.GetComponent<Socks_Controller>().Set_Present_Zone(zone_system.Get_Closer_Zone(rb_sock.transform.position));
                        spawn_dictionaries[spawnPointIndex].sock_counterF += 1;
                    }

                    break;
                case "Medium":
                    // Find a random index between zero and one less than the number of spawn points.

                    if (spawn_dictionaries[spawnPointIndex].sock_counterM != spawn_dictionaries[spawnPointIndex].num_medium)
                    {
                        rb_sock = Instantiate(socks_med, spawn_points[spawnPointIndex].transform.position, spawn_points[spawnPointIndex].transform.rotation) as GameObject;
                        rb_sock.GetComponent<Socks_Controller>().Set_myTerrier(spawnPointIndex);
                        rb_sock.transform.parent = GameObject.Find("Socks").transform;
                        not_spawned = true;
                        if (hammered)
                        {

                            StartCoroutine(sock_translate(rb_sock));


                        }
                        rb_sock.GetComponent<Socks_Controller>().Set_Present_Zone(zone_system.Get_Closer_Zone(rb_sock.transform.position));
                        spawn_dictionaries[spawnPointIndex].sock_counterM += 1;
                    }

                    break;
                case "Hard":
                    // Find a random index between zero and one less than the number of spawn points.

                    if (spawn_dictionaries[spawnPointIndex].sock_counterH != spawn_dictionaries[spawnPointIndex].num_hard)
                    {
                        rb_sock = Instantiate(socks_hard, spawn_points[spawnPointIndex].transform.position, spawn_points[spawnPointIndex].transform.rotation)as GameObject;
                        rb_sock.GetComponent<Socks_Controller>().Set_myTerrier(spawnPointIndex);
                        rb_sock.transform.parent = GameObject.Find("Socks").transform;

                        not_spawned = true;
                        if (hammered)
                        {

                            StartCoroutine(sock_translate(rb_sock));

                        }
                        rb_sock.GetComponent<Socks_Controller>().Set_Present_Zone(zone_system.Get_Closer_Zone(rb_sock.transform.position));
                        spawn_dictionaries[spawnPointIndex].sock_counterH += 1;
                    }
                    break;

            }

        }
        
        

    }
    private float GetRandomValue()
    {
        return Random.Range(-4, 4);
    }
    IEnumerator SpawnCoroutine()
    {
        // If the player has no health left...

        if (playerHealth.current_health <= 0f)
        {
            // ... exit the function.
            StopCoroutine(SpawnCoroutine());
        }
        
        yield return new WaitForSeconds(waitime);
        Spawn();
        yield return new WaitForSeconds(time_2_spawn);
        StartCoroutine(SpawnCoroutine());
        StopCoroutine(this.SpawnCoroutine());

    }
    public IEnumerator HammerSpawn(Transform position)
    {
        Spawn(true,position);
        yield return null;
    }
    IEnumerator sock_translate(GameObject sock)
    {
        yield return new WaitForSeconds(1f);
        sock.GetComponent<Rigidbody>().velocity = new Vector3(GetRandomValue(), 2, GetRandomValue());
       
        yield return null;
        
    }
    public void Load_caught_socks(int num_facile,int num_med, int num_hard)
    {
        for(int i = 0; i< spawn_points.Length; i++)
        {
            while(spawn_dictionaries[i].num_facile != spawn_dictionaries[i].sock_counterF)
            {
                spawn_dictionaries[i].sock_counterF += 1;
                num_facile -= 1;
            }
            while (spawn_dictionaries[i].num_medium != spawn_dictionaries[i].sock_counterM)
            {
                spawn_dictionaries[i].sock_counterM += 1;
                num_med -= 1;
            }
            while (spawn_dictionaries[i].num_hard != spawn_dictionaries[i].sock_counterH)
            {
                spawn_dictionaries[i].sock_counterH += 1;
                num_hard -= 1;
            }
            if(num_facile == 0 && num_med == 0 && num_hard == 0)
            {
                break;
            }
        }
        
    }
    public GameObject sock_type(string typename)
    {
        switch (typename)
        {
            case "0":
                return socks_ez;
            case "1":
                return socks_med;
            case "2":
                return socks_hard;
            case ("3"):
                return socks_doree;

            case ("4"):
                return socks_dark;

            case ("5"):
                return socks_filthy;
            case "Verte":
                return socks_ez;
            case "Jaune":
                return socks_med;
            case "Rouge":
                return socks_hard;
                
            default:
                Debug.Log("Le type donnée n'est pas dans le range de socks");
                return null; 
        }
    }
}
