﻿using UnityEngine;
using System.Collections;

public class UpAndDown : MonoBehaviour {


    private GameObject bird;

	// Use this for initialization
	void Start () {
        bird = transform.parent.transform.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        GetDistance();

    }


   public void GetDistance()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.up * -1, out hit))
        {

           Debug.Log(Vector3.Distance(transform.position, hit.transform.position) + " " + hit.transform.gameObject);
            if (Vector3.Distance(transform.position, hit.transform.position) >= 10f)
            {
                Debug.Log("test");
                bird.transform.Translate(transform.up * 5 * Time.deltaTime);
            }
        }
    }
}
