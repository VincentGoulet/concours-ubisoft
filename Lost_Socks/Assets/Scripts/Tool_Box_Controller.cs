﻿using UnityEngine;
using System.Collections;

public class Tool_Box_Controller : MonoBehaviour {

	public Transform spawn_hammer;
	public Transform spawn_remote;

	GameObject hammer;
	GameObject hammer_clone;
	GameObject remote;
	GameObject remote_clone;
    GameObject particule_unlock;
    GameObject new_item;
    Animator anim_coffre_hammer;
    Animator anim_coffre_remote;

    Transform hammer_box;
    Transform remote_box;

    public float rotate_speed = 5f;

    private int unlock_item;
    private Minimap_Controller minimap_controller;
    private SoundManager sounds;

    void Awake()
    {
        unlock_item = Animator.StringToHash("Unlock Item");
        hammer_box = transform.FindChild("Hammer_Box_");
        remote_box = transform.FindChild("Remote_Box_");
    }

    void Start()
	{
        anim_coffre_hammer = hammer_box.GetComponent<Animator>();
        anim_coffre_remote = remote_box.GetComponent<Animator>();
        hammer = (GameObject)Resources.Load("Prefabs/Marteau 1 1", typeof(GameObject));
		remote = (GameObject)Resources.Load("Prefabs/Manette", typeof(GameObject));
        particule_unlock = (GameObject)Resources.Load("Prefabs/Particles/NewItem", typeof(GameObject));
        spawn_hammer = transform.FindChild("Hammer_Box_").FindChild("Spawn_hammer");
		spawn_remote = transform.FindChild ("Remote_Box_").FindChild("Spawn_remote");
        minimap_controller = GameObject.Find("UI canvases").transform.FindChild("HUDCanvas").FindChild("Minimap").GetComponent<Minimap_Controller>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

	public void Unlock_Hammer()
	{
        StartCoroutine(Preview_Hammer());
        StartCoroutine(sounds.play_music());
    }

	public void Unlock_Remote()
    {
        StartCoroutine(Preview_Remote());
        StartCoroutine(sounds.play_music());
        /*        anim_coffre_remote.SetTrigger("Unlock Item");
                remote_clone = (GameObject)Instantiate(remote, spawn_remote.position, spawn_remote.rotation);
                new_item = (GameObject)Instantiate(particule_unlock, spawn_remote.position, Quaternion.Euler(-90, 0, 0));*/
    }

    float delta_azimut = -90f;
    float azimut_initial;
    float azimut_final;
    float azimut;
    float altitude = -45;
    float distance = 6;
    float preview_time = 4f;
    Vector3 direction;
    Quaternion rotation;
    Coroutine coroutine;
    

    
    IEnumerator Preview_Hammer()
    {
        // timescale 0
        Time.timeScale = 0;
        
        azimut_initial = hammer_box.rotation.eulerAngles.y - delta_azimut/2f + 180;
        azimut_final = azimut_initial + delta_azimut;

        // Switch camera + Animate Camera
        coroutine = StartCoroutine(Rotate_Camera(hammer_box));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.5f));
        anim_coffre_hammer.SetTrigger("Unlock Item");

        // Spawn hammer
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.7f));
        hammer_clone = (GameObject)Instantiate(hammer, spawn_hammer.position, spawn_hammer.rotation);

        // Resize hammer
        StartCoroutine(Utilities.Resize(hammer_clone.transform, 0.6f, 0.1f, 1f, true));

        // Spawn particles
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.3f));
        new_item = (GameObject)Instantiate(particule_unlock, spawn_hammer.position + 0.2f * Vector3.up, Quaternion.Euler(-90, 0, 0));

        // restore timescale
        yield return coroutine;
        Time.timeScale = 1;

        // Alert hammer map
        minimap_controller.Hammer_Location();

    }

    IEnumerator Preview_Remote()
    {
        // timescale 0
        Time.timeScale = 0;

        azimut_initial = remote_box.rotation.eulerAngles.y - delta_azimut / 2f + 180;
        azimut_final = azimut_initial + delta_azimut;

        // Switch camera + Animate Camera
        coroutine = StartCoroutine(Rotate_Camera(remote_box));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.5f));
        anim_coffre_remote.SetTrigger("Unlock Item");

        // Spawn remote
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.7f));
        remote_clone = (GameObject)Instantiate(remote, spawn_remote.position, spawn_remote.rotation);

        // Resize remote
        StartCoroutine(Utilities.Resize(remote_clone.transform, 0.6f, 0.1f, 1f, true));

        // Spawn particles
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(.3f));
        new_item = (GameObject)Instantiate(particule_unlock, spawn_remote.position + 0.2f * Vector3.up, Quaternion.Euler(-90, 0, 0));

        // restore timescale
        yield return coroutine;
        Time.timeScale = 1;

        // Alert remote map
        minimap_controller.Remote_Location();
    }

    IEnumerator Rotate_Camera(Transform box)
    {
        Set_Azimut(box, azimut_initial);
        float timer = 0;

        while (timer < preview_time)
        {
            yield return null;
            Set_Azimut(box, Mathf.Lerp(azimut_initial, azimut_final, timer / preview_time));
            timer += Time.unscaledDeltaTime;
        }

        yield return null;
        Set_Azimut(box, azimut_final);
    }

    void Set_Azimut(Transform box, float a)
    {
        azimut = a;
        direction = new Vector3(0, 0, distance);

        rotation = Quaternion.Euler(altitude, azimut, 0);
        Camera.main.transform.position = box.position + rotation * direction;
        Camera.main.transform.LookAt(box.position);
    }
}
