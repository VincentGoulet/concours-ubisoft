﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game_Controller : MonoBehaviour {
    public bool skip_intro = false;
    public bool has_fruit;
    public int max_fruit = 5;
    public float spawn_probability = 0.1f;
    public float spawn_delay = 2.0f;
    [HideInInspector]
    public Object_Dictionary dictionary;
    [HideInInspector]
    public UI_Manager ui_manager;
    [HideInInspector]
    public GameObject player;
    private Player_Movement player_movement;

    private int n_available_actions = 3;

    private string[] x_action_types;
    private int[] x_action_object_index;
    private Vector3[] x_action_positions;
    private int x_active_action = -1;

    private string[] b_action_types;
    private int[] b_action_object_index;
    private Vector3[] b_action_positions;
    private int b_active_action = -1;

    private SoundManager sounds;

    public string state = "game";
    public static Game_Controller instance = null;

    //Machine varibles for animation
    private Animator anim_player;
    private Animator anim_fruit;
    private Fruit_Controller fruit;          
    private DoneHashIDs hash;
    public bool do_anim_pick = false;

    [HideInInspector]
    public bool is_end = false;

    // Use this for initialization
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
        
        dictionary = GetComponent<Object_Dictionary>();
        ui_manager = GetComponent<UI_Manager>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        vortex_camera0 = GameObject.Find("Vortex0").transform;
        vortex_camera1 = GameObject.Find("Vortex1").transform;

        ps_lava = GameObject.Find("Volcano Smoke").GetComponent<ParticleSystem>();
        ps_vortex = ((GameObject)Resources.Load("Prefabs/Particles/Vortex Smoke", typeof(GameObject)));
        particles_transition = ((GameObject)Resources.Load("Prefabs/Particles/Transition", typeof(GameObject)));
        vortex_geometry = ((GameObject)Resources.Load("Prefabs/Vortex Geometry", typeof(GameObject)));
    }

    // Use this for initialization
    void Start()
    {
        player = dictionary.Get_Player();
        player_movement = dictionary.Get_Player_Movement();

        hash = GetComponent<DoneHashIDs>();
        anim_player = player.GetComponent<Animator>();  

        b_action_types = new string[n_available_actions];
        b_action_object_index = new int[n_available_actions];
        b_action_positions = new Vector3[n_available_actions];

        x_action_types = new string[n_available_actions];
        x_action_object_index = new int[n_available_actions];
        x_action_positions = new Vector3[n_available_actions];

        for (int i = 0; i < n_available_actions; i++)
        {
            b_action_types[i] = "";
            x_action_types[i] = "";

            b_action_object_index[i] = -1;
            x_action_object_index[i] = -1;
        }

        if (!skip_intro)
        {
            StartCoroutine(Start_Animation());
        }
        Time.timeScale = 1;
    }

    public bool Has_Fruit()
    {
        return has_fruit;
    }

    bool debug;
    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetButtonDown("Backspace"))
        {
            print("back");
        }*/

        if (!debug)
        {
            if (Time.timeSinceLevelLoad > 1.5f)
            {
                //StartCoroutine(Vortex());
                debug = true;
                //is_end = true;
            }
        }

        if (Input.GetButtonDown("Start"))
        {
            if (ui_manager.Get_State() != "game")
            {
                if (!ui_manager.Get_Validating())
                {
                    ui_manager.Toggle("game");
                }
            }
            else
            {
                ui_manager.Toggle("menu");
            }
        }

        else if (state == "game")
        {
            //if (dictionary.Get_Player_Movement().is_take)
            //{
            //int best_candidate = -1;
            if (player_movement.fruit_index == -1)
            {
                if (b_active_action != -1)
                {
                    for (int i = 0; i < n_available_actions; i++)
                    {
                        if ((b_action_types[i] != "") && (i != b_active_action))
                        {
                            float distance_player_to_this_action = Vector3.Distance(player.transform.position, b_action_positions[i]);
                            float distance_player_to_active_action = Vector3.Distance(player.transform.position, b_action_positions[i]);
                            float buffer = 0.2f;

                            if (distance_player_to_this_action < distance_player_to_active_action - buffer)
                            {
                                b_active_action = i;
                                ui_manager.Display_Action(b_action_types[b_active_action]);
                            }
                        }
                    }

                    if (Input.GetButtonDown("Interact"))
                    {
                        B_Activate();
                    }
                }

                if (x_active_action != -1)
                {
                    if (Input.GetButtonDown("Whip"))
                    {
                        B_Activate();
                    }
                }
            }
            else
            {
                if (!do_anim_pick)
                {

                    if (Input.GetButtonDown("Interact"))
                    {
                        //deposer fruit
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).Set_is_Take(false);
                        anim_player.SetBool(hash.holdBool, false);
                        StartCoroutine(dictionary.Get_Fruit_Controller(player_movement.fruit_index).Take_Fruit(dictionary.Get_Fruit_Controller(player_movement.fruit_index).transform));
                        dictionary.Get_Player_Movement().fruit_index = -1;
                    }
                    if (Input.GetButtonDown("Hammer"))
                    {
                        //Lancer fruit
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).tag = "Fruits";
                        has_fruit = false;
                        StartCoroutine(dictionary.Get_UI_Manager().Toggle_Controls("Default"));
                        anim_player.SetBool(hash.throwBool, true);
                        StartCoroutine(dictionary.Get_Player_Movement().Disable_anim_throw());
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).transform.parent = null;
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).GetComponent<Rigidbody>().isKinematic = false;
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).Set_is_Take(false);
                        dictionary.Get_Fruit_Controller(player_movement.fruit_index).GetComponent<Rigidbody>().velocity = player.transform.rotation * new Vector3 (0, 7, 7);
                    }
                    if (Input.GetButtonDown("Remote"))
                    {
                        //Manger fruit
                        StartCoroutine(dictionary.Get_UI_Manager().Toggle_Controls("Default"));
                        anim_player.SetBool(hash.eatBool, true);
                        sounds.play_eating();
                        StartCoroutine(dictionary.Get_Player_Movement().Disable_anim_eating());
                    }
                }
            }
        }
    }

    public void Set_Available_Interaction(Collider collider)
    {
        if (player_movement.fruit_index == -1)
        {
            int b_action_index = -1;
            int x_action_index = -1;

            for (int i = 0; i < n_available_actions; i++)
            {
                if (b_action_types[i] == "")
                {
                    b_action_index = i;
                    break;
                }

                if (x_action_types[i] == "")
                {
                    x_action_index = i;
                    break;
                }
            }


            if (b_action_index >= 0)
            {
                switch (collider.tag)
                {
                    case ("Washer"):
                        b_action_types[b_action_index] = "Washer";
                        b_action_object_index[b_action_index] = collider.gameObject.GetComponent<Washer_Controller>().Get_Index();
                        b_action_positions[b_action_index] = collider.transform.position;
                        b_active_action = b_action_index;
                        ui_manager.Display_Action(b_action_types[b_active_action]);
                        break;

                    case ("Fruits"):
                        b_action_types[b_action_index] = "Pick";
                        b_action_object_index[b_action_index] = collider.GetComponent<Fruit_Controller>().Get_Index();
                        b_action_positions[b_action_index] = collider.transform.position;
                        b_active_action = b_action_index;
                        ui_manager.Display_Action(b_action_types[b_active_action]);
                        break;

                    default:
                        break;
                }
            }
        }
    }
    
    public void Set_Unavailable_Interaction(Collider collider)
    {
        int object_index = -1;
        int action_index = -1;

        switch (collider.tag)
        {
            case ("Washer"):
                object_index = collider.gameObject.GetComponent<Washer_Controller>().Get_Index();
                for (int i = 0; i < n_available_actions; i++)
                {
                    if ((b_action_types[i] == "Washer") && (b_action_object_index[i] == object_index))
                    {
                        action_index = i;
                    }
                }

                ui_manager.Display_Action(b_action_types[action_index], false);
                b_action_types[action_index] = "";

                b_active_action = -1;
                for (int i = 0; i < n_available_actions; i++)
                {
                    if (b_action_types[i] != "")
                    {
                        b_active_action = i;
                        ui_manager.Display_Action(b_action_types[i], true);
                    }
                }
                break;

            case ("Fruits"):
                object_index = collider.GetComponent<Fruit_Controller>().Get_Index();
                
                for (int i = 0; i < n_available_actions; i++)
                {
                    if ((b_action_types[i] == "Pick") && (b_action_object_index[i] == object_index))
                    {
                        action_index = i;
                    }
                }

                if (action_index != -1)
                {
                    b_action_types[action_index] = "";
                    ui_manager.Display_Action("Pick", false);

                    b_active_action = -1;
                    for (int i = 0; i < n_available_actions; i++)
                    {
                        if (b_action_types[i] != "")
                        {
                            b_active_action = i;
                            ui_manager.Display_Action(b_action_types[i], true);
                        }
                    }
                }
                break;

            default:
                break;
        }

        for (int i = 0; i < n_available_actions; i++)
        {
            if ((b_action_types[i] == collider.tag) && (b_action_object_index[i] == object_index))
            {
                action_index = i;
                break;
            }

            if ((x_action_types[i] == collider.tag) && (x_action_object_index[i] == object_index))
            {
                action_index = i;
                break;
            }
        }

        if (action_index == -1)
        {
            //throw new System.Exception("No action index could be assigned while removing available action");
        }
    }

    public void Set_All_Unavailable()
    {
        if (b_active_action != -1)
        {
            ui_manager.Display_Action(b_action_types[b_active_action], false);
        }
        b_action_object_index = new int[3] { -1, -1, -1 };
        b_action_types = new string[3] { "", "", "" };
        b_active_action = -1;
    }

    private void B_Activate()
    {
        //Vector3 position;

        switch (b_action_types[b_active_action])
        {
            case ("Washer"):
                
                if (dictionary.Get_Player_Movement().fruit_index == -1)
                {
                    anim_player.SetBool(hash.machineBool, true);
                    StartCoroutine(StopAnimMachine());
                    bool level = dictionary.Get_Washer_Controller(b_action_object_index[b_active_action]).Get_Level();
                    StartCoroutine(dictionary.Get_Water_Controller().Set_Level(level));
                    sounds.activate_washer();
                }
                break;

           case ("Pick"):
                //Animation prendre fruit
                if (dictionary.Get_Fruit_Controller(b_action_object_index[b_active_action]).is_grounded == true)
                {
                    dictionary.Get_Player_Movement().fruit_index = b_action_object_index[b_active_action];
                    dictionary.Get_Fruit_Controller(b_action_object_index[b_active_action]).Set_is_Take(true);
                    StartCoroutine(dictionary.Get_Fruit_Controller(b_action_object_index[b_active_action]).Take_Fruit(dictionary.Get_Fruit_Controller(b_action_object_index[b_active_action]).transform));
                    anim_player.SetBool(hash.pickBool, true);
                    StartCoroutine(player_movement.Taking());
                    do_anim_pick = true;
                    //StartCoroutine(enable_anim_player_hold());
                    StartCoroutine(Disable_anim_player_pick());
                    Set_All_Unavailable();                    
                }
                break;

            default:
                throw new System.Exception("The tag of this collider has no associated interaction.");
        }
    }

    public void Set_State(string _state)
    {
        state = _state;
    }

    public void Reload()
    {
    }

    public void Exit()
    {
        //Screen.fullScreen = !Screen.fullScreen;
        Application.Quit();
    }

    IEnumerator StopAnimMachine()
    {
        yield return new WaitForSeconds(0.5f);
        anim_player.SetBool(hash.machineBool, false);
    }
    /*IEnumerator enable_anim_player_hold()
    {
        yield return new WaitForSeconds(0.5f);
        anim_player.SetBool(hash.holdBool, true);
    }*/

    IEnumerator Disable_anim_player_pick()
    {
        //yield return new WaitForSeconds(0.5f);
        anim_player.SetBool(hash.holdBool, true);
        yield return new WaitForSeconds(1.0f);
        anim_player.SetBool(hash.pickBool, false);
        do_anim_pick = false;
    }

    public bool Is_B_Available()
    {
        return b_active_action != -1;
    }

    public string Get_B_Action()
    {
        return b_action_types[b_active_action];
    }

    ParticleSystem ps_lava;
    GameObject ps_vortex;
    GameObject particles_transition;
    GameObject vortex_geometry;
    Transform vortex_camera0;
    Transform vortex_camera1;
    float camera_time = 8f;
    float before_vortex = 2.5f;
    float during_vortex = 2f;
    float between_smokes = 1f;
    Material vortex_material;
    Material plane_material;
    Coroutine camera_coroutine;

    public IEnumerator Vortex()
    {
        yield return new WaitForSeconds(1f);

        Time.timeScale = 0;
        ps_lava.Stop();
        
        camera_coroutine = StartCoroutine(Utilities.Lerp_Camera(vortex_camera0, vortex_camera1, camera_time));

        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(before_vortex));
        StartCoroutine(Transition_Vortex());

        yield return camera_coroutine;
        Time.timeScale = 1;
    }

    IEnumerator Transition_Vortex()
    {
        ps_lava.Stop();
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(0.5f));
        Instantiate(particles_transition);
        sounds.play_explosion();
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(0.5f));
        Destroy(ps_lava.gameObject);
        Instantiate(ps_vortex);
        Instantiate(vortex_geometry);
        yield return new WaitForSeconds(0.5f);
        sounds.play_vortex();
        

        
    }

    public void End_Signal()
    {
        if (is_end)
        {
            // Declencher cinematique de fin
            StartCoroutine(dictionary.Get_UI_Manager().End());
        }
    }


    Coroutine cam_coroutine;
    public Text intro_text;
    public Image logo;
    public Transform[] starts = new Transform[3];
    public Transform[] ends = new Transform[3];

    float in0 = 1f;///4f;
    float bet0 = 2f;///4f;
    float out0 = 1f;///4f;

    float read0 = 5f;
    float read1 = 6f;
    float read2 = 5f;

    float in3 = 2f;///4f;
    float bet3 = 3f;///4f;
    float out3 = 2f;///4f;

    float logo_fade = 1f;

    public IEnumerator Start_Animation()
    {
        dictionary.Get_UI_Manager().ui_bubbles.SetActive(false);

        Time.timeScale = 0;
        StartCoroutine(ui_manager.Fade_To_Black(0f));
        //print("faded to black");
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(1.0f));

        cam_coroutine = StartCoroutine(Utilities.Lerp_Camera(starts[0], ends[0], in0 + bet0 + out0));
        yield return StartCoroutine(ui_manager.Fade_From_Black(in0));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(bet0));
        yield return StartCoroutine(ui_manager.Fade_To_Black(out0));

        intro_text.text = text1;
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, 0, .75f, true));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(read0));
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, .75f, 0, true));

        float field = Camera.main.fieldOfView;
        Camera.main.fieldOfView = 90;
        float far = Camera.main.farClipPlane;
        Camera.main.farClipPlane = 2000;
        cam_coroutine = StartCoroutine(Utilities.Lerp_Camera(starts[1], ends[1], in0 + bet0 + out0));
        yield return StartCoroutine(ui_manager.Fade_From_Black(in0));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(bet0));
        yield return StartCoroutine(ui_manager.Fade_To_Black(out0));
        Camera.main.farClipPlane = far;
        Camera.main.fieldOfView = field;

        intro_text.text = text2;
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, 0, .75f, true));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(read1));
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, .75f, 0, true));

        field = Camera.main.fieldOfView;
        far = Camera.main.farClipPlane;
        Camera.main.farClipPlane = 1000;
        cam_coroutine = StartCoroutine(Utilities.Lerp_Camera(starts[2], ends[2], in0 + bet0 + out0));
        yield return StartCoroutine(ui_manager.Fade_From_Black(in0));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(bet0));
        yield return StartCoroutine(ui_manager.Fade_To_Black(out0));
        Camera.main.farClipPlane = far;

        intro_text.text = text3;
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, 0, .75f, true));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(read2));
        yield return StartCoroutine(Utilities.Fade(intro_text, in0, .75f, 0, true));

        field = Camera.main.fieldOfView;
        far = Camera.main.farClipPlane;
        cam_coroutine = StartCoroutine(Utilities.Lerp_Camera(starts[3], ends[3], in3 + bet3 + out3));
        cam_coroutine = StartCoroutine(ui_manager.Fade_From_Black(in3));
        yield return StartCoroutine(Utilities.Fade(logo, in3, 0, 1));
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(bet3));
        yield return StartCoroutine(ui_manager.Fade_To_Black(out3));
        StartCoroutine(Utilities.Fade(logo, 0, 1, 0));

        print("time scale is back");
        Time.timeScale = 1;
        //player_movement = GameObject.Find("Player").GetComponent<Player_Movement>();
        //player_movement.is_cant_move = true;
        print("about to fade back");
        yield return StartCoroutine(ui_manager.Fade_From_Black(1f));
        Time.timeScale = 1;
        //player_movement.is_cant_move = false;
        print("leaving coroutine");

        Camera.main.GetComponent<Camera_Controller>().Reload();
        dictionary.Get_UI_Manager().ui_bubbles.SetActive(true);
        print(Time.timeScale);
    }

    string text1 = "Mom was really upset today.\n\nI think it's because the socks always disappear in the laundry.";
    string text2 = "I know because she told me that sometimes things go away and nothing can stop it.\n\nI just wish it didn't make her cry like this.";
    string text3 = "But I know where all the socks go. Maybe if I bring all of them back, she'll be smiling again !\n\nI must get all of them back !";
}
