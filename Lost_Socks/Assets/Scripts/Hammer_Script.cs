﻿using UnityEngine;
using System.Collections;

public class Hammer_Script : MonoBehaviour 
{
	bool is_take; 

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!is_take)
		{
			Debug.Log("enter");
			GetComponent<Rigidbody> ().angularVelocity = Vector3.up * 5;
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Ground")
		{
			GetComponent<Rigidbody> ().isKinematic = true;
		}
	}
}
