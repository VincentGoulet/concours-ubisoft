﻿using UnityEngine;
using System.Collections;

public class Spray : MonoBehaviour {
    public Transform spray_transform;
    private Collider spray_collider;
    public Transform spray_particles;
    public float cool_down = 1.0f;
    public float spray_time = 0.2f;
    public float hold_time = 0.5f;

    private float scale_factor = 3.0f;
    private bool is_spraying;
    private bool is_pressed;

    private Vector3 min_scale;
    private Vector3 max_scale;
    private Vector3 transform_localPosition;
    private Quaternion transform_localRotation;

    //Audio variables
    private SoundManager sounds;
    private Vector3 particles_offset = new Vector3(0.0f, 0.3f, .9f);

    private Object_Dictionary dictionary;
    private Player_Movement player_movement;
    private GameObject player;
    private Fruit_Controller fruit_controller;

    // Use this for initialization
    void Start () {
        GameObject gc = GameObject.Find("Game Controller");
        dictionary = gc.GetComponent<Object_Dictionary>();
        player_movement = dictionary.Get_Player_Movement();
        player = dictionary.Get_Player();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
       
        spray_collider = spray_transform.GetComponentInChildren<Collider>();
        spray_collider.enabled = false;
        
        spray_transform.parent = null;
        max_scale = spray_transform.localScale;
        min_scale = max_scale / scale_factor;
        spray_transform.parent = transform;

        transform_localPosition = spray_transform.localPosition;
        transform_localRotation = spray_transform.localRotation;
    }

    // Update is called once per frame
    void Update () {

        if (is_pressed)
        {
            if (0 == Input.GetAxis("Spray"))
            {
                is_pressed = false;
            }
        }
        else if (!is_spraying && player_movement.fruit_index == -1)
        {
            if (0 != Input.GetAxis("Spray"))
            {
                //print("in get axis spray");
                StartCoroutine(Start_Spray());
            }
        }
	}

    IEnumerator Start_Spray()
    {
        player_movement.anim_player.SetBool(player_movement.hash.sprayBool, true);


        Vector3 velocity = player_movement.Get_Velocity();
        velocity = new Vector3(velocity.x, 0.0f, velocity.z);
        is_pressed = true;
        is_spraying = true;

        yield return new WaitForSeconds(0.2f);
        player_movement.anim_player.SetBool(player_movement.hash.sprayBool, false);

        spray_collider.enabled = true;
        Transform spray_clone = (Transform)Instantiate(spray_particles, spray_transform.position - 1.0f * Vector3.up, spray_transform.rotation * Quaternion.Euler(20.0f, 0.0f, 0.0f));
        spray_clone.parent = transform;
        spray_clone.localPosition += particles_offset;
        spray_clone.parent = null;
        spray_transform.parent = null;


        float end_time = Time.time + spray_time;
        Vector3 new_scale;
        float t;
        sounds.play_spray();
        while (Time.time < end_time + hold_time)
        {
            spray_transform.localScale = max_scale;
            if (Time.time < end_time)
            {
                t = 1 - (end_time - Time.time) / spray_time;
                new_scale = Vector3.Lerp(min_scale, max_scale, t);
                spray_transform.localScale = new_scale;
            }
            
            spray_transform.position = spray_transform.position + velocity/2.0f * Time.deltaTime;
            spray_clone.position = spray_clone.position + velocity/2.0f * Time.deltaTime;
            yield return null;
        }

        spray_collider.enabled = false;
        spray_transform.parent = transform;
        spray_transform.localPosition = transform_localPosition;
        spray_transform.localRotation = transform_localRotation;

        is_spraying = false;

        end_time = Time.time + 0.5f;
        while (Time.time < end_time)
        {
            spray_clone.position = spray_clone.position + velocity / 2.0f * Time.deltaTime;
            yield return null;
        }
        Destroy(spray_clone.gameObject);
    }

    public void Trigger_Spray(Collider other)
    {
    }
}
