﻿using UnityEngine;
using System.Collections;

public class Inventory_Manager : MonoBehaviour {
    private bool[] has_object;

    public int nombre_object = 2;
    public int types_of_socks = 6;

    public int[] washer_numbers = new int[] { 100, 500, 2000, 3000 };
    int hammer_number = 1500;
    int remote_number = 5000;

    int hammer_score = 2500;
    int remote_score = 5000;
    int[] sock_scores = new int[6] { 100, 200, 500, 10000, 20000, 99999 };

    private int[] sock_type_number;
    private bool[] are_unlocked;
    private bool is_hammer_unlock = false;
    private bool is_remote_unlock = false;
	public int score = 0;

    Object_Dictionary obj_dict;
    Player_Movement player_script;
	Tool_Box_Controller tool_box_script;
    Text_Popper text_popper;
    int[] washer_unlock;
    bool[] is_washer_unlocked = new bool[4];
    bool remote_unlocked;
    bool hammer_unlocked;

    private GameObject tool_box_hammer;
    private GameObject tool_box_remote;
    Checkpoint_Manager save_manager;
    
    // Use this for initialization
    void Awake()
    {
        tool_box_script = GameObject.Find("Tool_Box_Controller").GetComponent<Tool_Box_Controller>();
        obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        save_manager = GameObject.Find("Game Controller").GetComponent<Checkpoint_Manager>();
    }

    void Start ()
    {
        text_popper = obj_dict.Get_UI_Manager().Get_Text_Popper();
        washer_unlock = text_popper.washer_numbers;
        player_script = obj_dict.Get_Player_Movement();
        has_object = new bool[nombre_object];
        for(int i = 0; i< has_object.Length; i++)
        {
            if(i == 0)
            {
                has_object[i] = player_script.Has_Hammer();
            }
            if(i == 1)
            {
                has_object[i] = player_script.Has_Remote();
            }
        }
        //Debug.Log(types_of_socks);
        sock_type_number = new int[types_of_socks];
        are_unlocked = new bool[types_of_socks];	
	}

    public void Set_Hammy(bool _has_hammy)
    {
        has_object[0] = _has_hammy;
        
    }
    public void Set_Remmy(bool _has_remmy)
    {
        has_object[1] = _has_remmy;

    }
    public void Add_Sock(string type)
    {
		score += Get_Score(type);
        Pop_Score(Get_Score(type));
        
        if (score >= hammer_number)
        {
            if (!hammer_unlocked)
            {
                tool_box_script.Unlock_Hammer();
                hammer_unlocked = true;
                //save_manager.Set_Checkpoint(2);
            }
        }
        if (score >= remote_number)
        {
            if (!remote_unlocked)
            {
                tool_box_script.Unlock_Remote();
                remote_unlocked = true;
                //save_manager.Set_Checkpoint(4);
            }
        }

        for (int i = 0; i < washer_unlock.Length; i++)
        {
            if (score >= washer_unlock[i])
            {
                if (!is_washer_unlocked[i])
                {
                    obj_dict.Get_Washer_Controller(i).tag = "Washer";
                    is_washer_unlocked[i] = true;
                }
            }
        }
        if (sock_type_number[Utilities.Type2Int(type)] == 0)
        {
            obj_dict.Get_UI_Manager().Unlock_Sock(type);
        }
        sock_type_number[Utilities.Type2Int(type)] += 1;
        obj_dict.Get_UI_Manager().Update_Socks(score);
        obj_dict.Get_UI_Manager().Get_Text_Popper().Update_Numbers(score);
        obj_dict.Get_UI_Manager().Get_Inventory_Script().Increment_Score(type);

        if (type == "Filthy")
        {
            StartCoroutine(obj_dict.Get_Game_Controller().Vortex());
            obj_dict.Get_Game_Controller().is_end = true;
        }
    }

    private void Pop_Score(int score)
    {
        StartCoroutine(obj_dict.Get_UI_Manager().Pop_Text(score.ToString()));
    }

    public int[] Get_Inventaire()
    {
        return sock_type_number;
    }

    public int Get_Score(string type, int index = -1)
    {
        if(index == -1)
        {
            if (type == "Hammer")
            {
                return hammer_score;
            }
            else if (type == "Remote")
            {
                return remote_score;
            }
            else
            {
                return sock_scores[Utilities.Type2Int(type)];
            }
        }
        else
            return sock_scores[index];
    }

    public int[] Get_Sock_Scores()
    {
        return sock_scores;
    }

    public int[] Get_Max_Socks()
    {
        return obj_dict.Get_Spawn_Manager().Get_Max_Socks();
    }

    public int Get_Player_Score()
    {
        return score;
    }

    public int[] Get_Sock_Inventory()
    {
        return sock_type_number;
    }
}
