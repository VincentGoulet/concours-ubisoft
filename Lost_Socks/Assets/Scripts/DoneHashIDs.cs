﻿using UnityEngine;
using System.Collections;

public class DoneHashIDs : MonoBehaviour
{
	// Here we store the hash tags for various strings used in our animators.
	public int dyingState;
	public int locomotionState;
    public int jumpingState;
	public int sprayingState;
    public int jumpBool;
	public int deadTrigger;
	public int speedFloat;
	public int sprayBool;

    public int machineState;
    public int machineBool;
    public int holdingState;
    public int holdBool;
    public int pickBool;
    public int throwingState;
    public int throwBool;
    public int smashingState;
    public int smashBool;
    public int eatingState;
    public int eatBool;
    public int fallBool;
    public int groundBool;
    public int remoteBool;
    public int releveBool;
    public int releveState;
    public int endTrigger;


    void Awake()
	{
		dyingState = Animator.StringToHash("Base Layer.Mort");
		locomotionState = Animator.StringToHash("Base Layer.Locomotion");
        jumpingState = Animator.StringToHash("Jumping.Saut");
        jumpBool = Animator.StringToHash("Jump");
        sprayingState = Animator.StringToHash("Spraying.Spray");
		deadTrigger = Animator.StringToHash("Dead");
		speedFloat = Animator.StringToHash("Speed");
		sprayBool = Animator.StringToHash("Spray");

        machineState = Animator.StringToHash("Machine.Machine");
        machineBool = Animator.StringToHash("Machine");
        holdingState = Animator.StringToHash("Holding Fruit.Hold");
        holdBool = Animator.StringToHash("Hold");
        pickBool = Animator.StringToHash("Pick");
        throwingState = Animator.StringToHash("Throwing.Throw");
        throwBool = Animator.StringToHash("Throw");
        smashingState = Animator.StringToHash("Smashing.Smash");
        smashBool = Animator.StringToHash("Smash");
        eatingState = Animator.StringToHash("Eating.Manger fruit");
        eatBool = Animator.StringToHash("Eat");
        fallBool = Animator.StringToHash("Fall");
        groundBool = Animator.StringToHash("Ground");
        remoteBool = Animator.StringToHash("Remote");
        releveState = Animator.StringToHash("Releve.releve");
        releveBool = Animator.StringToHash("Releve");
        endTrigger = Animator.StringToHash("End");
    }
}