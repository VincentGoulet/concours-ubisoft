﻿using UnityEngine;
using System.Collections;

public class ClimbController : MonoBehaviour
{
    public bool is_up;
    public Transform up_transform;
    public Transform down_transform;

    private int index;
	
    public void Setup(int _index)
    {
        index = _index;
    }

    public int Get_Index()
    {
        return index;
    }

    public Vector3 Get_Down_Position()
    {
        return down_transform.position;
    }

    public Vector3 Get_Up_Position()
    {
        return up_transform.position;
        
    }
}
