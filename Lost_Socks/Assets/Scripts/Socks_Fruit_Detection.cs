﻿using UnityEngine;
using System.Collections;

public class Socks_Fruit_Detection : MonoBehaviour
{
    private Socks_Controller socks_controller;
    public Transform sock_transform;

    // Use this for initialization
    void Start()
    {
        socks_controller = GetComponentInParent<Socks_Controller>();
        sock_transform = transform.parent.transform;
    }

    void OnTriggerEnter(Collider other)
    {
        GameObject object_detected = other.gameObject;
        switch (object_detected.tag)
        {

            case "Fruits":
                if (socks_controller.state == Socks_Controller.State.is_walking || socks_controller.state == Socks_Controller.State.is_idle)
                {
                    if (!socks_controller.transport_fruit)
                    {
                        if (object_detected.GetComponent<Fruit_Controller>().is_take == false)
                            socks_controller.Set_Fruit_Target(object_detected.GetComponent<Transform>());
                    }
                }
                break;
                /*
            case "Socks":
                if (socks_controller.state == Socks_Controller.State.is_fleeing)
                {
                    object_detected.GetComponent<Socks_Controller>().Set_Fleeing_Destination();
                }
                break;
                /*case "Terrier":
                    if(socks_controller.transport_fruit)
                    {
                        object_detected.GetComponent<Spawn_Dictionnary>().Enter_Drop_Zone(sock_transform);
                    }
                    break;
                 */
        }
    }

    /*void OnTriggerExit (Collider other)
    {
        if(other.gameObject.tag == "Terrier")
        {
            other.gameObject.GetComponent<Spawn_Dictionnary>().Exit_Drop_Zone();
        }
    }*/

    // Update is called once per frame
    void Update()
    {

    }
}
