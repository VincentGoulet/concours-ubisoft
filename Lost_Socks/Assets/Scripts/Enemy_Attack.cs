﻿using UnityEngine;
using System.Collections;

public class Enemy_Attack : MonoBehaviour {
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 1;


    Animator anim;
    GameObject player;
    Player_Health playerHealth;
    Object_Dictionary obj_dict;

    //EnemyHealth enemyHealth;
    bool playerInRange;
    float timer;


    void Start()
    {
        obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        player = obj_dict.Get_Player();
        playerHealth = player.GetComponent<Player_Health>();
        //enemyHealth = GetComponent<EnemyHealth>();
        anim = player.GetComponent<Animator>();
        if (name.Contains("Lava")) is_lava = true;        
    }
    

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = true;
            Contact_water();
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!obj_dict.Get_Game_Controller().is_end)
            {
                if (is_lava)
                {
                    playerHealth.Set_Health(0);
                }
                else
                {
                    timer += Time.deltaTime;
                    if (timer >= timeBetweenAttacks)
                    {
                        Attack();
                    }
                }
            }
           
        }
    }

    bool is_lava;    

    void Contact_water()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange || player.transform.position.y < gameObject.transform.position.y && timer >= timeBetweenAttacks)
        {
            if (!is_lava)
            {
                Attack();
            }          
        }

        if (playerHealth.current_health <= 0)
        {
            //anim.SetTrigger("Dead");
        }

        if (player.transform.position.y - gameObject.transform.position.y < -1)
        {
            playerHealth.Set_Health(0);
        }
    }


    void Attack()
    {
        timer = 0f;

        if (playerHealth.current_health > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}




