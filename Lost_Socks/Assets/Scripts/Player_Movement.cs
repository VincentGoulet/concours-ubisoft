using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour
{
    public int start = 0;
    [HideInInspector]
    public bool is_taking;

    public float turnSmoothing = 15f;   // A smoothing value for turning the player.
    public float speedDampTime = 0.1f;  // The damping for the speed parameter
    public float max_speed;
    [HideInInspector]
    public float speed;
    public float jump_power = 1.0f;
    public bool is_jump_down = false;
    private float h;
    private float v;
    private float speed_threshold = 0.1f;

    // Bool states
    private bool is_moving = false;
    private bool is_grounded = false;

    public Transform present_zone;

    public Rigidbody this_rigidbody;
    [HideInInspector]
    public Animator anim_player;              // Reference to the animator component.
    [HideInInspector]
    public DoneHashIDs hash;           // Reference to the HashIDs.
    public float angle = 0.0f;


    private Camera cam;
    private Object_Dictionary dictionary;
    private Inventory_Manager obj_inventory;
    Checkpoint_Manager save_manager;

    //Hammer variable
    public GameObject hammer_collider;
    private GameObject hammer;
    public float Hammer_appear;
    public float Hammer_disappear;
    //[HideInInspector]
    public bool has_hammer = false;
    private bool HammerHit = false;
    private Hammer_Controller hammer_controller;

    //Remote variables
    //[HideInInspector]
    public bool has_remote = false;

    //Audio variables
    private float _time = 2;
    private float _waitime = 0;
    private SoundManager sounds;

    //Variable marteau
    private bool is_hammering = false;
    private GameObject hammer_clone;
    private GameObject dust;
    public Transform ray_geometry;
    private RaycastHit hit;
    private Collider coll_hammer_ground;

    //Spray variables
    private float spray_attack;
    private bool spray_bool = false;

    //Machine variables
    private bool machine_bool = false;
    bool is_down_button = false;
    public int fruit_index = -1;

    // freeze player
    bool is_frozen = false;
    bool is_down_jump;
    public bool is_steep;
    float vertical_angle;
    float horizontal_angle;

    public string isle_name;

    //GameObject Remote/Marteau
    public GameObject remote;
    private GameObject remote_clone;
    public GameObject marteau;
    Transform spawn_hammer_hand;
    Transform spawn_remote_hand;
    Transform hand_right_parent;
    Transform hand_left_parent;
    private bool is_hammer_down = false;
    private bool is_remote_down = false;

    private Minimap_Controller minimap_controller;
    
    private bool is_releve = false;

    public bool is_cant_move = false;
    public float slide_speed = -200f;
    public float wall_slide = 1000f;
    private GameObject slide;
    private GameObject ground_collider;

    public bool is_lava = false;
    public bool is_on_raft = false;

    Transform sp_player_end;

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Wall")
        {
            //print(is_cant_move);
            is_cant_move = false;
        }
    }

    void OnCollisionStay (Collision col)
    {
        for (int i = 0; i < col.contacts.Length; i++)
        {
            if (col.contacts[i].otherCollider.tag == "Ground")
            {
                vertical_angle = Mathf.Acos(col.contacts[i].normal.y) * Mathf.Rad2Deg;
                horizontal_angle = transform.rotation.eulerAngles.y - Mathf.Atan2(-col.contacts[i].normal.x, -col.contacts[i].normal.z) * Mathf.Rad2Deg;
                //print(horizontal_angle);

                if (vertical_angle > 40)
                {
                    is_steep = true;
                }
                else
                {
                    is_steep = false;
                }
            }
        }
        switch (col.gameObject.tag)
        {
            case ("Wall"):
                is_cant_move = false;
                if (!is_grounded)
                {
                    is_cant_move = true;
                    //MovementManagement(0, 0);
                    //Rotating(0, 0);

                    float z = 0;
                    if (!Physics.Raycast(transform.position, transform.forward, 2f))
                    {
                        z = wall_slide;
                    }

                    Vector3 force = transform.rotation * new Vector3(0, 0, z);
                    Vector3 force_2 = col.contacts[0].normal * 20;
                    Debug.DrawLine(col.contacts[0].point, col.contacts[0].point + force_2);
                    this_rigidbody.AddForce(force_2);
                    this_rigidbody.AddForce(force);
                }
                break;

            default:
                break;
        }
        
    }

    void Awake ()
	{
		// Setting up the references.
		anim_player = GetComponent<Animator>();
        this_rigidbody = GetComponent<Rigidbody>();
		hash = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneHashIDs>();
        remote = (GameObject)Resources.Load("Prefabs/telecommande 1", typeof(GameObject));
        hammer = (GameObject)Resources.Load("Prefabs/Marteau 1 1 1", typeof(GameObject));
        dust = (GameObject)Resources.Load("Prefabs/HammerDust", typeof(GameObject));
        anim_player.SetLayerWeight(1, 1f);
	}
	
    void Start()
    {    
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        try
        {
          sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        }
        catch
        {
            throw new System.Exception("Il manque le prefab SoundManager ou le prefab n'est pas  la bonne version, peser sur revert pour r�soudre");
        }
        save_manager = dictionary.Get_Checkpoint_Manager();
        obj_inventory = dictionary.Get_Inventory();
        cam = dictionary.Get_Camera();
        hammer_controller = GetComponent<Hammer_Controller>();
        spawn_hammer_hand = transform.FindChild("Enfant_Mort").FindChild("Pelvis").FindChild("Ventre_01").FindChild("clavicule_Droite_01").FindChild("Coude_Droit_01").FindChild("Main_Droite_01").FindChild("Spawn_hammer_hand");
        spawn_remote_hand = transform.FindChild("Enfant_Mort").FindChild("Pelvis").FindChild("Ventre_01").FindChild("Clavicule_Gauche_01").FindChild("Coude_Gauche_01").FindChild("Main_Gauche").FindChild("Spawn_remote_hand");
        hand_right_parent = transform.FindChild("Enfant_Mort").FindChild("Pelvis").FindChild("Ventre_01").FindChild("clavicule_Droite_01").FindChild("Coude_Droit_01").FindChild("Main_Droite_01");
        hand_left_parent = transform.FindChild("Enfant_Mort").FindChild("Pelvis").FindChild("Ventre_01").FindChild("Clavicule_Gauche_01").FindChild("Coude_Gauche_01").FindChild("Main_Gauche");
        minimap_controller = GameObject.Find("UI canvases").transform.FindChild("HUDCanvas").FindChild("Minimap").GetComponent<Minimap_Controller>();
        
        if (start != -1)
        {
            dictionary.Get_Checkpoint_Manager().Set_Checkpoint(start);
            dictionary.Get_Checkpoint_Manager().Load();
        }
    }

    private RaycastHit[] hits;
	void Update ()
    {

        //print(is_cant_move);
        if (remote.activeInHierarchy)
        {
            StartCoroutine(sounds.play_manette());
        }
       

        
        if (!is_frozen)
        {
            hits = Physics.RaycastAll(transform.position, Vector3.down, 100);

            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform.parent != null)
                {
                    if (hits[i].transform.parent.name.StartsWith("Isle"))
                    {
                        isle_name = hits[i].transform.parent.name;
                    }
                }
            }
            
            h = Input.GetAxis("Horizontal");
            v = Input.GetAxis("Vertical");
            //Physics.gravity = new Vector3(0, -50.0f, 0);

            Vector2 hv = new Vector2(h, v);

            if (is_moving && _waitime > _time)
            {
                sounds.play_fstepsR();
                _waitime = 0;
            }

            _waitime += 0.1f;
            if (hv.magnitude < speed_threshold)
            {
                is_moving = false;
            }
            else
            {
                if (true)
                {
                    angle = -cam.transform.eulerAngles.y;
                    is_moving = true;
                }
            }

            h = Mathf.Cos(Mathf.Deg2Rad * angle) * hv.x - Mathf.Sin(Mathf.Deg2Rad * angle) * hv.y;
            v = Mathf.Sin(Mathf.Deg2Rad * angle) * hv.x + Mathf.Cos(Mathf.Deg2Rad * angle) * hv.y;
        }
     
    }

    void FixedUpdate()
    {
        if (!is_hammering)
        {
            MovementManagement(h, v);
            
            //bool spray = Input.GetButton("Spray");

            if (is_grounded)
            {

                if (Input.GetButtonDown("Jump"))
                {
                    if (!is_jump_down)
                    {
                        //print("after get button");
                        sounds.play_jump();
                        Jump();
                        is_jump_down = true;
                    }
                }
                else if (is_jump_down)
                {
                    is_jump_down = false;
                }
              

                if (has_hammer && fruit_index == -1)
                {
                    if (Input.GetButtonDown("Hammer"))
                    {
                        if (!is_hammer_down)
                        {
                            MovementManagement(0.0f, 0.0f);
                            Hammer();
                            is_hammer_down = true;
                        }
                    }
                    else if (is_hammer_down)
                    {
                        is_hammer_down = false;
                    }
                }

                if (has_remote && fruit_index == -1)
                {
                    if (Input.GetButtonDown("Remote"))
                    {
                        if (!is_remote_down)
                        {
                            Remote();
                            StartCoroutine(sounds.play_manette());
                            is_remote_down = true;
                        }
                        /*
                        else if (is_remote_down)
                        {
                            is_remote_down = false;
                        }
                        */
                    }
                }
            }
        }
    }

    
	public void MovementManagement (float horizontal, float vertical)
	{
        if (horizontal != 0f || vertical != 0f)
        {
            Rotating(h, v);
        }

        if (!is_cant_move)
        {
            Vector3 direction = Vector3.forward;

            if (horizontal != 0f || vertical != 0f)
            {
                speed = new Vector2(h, v).magnitude * max_speed;
                float water_height = dictionary.Get_Water().transform.position.y;

                if (transform.position.y - water_height < 0)
                {
                    speed *= Mathf.Pow(3, transform.position.y - water_height);
                    if(transform.position.y - water_height < -2)
                    {
                        GetComponent<Player_Health>().Set_Health(0);
                    }
                }

                anim_player.SetFloat(hash.speedFloat, speed, speedDampTime, Time.deltaTime);
                direction = transform.forward;

                if (is_steep)
                {
                    direction = transform.forward * Mathf.Abs(Mathf.Sin(horizontal_angle * Mathf.Deg2Rad));
                }
                transform.position = transform.position + direction * speed * Time.deltaTime;
            }
            else
            {
                anim_player.SetFloat(hash.speedFloat, 0);
            }
        }
    }	
	
	public void Rotating (float horizontal, float vertical)
	{
        // Create a new vector of the horizontal and vertical inputs.
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
       
        // Create a rotation based on this new vector assuming that up is the global y axis.
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
		
		// Create a rotation that is an increment closer to the target rotation from the player's rotation.
		Quaternion newRotation = Quaternion.Lerp(GetComponent<Rigidbody>().rotation, targetRotation, turnSmoothing * Time.deltaTime);
		
		// Change the players rotation to this new rotation.
		GetComponent<Rigidbody>().MoveRotation(newRotation);
    }

    private void Jump()
    {
        //print("in Jump");
        anim_player.SetBool(hash.jumpBool, true);
        StartCoroutine(Disable_anim_Jump());
    }

    public bool Set_Grounded(bool _is_grounded)
    {
        if(_is_grounded && !is_grounded)
        {
            sounds.play_drop((int)SoundManager.collide_object.ground);
        }
        is_grounded = _is_grounded;

        anim_player.SetBool(hash.groundBool, _is_grounded);
        anim_player.SetBool(hash.fallBool, !_is_grounded);

        return is_grounded;
    }

    public void Climb(Vector3 position)
    {
        transform.position = position;
    }

    public void Hammer()
    {
        is_hammering = true;
        StartCoroutine(Spawning_Hammer());
        if (hammer_collider == true)
        {
            StartCoroutine(WaitHammer());
        }
    }
    public void Remote()
    {
        //print("remote manage");
        anim_player.SetBool(hash.remoteBool, true);
        remote_clone = Instantiate(remote, spawn_remote_hand.position, spawn_remote_hand.rotation) as GameObject;
        remote_clone.transform.parent = hand_left_parent.transform;
        StartCoroutine(Disable_anim_Remote());
        dictionary.Get_Water_Controller().Toggle_level();
    }

    IEnumerator Spawning_Hammer()
    {
        anim_player.SetBool(hash.smashBool, true);
        sounds.play_swing();
        yield return new WaitForSeconds(0.2f);
        //hammer.SetActive(true);
        hammer_clone = (GameObject)Instantiate(hammer, spawn_hammer_hand.position, spawn_hammer_hand.rotation);
        hammer_clone.transform.parent = hand_right_parent.transform;
        anim_player.SetBool(hash.smashBool, false);
        
        yield return StartCoroutine(Whack_Hammer());
    }

    private IEnumerator Whack_Hammer()
    {
        yield return new WaitForSeconds(0.0f);
        hammer_collider.SetActive(true);

        //Instantiate(dust, hit.point, Quaternion.Euler(90.0f, 0.0f, 0.0f));
        //anim_player.SetBool(hash.smashBool, false);

        sounds.play_hit();
        //print("enable");
        //hammer_clone.transform.FindChild("Spawn_Dust").GetComponent<Collider>().enabled = true;
        yield return new WaitForSeconds(0.3f);
        Destroy(hammer_clone);
        hammer_collider.SetActive(false);
        yield return new WaitForSeconds(0.3f);
    }

    IEnumerator WaitHammer()
    {
        //print("4.5.6...");
        yield return new WaitForSeconds(Hammer_disappear);
        is_hammering = false;
    }

    void OnCollisionEnter(Collision other)
    {
        is_on_raft = false;
        switch (other.gameObject.tag)
        {
            case ("Hammer"):
                Set_Hammer(true);
                dictionary.Get_Game_Controller().ui_manager.Display_Action("Hammer");
                save_manager.Set_Checkpoint(2);
                save_manager.Save_Inventory();
                sounds.play_getObj();
                Destroy(other.gameObject);
                Destroy(GameObject.Find("NewItem(Clone)"));
                minimap_controller.Destroy_Hammer_Location();
                break;

            case ("Remote"):
                Set_Remote(true);
                dictionary.Get_Game_Controller().ui_manager.Display_Action("Remote");
                save_manager.Set_Checkpoint(4);
                save_manager.Save_Inventory();
                sounds.play_getObj();
                Destroy(other.gameObject);
                Destroy(GameObject.Find("NewItem(Clone)"));
                minimap_controller.Destroy_Remote_Location();
                break;
                

            case ("Chest"):
                is_grounded = true;
                is_cant_move = false;
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            case ("Washer"):
                is_grounded = true;
                is_cant_move = false;
                sounds.play_drop((int)SoundManager.collide_object.washer);
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            case ("Nest"):
                is_grounded = true;
                is_cant_move = false;
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            case ("Raft"):
                is_on_raft = true;
                is_grounded = true;
                is_cant_move = false;
                sounds.play_drop((int)SoundManager.collide_object.raft);
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            case ("Terrier"):
                is_grounded = true;
                is_cant_move = false;
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            case ("Sock"):
                is_grounded = true;
                is_cant_move = false;
                anim_player.SetBool(hash.groundBool, true);
                anim_player.SetBool(hash.fallBool, false);
                break;

            default:
                break;
        }
    }

    public bool Has_Hammer()
    {
        return has_hammer;
    }

    public bool Has_Remote()
    {
        return has_remote;
    }
    
    public void Set_Hammer(bool _has_hammer)
    {
        has_hammer = _has_hammer;
        obj_inventory.Set_Hammy(_has_hammer);
    }

    public void Set_Remote(bool _has_remote)
    {
        has_remote = _has_remote;
        obj_inventory.Set_Remmy(_has_remote);
    }

    public void Spray()
    {
    }
    
    public IEnumerator Disable_anim_throw()
    {
        anim_player.SetBool(hash.holdBool, false);
        dictionary.Get_Fruit_Controller(fruit_index).GetComponent<SphereCollider>().isTrigger = false;
        dictionary.Get_Fruit_Controller(fruit_index).GetComponent<BoxCollider>().isTrigger = false;
        yield return new WaitForSeconds(0.1f);
        anim_player.SetBool(hash.throwBool, false);
        fruit_index = -1;
    }

    public IEnumerator Disable_anim_eating()
    {
        yield return new WaitForSeconds(0.1f);
        anim_player.SetBool(hash.holdBool, false);
        anim_player.SetBool(hash.eatBool, false);
        fruit_index = -1;
    }

    IEnumerator Disable_anim_Jump()
    {
        float factor = 0f;
        if (transform.position.y < dictionary.Get_Water().transform.position.y)
        {
            factor = 4 * (dictionary.Get_Water().transform.position.y - transform.position.y);
        }
        is_cant_move = false;
        this_rigidbody.velocity = new Vector3(this_rigidbody.velocity.x, jump_power - factor, this_rigidbody.velocity.z);
        //print("Assign velocity");
        yield return new WaitForSeconds(0.25f);
        anim_player.SetBool(hash.jumpBool, false);
    }

    IEnumerator Disable_anim_Remote()
    {
        yield return new WaitForSeconds(0.2f);
        anim_player.SetBool(hash.remoteBool, false);
        yield return new WaitForSeconds(0.7f);
        Destroy(hand_left_parent.FindChild("telecommande 1(Clone)").gameObject);
        anim_player.SetBool(hash.remoteBool, false);
        is_remote_down = false;
    }
    /*IEnumerator Disable_anim_releve()
    {
        yield return new WaitForSeconds(0.5f);
        anim_player.SetBool(hash.releveBool, false);
    }*/

    public void Freeze(bool state)
    {
        is_frozen = state;
        h = 0.0f;
        v = 0.0f;
    }

    public void Get_Map_Position(out float x, out float z, out float angle)
    {
        x = transform.position.x;
        z = transform.position.z;
        angle = transform.rotation.eulerAngles.y;
    }

    public void Resurrect()
    {
        anim_player.SetTrigger("Resurrect");
    }
    public Vector3 Get_Velocity()

   {
        return transform.forward* speed;

    }

    public IEnumerator Taking()
    {
        float timer = 0f;
        while (timer < 1.4f)
        {
            yield return null;
            timer += Time.deltaTime;
        }
        is_taking = true;
    }
    
}


