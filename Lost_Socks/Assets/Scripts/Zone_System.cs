﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Zone_System : MonoBehaviour
{
    public int hideout_value = 10;

    public List<Transform> all_zones_list;
    public List<Transform> zones_not_underwater;
    public List<Transform> hideout_position;

    //public Transform closer_nest_zone;

    private GameObject player;
    private Object_Dictionary dictionary;
    public GameObject[] socks_list;


    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        player = dictionary.Get_Player();
        Transform[] zones_array = transform.GetComponentsInChildren<Transform>();
        for (int i = 1; i < zones_array.Length; i++)
        {
            zones_array[i].GetComponent<Zone_Controller>().Setup(i);

            all_zones_list.Add(zones_array[i]);
            if (!zones_array[i].GetComponent<Zone_Controller>().is_underwater)
            {
                zones_not_underwater.Add(zones_array[i]);
            }
        }
        socks_list = GameObject.FindGameObjectsWithTag("Socks");

        Set_Start_Zone(socks_list);

        Make_Graph(zones_not_underwater);
        Make_Hideout_List(zones_not_underwater);
        /*
        for (int i = 0; i < all_zones_list.Count; i++)
        {
            if (all_zones_list[i].GetComponent<Zone_Controller>().nest_zone)
            {
                nest_position.Add(all_zones_list[i]);
                Zone_Controller zone_controller_script = all_zones_list[i].GetComponent<Zone_Controller>();
                zone_controller_script.birds_variable_value = nest_value;
                zone_controller_script.Initiate_Birds_Value();
            }
        }
        */

    }

    void Set_Start_Zone(GameObject[] list)
    {
        //Debug.Log(list[0].transform);
        for (int i = 0; i < list.Length; i++)
        {
            Vector3 sock_position = list[i].transform.position;
            Transform closer_zone = Get_Closer_Zone(sock_position);
            //Debug.Log(list[i].transform.position);
            if (list[i].tag == "Socks")
            {
                list[i].GetComponent<Socks_Controller>().Set_Present_Zone(closer_zone);
            }
            /*else if(list[i].tag == "Birds")
            {
                list[i].GetComponent<StatePatternBird>().Set_Present_Zone(closer_zone);
            }*/
        }
    }

    public void Change_Water_Level(string level)
    {
        if (level == "low")
        {
            Make_Graph(all_zones_list);
            Make_Hideout_List(all_zones_list);
        }
        if (level == "high")
        {
            Make_Graph(zones_not_underwater);
            Make_Hideout_List(zones_not_underwater);
        }
    }

    void Make_Graph(List<Transform> zones_list)
    {
        for (int i = 0; i < zones_list.Count; i++)
        {
            for (int j = 0; j < zones_list.Count; j++)
            {
                float relative_max_distance = (zones_list[i].GetComponent<SphereCollider>().radius + zones_list[j].GetComponent<SphereCollider>().radius);
                float zones_distance = Vector3.Distance(zones_list[i].transform.position, zones_list[j].transform.position);
                if (zones_distance < relative_max_distance && zones_distance > 0)
                {
                    zones_list[i].GetComponent<Zone_Controller>().zone_connected.Add(zones_list[j]);

                    //*Debugging pour l'affichage du graph de déplacement
                    Vector3 zone_position_1 = zones_list[i].transform.position;
                    Vector3 zone_position_2 = zones_list[j].transform.position;
                    zone_position_1.y = 1;
                    zone_position_2.y = 1;
                    Debug.DrawLine(zone_position_1, zone_position_2, Color.red, 1000.0f);
                    //*Fin du debugging
                }
            }
        }
    }

    List<Transform> Make_Hideout_List(List<Transform> zones_list)
    {
        List<Transform> new_hideouts_list = new List<Transform>();
        for (int i = 0; i < zones_list.Count; i++)
        {

            if (zones_list[i].GetComponent<Zone_Controller>().hideout_zone)
            {
                new_hideouts_list.Add(zones_list[i]);
                Zone_Controller zone_controller_script = zones_list[i].GetComponent<Zone_Controller>();
                zone_controller_script.security_value = hideout_value;
                zone_controller_script.Initiate_Security_Value();
            }
        }
        return new_hideouts_list;
    }

    // Update is called once per frame
    void Update()
    {

    }

    List<Transform> Multiply_Zones(List<Transform> zones_multiply, List<int> multipy_values)
    {
        List<Transform> zones_by_value = new List<Transform>();
        for (int i = 0; i < zones_multiply.Count; i++)

        {
            for (int j = 0; j < multipy_values[i]; j++)
            {
                zones_by_value.Add(zones_multiply[i]);
            }
        }
        return zones_by_value;
    }

    public Transform Get_Socks_Next_Zone(Transform present_zone, bool fleeing)
    {
        Transform present_player_zone = player.GetComponent<Player_Movement>().present_zone;
        List<Transform> zone_connected = present_zone.GetComponent<Zone_Controller>().zone_connected;
        List<int> zones_values = new List<int>();
        for (int i = 0; i < zone_connected.Count; i++)
        {

            if (zone_connected[i] == present_player_zone)
            {
                zones_values.Add(0);
            }
            if (zone_connected[i] == present_zone)
            {
                zones_values.Add(0);
            }
            else
            {
                if (fleeing)
                {
                    zones_values.Add(zone_connected[i].GetComponent<Zone_Controller>().Get_Security_Value());
                }
                else
                {
                    zones_values.Add(zone_connected[i].GetComponent<Zone_Controller>().Get_Socks_Value());
                }
            }
        }
        List<Transform> zones_by_value = Multiply_Zones(zone_connected, zones_values);
        
        var random = Random.Range(0, zone_connected.Count - 1);
        Transform next_zone = present_zone;
        //  Transform next_zone = zones_by_value[Random.Range(0, zones_by_value.Count)];
        if (zones_by_value.Count > 0)
        {
            next_zone = zones_by_value[random];
        }
        // Transform next_zone = present_zone;
        return next_zone;
    }

    public Vector3 Get_Hideout(Vector3 present_position)
    {
        Vector3 closer_hideout_position = new Vector3();
        float smallest_distance = 1000.0f;
        for (int i = 0; i < hideout_position.Count; i++)
        {
            

            float new_distance = Vector3.Distance(hideout_position[i].GetComponent<Zone_Controller>().hideout_transform.position, present_position);
            if (smallest_distance > new_distance)
            {
                smallest_distance = new_distance;
                closer_hideout_position = hideout_position[i].GetComponent<Zone_Controller>().hideout_transform.position;
            }
        }
        return closer_hideout_position;
    }

    public Transform Get_Closer_Zone(Vector3 present_position)
    {

        //Debug.Log("enter0");
        Transform closer_zone = null;
        float smallest_distance = 1000.0f;
        //Debug.Log(zones_not_underwater.Count);
        for (int i = 0; i < zones_not_underwater.Count; i++)
        {
            //Debug.Log("enter1");
            float new_distance = Vector3.Distance(zones_not_underwater[i].position, present_position);
            if (smallest_distance > new_distance)
            {
                smallest_distance = new_distance;
                closer_zone = zones_not_underwater[i];
                //Debug.Log("enter2");
            }
        }


        return closer_zone;
    }


    /*Fonction pour l'oiseau qui trouver le nid le plus proche de la position
	public Transform Get_Nest(Vector3 present_position)
	{
		float smallest_distance = 1000.0f;
		for (int i = 0; i < nest_position.Count; i++) 
		{
			float new_distance = Vector3.Distance(nest_position[i].GetComponent<Zone_Controller> ().nest_transform.position, present_position);
			if(smallest_distance > new_distance)
			{
				smallest_distance = new_distance;
				closer_nest_zone = nest_position[i];
			}
		}
		return closer_nest_zone;
	}*/

    public Transform Get_Bird_Next_Zone(Transform present_zone)
    {
        List<int> birds_values = new List<int>();
        for (int i = 0; i < all_zones_list.Count; i++)
        {
            if (all_zones_list[i] == present_zone)
            {
                birds_values.Add(0);
            }
            else
            {
                birds_values.Add(all_zones_list[i].GetComponent<Zone_Controller>().Get_Birds_Value());
            }
        }
        List<Transform> zones_by_value = Multiply_Zones(all_zones_list, birds_values);
        Transform next_zone = zones_by_value[Random.Range(0, zones_by_value.Count)];
        return next_zone;
    }

}