﻿using UnityEngine;
using System.Collections;

public class Socks_Dying : MonoBehaviour 
{
	private Socks_Controller socks_controller;
	
	// Use this for initialization
	void Start ()
	{
		socks_controller = GetComponentInParent<Socks_Controller>();
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Spray") 
		{
			if (socks_controller.is_alive) 
			{
				socks_controller.Is_Dying ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
