﻿using UnityEngine;
using System.Collections;

public class Back_of_Camera : MonoBehaviour {
    float distance = 0.2f;
	// Update is called once per frame
	void LateUpdate () {
        Vector3 new_position = transform.parent.position + distance * (transform.parent.position - Camera.main.transform.position).normalized;
        transform.position = new_position;
	}
}
