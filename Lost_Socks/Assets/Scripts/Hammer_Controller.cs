﻿using UnityEngine;
using System.Collections;

public class Hammer_Controller : MonoBehaviour
{
    private GameObject rock;
    private GameObject small_rock;
    private Rigidbody petit_rockR;
    
    private SpawnManager spawn_manager;
    private Object_Dictionary obj_dicto;
    private bool is_Colliding;

    void Start()
    {
        rock = GameObject.FindWithTag("Rock");
        spawn_manager = GameObject.Find("Game Controller").GetComponent<SpawnManager>();
        obj_dicto = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();



    }
    void FixedUpdate()
    {
        is_Colliding = false;
    }

    void OnTriggerEnter (Collider other)
    {
        
        if (is_Colliding) { return; }
        is_Colliding = true;

        if (other.tag == "Rock")
        {
            Transform t = other.gameObject.transform;
            obj_dicto.Crash_Rock(other);
            //StartCoroutine(other.GetComponent<Rock_Controller>().Crash_Rock());
            small_rock = obj_dicto.Instantiate_smallRock(t);
            
            petit_rockR = small_rock.GetComponent<Rigidbody>();         
            small_rock.transform.position = t.position;

            petit_rockR.velocity = new Vector3(0f, 1f, 0f);
            petit_rockR.velocity *= 10f;

        }
        if(other.tag == "Terrier")
        {
           StartCoroutine(spawn_manager.HammerSpawn(other.transform));
        }
        if (other.tag == "Bird")
        {
            print("yo bird");
        }
    }
}
