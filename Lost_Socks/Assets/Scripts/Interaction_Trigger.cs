﻿using UnityEngine;
using System.Collections;

public class Interaction_Trigger : MonoBehaviour {
    private Player_Movement player_movement;
    private Game_Controller game_controller;
    private Object_Dictionary dictionary;
    

	// Use this for initialization
	void Start () {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        player_movement = dictionary.Get_Player_Movement();
        game_controller = GameObject.Find("Game Controller").GetComponent<Game_Controller>();
        
	}
	
	void OnTriggerEnter (Collider other)
    {
        switch(other.tag)
        {
            case ("Washer"):
                game_controller.Set_Available_Interaction(other);
                break;

            case ("Fruits"):
                game_controller.Set_Available_Interaction(other);
                break;

            default:
                break;
        }
      
    }

    void OnTriggerExit (Collider other)
    {
        switch(other.tag)
        {
            case ("Washer"):
                game_controller.Set_Unavailable_Interaction(other);
                break;

            case ("Fruits"):
                game_controller.Set_Unavailable_Interaction(other);
                break;

            default:
                break;
        }

    }
}
