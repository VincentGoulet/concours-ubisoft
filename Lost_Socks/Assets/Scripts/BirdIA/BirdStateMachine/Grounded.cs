﻿using UnityEngine;
using System.Collections;

public class Grounded : MonoBehaviour {
    private StatePatternBird _bird;

    // Use this for initialization
    void Start () {
        _bird = this.transform.parent.GetComponent<StatePatternBird>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            _bird.navMeshAgent.enabled = true;
            _bird.IsGrounded = true;
            this.gameObject.SetActive(false);
            _bird.attack.SetActive(true);
            _bird.anim.SetBool("Marche", true);
        }
    }

  void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            _bird.IsGrounded = false;
        }
    }
}
