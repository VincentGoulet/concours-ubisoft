﻿using UnityEngine;
using System.Collections;

public class ChaseState : IBirdState
{

    // Use this for initialization
    private readonly StatePatternBird _bird;
    private GameObject _player;
    public NavMeshAgent _agent;

    bool isChasing = true;

    public ChaseState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
        _agent = _bird.navMeshAgent;
        _player = GameObject.Find("Player");
    }

    public void UpdateState()
    {
        if (_bird.IsGrounded == false)
        {
            _bird.transform.Translate(_bird.transform.up * -1 * _bird.speed / 2 * Time.deltaTime);
        }
        else
        {
            if (_bird.IsAttacking == false)
            {
                _agent.SetDestination(_player.transform.position);
            }
            if (Vector3.Distance(_bird.transform.position, _player.transform.position) > 15f)
            {
               // _bird.grounded.SetActive(false);
               // _bird.attack.SetActive(false);
                _bird.IsAngry = false;
                _bird.anger = Random.Range(0, 4);
                _bird.navMeshAgent.Stop();
                _bird.navMeshAgent.enabled = false;
                _bird.IsGrounded = false;
                _bird.currentState = _bird.idleState;
                _bird.anim.SetBool("Idle", true);
                _bird.anim.SetBool("Marche", false);

            }
        }

    }

    public void OnCollisionEnter(Collision collision)
    {

    }

    public void OnTriggerEnter(Collider other)
    {

    }

    public void OnTriggerExit(Collider other)
    {

    }

    public void ToAttackState()
    {
    }

    public void ToBackNestState()
    {
    }

    public void ToIdleState()
    {
    }

    public void ToPatrolState()
    {

    }

    public void ToRestState()
    {
    }

    public void ToChaseSate()
    {

    }

}
