﻿using UnityEngine;
using System.Collections;

public class RestState : IBirdState
{

    private readonly StatePatternBird _bird;
    private float timer = 0f;

    public int maxStamina = 100;
    public int restUp = 1;

    public void UpdateState()
    {
        Rest();
    }
    public void OnCollisionEnter(Collision collision)
    {

    }

    public void StartC()
    {
        _bird.UpStamina();
    }

    public RestState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
    }

    public void ToPatrolState()
    {

    }

    public void ToAttackState()
    {

    }

    public void ToBackNestState()
    {

    }

    public void ToIdleState()
    {

    }

    public void ToRestState()
    {

    }

    public void BackToNest()
    {

    }

    public void OnTriggerEnter(Collider other)
    {

    }

    public void Rest()
    {
        if (_bird.stamina >= maxStamina)
        {
            _bird.IsResting = false;
            _bird.anim.SetBool("Idle", false);
            _bird.DownStamina();
            _bird.currentState = _bird.patrolState;
            _bird.patrolState._target_position.Pop();
            _bird.patrolState.SetNearestWayPoint();
            _bird.IsBack = false;
            //  Debug.Log(_bird.patrolState._target_position.Peek());
        }

    }

    public void ToChaseSate()
    {
    }

    public void OnTriggerExit(Collider other)
    {
    }
}
