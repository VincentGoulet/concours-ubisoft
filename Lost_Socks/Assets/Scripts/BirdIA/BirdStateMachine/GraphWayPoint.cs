﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GraphWayPoint : MonoBehaviour {

    public List<GameObject> gameObjectWayPoints;
    private GameObject FlyingWaypoints;

    // Use this for initialization
    void Start () {
        gameObjectWayPoints = new List<GameObject>();
        FlyingWaypoints = GameObject.Find("FlyingWaypoints");
        FillWayPointsList();
        CreateGraph();
    }
	
	// Update is called once per frame
	void Update () {
      //  PrintGraph();

    }


    void FillWayPointsList()
    {
        int i = 0;
        foreach (Transform child in FlyingWaypoints.transform)
        {
            gameObjectWayPoints.Add(child.transform.gameObject);
            child.gameObject.GetComponent<WayPoint>().number = i;
            i++;
        }
    }

    void CreateGraph()
    {
        foreach (GameObject gameObjectWaypoint in gameObjectWayPoints)
        {
            LinkGraph(gameObjectWaypoint);
        }
    }

    void LinkGraph(GameObject gameObjectWaypoint)
    {
        WayPoint wayPoint = gameObjectWaypoint.GetComponent<WayPoint>();
        foreach (Transform child in FlyingWaypoints.transform)
        {
            RaycastHit hit;
           // Debug.Log("Name : " + child.transform.gameObject.name + " " + gameObjectWaypoint.transform.position + " " + child.transform.position);
            if (gameObjectWaypoint.transform.position != child.transform.position)
            {
                if (Physics.Raycast(gameObjectWaypoint.transform.position, (child.transform.position - gameObjectWaypoint.transform.position), out hit))
                {
                  //  Debug.Log(hit.transform.gameObject.name);
                    if (hit.transform.position == child.transform.position)
                    {
                        wayPoint.gameObjectWayPoints.Add(child.transform.gameObject);
                    }
                }

            }
        }
    }

    void PrintGraph()
    {
        foreach (GameObject gameObjectWaypoint in gameObjectWayPoints)
        {
            foreach (Transform child in FlyingWaypoints.transform)
            {
                RaycastHit hit;
               //  Debug.Log("Name : " + child.transform.gameObject.name + " " + gameObjectWaypoint.transform.position + " " + child.transform.position);
                if (gameObjectWaypoint.transform.position != child.transform.position)
                {
                    if (Physics.Raycast(gameObjectWaypoint.transform.position, (child.transform.position - gameObjectWaypoint.transform.position), out hit))
                    {
                        //  Debug.Log("Hit");
                        if (hit.transform.position == child.transform.position)
                        {
                            Debug.DrawRay(gameObjectWaypoint.transform.position, (child.transform.position - gameObjectWaypoint.transform.position), Color.red);
                        }
                    }

                }
            }
        }
    }
}
