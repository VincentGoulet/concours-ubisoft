﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BirdCollider : MonoBehaviour
{

    public int sock = -1;
    public string type = null;
    public int terrier = -1;
    public Material material;

    private HuntTrigger huntTrigger;
    private StatePatternBird _bird;
    private Object_Dictionary _dictionary;

    bool damage_playing;
    int attackHash;
    int stunHash;
    int takeDamageHash;
    // Use this for initialization
    void Start()
    {
        huntTrigger = this.transform.parent.GetChild(1).GetComponent<HuntTrigger>();
        _bird = this.transform.parent.GetComponent<StatePatternBird>();
        attackHash = Animator.StringToHash("Attack");
        stunHash = Animator.StringToHash("Stunned");
        takeDamageHash = Animator.StringToHash("TakeDamage");
    }

    private IEnumerator WaitForAnimation(float time, Collider other)
    {

        yield return new WaitForSeconds(time);
      

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            if (!_bird.has_socks)
            {
                //Debug.Log(_bird.gameObject + "Attrape sock");
                _bird.anim.SetTrigger(attackHash);
                _bird.sounds.play_birdTakeSock(_bird.gameObject.GetComponent<AudioSource>());
                sock = 1;
                _bird.has_socks = true;
                Socks_Controller sc = other.gameObject.GetComponent<Socks_Controller>();
                sc.nav.Stop();
                type = other.gameObject.name.Split('_')[1];
                terrier = sc.Get_myTerrier();
                huntTrigger.inventory_socks.Remove(other.gameObject);
                Destroy(other.gameObject);
                _bird.IsIdle = true;
                _bird.anim.SetBool("HasSock", true);
                _bird.me = false;
             //   StartCoroutine(WaitForAnimation(_bird.anim.GetCurrentAnimatorStateInfo(0).length - 0.2f, other));
            }
        }

        if (other.gameObject.tag == "Hammer" || other.gameObject.tag == "Spray")
        {

            StartCoroutine(Damage_Visual());
            _bird.anger += Random.Range(2, 4);
            if (other.gameObject.tag == "Hammer")
            {
                _bird.stamina -= 35;
                _bird.IsStunned = true;
                _bird.anim.SetTrigger(stunHash);
                _bird.sounds.play_birdHurt(_bird.gameObject.GetComponent<AudioSource>());
                //_dictionary.Get_Bubble_Popper().Pop_Stun(_bird.transform);
                StartCoroutine(Stun(2f));
            }
            else if (other.gameObject.tag == "Spray")
            {
                _bird.stamina -= 20;
                _bird.anim.SetTrigger(takeDamageHash);
                _bird.sounds.play_birdHurt(_bird.gameObject.GetComponent<AudioSource>());
            }

            if (_bird.anger >= _bird.angerMax)
            {
                _bird.IsAngry = true;
                _bird.grounded.SetActive(true);
                //  _bird.attack.SetActive(true);
            }
        }


    }

    IEnumerator Stun(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Debug.Log("test");
        _bird.IsStunned = false;
    }

    float fade_time = 0.3f;
    Color new_color;
    float damage_timer;
    IEnumerator Damage_Visual()
    {
        //print("Entree coroutine");
        damage_playing = true;

        damage_timer = 0f;
        new_color = Color.red;
        Set_Color(new_color);


        while (damage_timer < fade_time)
        {
            yield return null;
            new_color = Color.Lerp(Color.red, Color.white, damage_timer / fade_time);
            Set_Color(new_color);
            damage_timer += Time.deltaTime;
        }

        Set_Color(Color.white);
        damage_playing = false;
        yield return null;
    }

    private void Set_Color(Color color)
    {
        material.color = color;
    }

    public bool IsSockTaken()
    {
        if (sock == -1)
        {
            return false;
        }
        return true;
    }
}
