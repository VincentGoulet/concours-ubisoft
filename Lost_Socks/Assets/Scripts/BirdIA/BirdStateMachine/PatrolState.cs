﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatrolState : IBirdState
{


    public float slerpspeed = 20f;
    private bool is_reach = true;
    public Stack<GameObject> _target_position;
    private Transform _current_zone;
    private GameObject wall;
    private readonly StatePatternBird _bird;
    private GameObject _enemy;

    private Socks_Controller _socks_controller;
    public PatrolState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
        _target_position = new Stack<GameObject>();
        FlyingWaypoints = GameObject.Find("FlyingWaypoints");
    }

    public void UpdateState()
    {
        Patrol();
    }

    public void OnTriggerEnter(Collider other)
    {

    }

    public void OnCollisionEnter(Collision collision)
    {

    }
    public void ToPatrolState()
    {
    }

    public void ToAttackState()
    {

    }
    public void ToBackNestState()
    {

    }
    public void ToRestState()
    {

    }
    public void ToIdleState()
    {

    }

    Vector3 cc = new Vector3(0, 0, 0);

    float r = 5f;
    int sign = 1;

    void Patrol()
    {
            Move();
    }

    bool isTurning = false;

    float turnTimer = 0f;
    float turnTimerMax = 4f;
    Vector3 nextTarget;

    void Get_New_Position()
    {
        WayPoint wayPoint = _bird.currentWayPoint;
        int index = Random.Range(0, wayPoint.gameObjectWayPoints.Count - 1);
       // if (_bird.previousWayPoint == null)
        //{
            while (wayPoint.gameObjectWayPoints[index].transform.position == wayPoint.gameObject.transform.position)
            {
                index = Random.Range(0, wayPoint.gameObjectWayPoints.Count);
            }
    /*    }
        else
        {
            while ((wayPoint.gameObjectWayPoints[index].transform.position == wayPoint.gameObject.transform.position) && (wayPoint.gameObjectWayPoints[index].transform.position != _bird.previousWayPoint.gameObject.transform.position))
            {
                index = Random.Range(0, wayPoint.gameObjectWayPoints.Count);
            }
        }*/
        
        GameObject target = wayPoint.gameObjectWayPoints[index];
        _target_position.Push(target);
    }

    void Move()
    {
        if (_target_position.Count == 0)
        {
            Get_New_Position();
        }
         if (Vector3.Distance(_bird.transform.position, _target_position.Peek().transform.position) <= 0.1f)
        {
            _bird.previousWayPoint = _bird.currentWayPoint;
            _bird.currentWayPoint = _target_position.Peek().GetComponent<WayPoint>();
            _target_position.Pop();
        }
        else
        {
            GameObject temp = _target_position.Peek();
            if (temp.transform.position - _bird.transform.position != Vector3.zero)
            {
                Quaternion q = Quaternion.LookRotation(temp.transform.position - _bird.transform.position);
                q.x = 0;
                q.z = 0;
                _bird.transform.rotation = Quaternion.Slerp(_bird.transform.rotation, q, _bird.speed * Time.deltaTime);
            }
            _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, _target_position.Peek().transform.position, _bird.speed * Time.deltaTime);
        }


    }

    private GameObject FlyingWaypoints;

    public void SetNearestWayPoint()
    {
        Transform nearestWaypointObj = FlyingWaypoints.transform.GetChild(0);
        foreach (Transform child in FlyingWaypoints.transform)
        {
            if (Vector3.Distance(_bird.transform.position, child.position) <= Vector3.Distance(_bird.transform.position, nearestWaypointObj.position))
            {
                nearestWaypointObj = child;
            }
        }
        _target_position.Push(nearestWaypointObj.gameObject);
    }
    /*
        void Move()
        {

            //  GetForwardObstacle();

            float d = _bird.speed * Time.deltaTime;


            if (_target_position.Count == 0)
            {
                Vector3 nw = Get_New_Position();
                while (Vector3.Distance(_bird.transform.position, nw) <= 10)
                    nw = Get_New_Position();
                isTurning = true;
                _target_position.Push(nw);
                Vector3 birdl = _target_position.Peek() ;
                if (_bird.transform.InverseTransformPoint(birdl).x <= 0)
                {
                    cc = _bird.transform.position + r * -1 * _bird.transform.right;
                    sign = 1;
                }
                else
                {
                    cc = _bird.transform.position + r * _bird.transform.right;
                    sign = -1;
                }
            }
            else
            {
                Debug.DrawLine(_bird.transform.position, _target_position.Peek());
                Vector3 birdl = _target_position.Peek();
                if (isTurning == false)
                {
                    _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, _target_position.Peek(), _bird.speed * Time.deltaTime);
                    Vector3 dir = _target_position.Peek() - _bird.transform.position;
                    _bird.transform.rotation = Quaternion.Euler(0f, Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg, 0f);
                }
                else
                {
                    turnTimer += Time.deltaTime;

                    Debug.DrawLine(cc, _bird.transform.position, Color.red);
                    _bird.transform.position = Quaternion.Euler(0f, -sign * (d / r) * Mathf.Rad2Deg, 0f) * (_bird.transform.position - cc) + cc;
                    _bird.transform.rotation = Quaternion.Euler(0f, -sign * (d / r) * Mathf.Rad2Deg + _bird.transform.rotation.eulerAngles.y, 0f);

                    Vector3 newcc = _bird.transform.InverseTransformPoint(cc);
                    Vector3 targetlocal = _bird.transform.InverseTransformPoint(birdl);
                    if (targetlocal.x * newcc.x <= 0)
                        isTurning = false;
                    /*  if (turnTimer >= turnTimerMax)
                      {
                          isTurning = false;
                          _target_position.Pop();
                          _target_position.Push(nextTarget);
                      } 
                }
                if (Vector3.Distance(_bird.transform.position, _target_position.Peek()) <= 0.1)
                {
                    _target_position.Pop();
                }
            }


        }
        */

    /*Vector3 Get_New_Position()
    {
        Transform newPostion = _bird.zone_system.Get_Bird_Next_Zone(_bird.present_waypoint);
        _bird.present_waypoint = newPostion;
        Vector3 targetPosition = Random.insideUnitSphere * newPostion.GetComponent<Zone_Controller>().zone_size + newPostion.position;
        Vector3 newTargetPosition = new Vector3(targetPosition.x, 15, targetPosition.z);
        return (newTargetPosition);
    }
    */

    /*
        public void GetForwardObstacle()
        {
            Vector3 fwd = _bird.transform.TransformDirection(Vector3.forward);
            RaycastHit hit;
            if (Physics.Raycast(_bird.transform.position, fwd, out hit, 5))
            {
                Vector3 pos = new Vector3(hit.transform.position.x, hit.transform.position.y + 1, hit.transform.position.z);
                _target_position.Push(pos);
            }
        }
        */
    public void ToChaseSate()
    {

    }

    public void OnTriggerExit(Collider other)
    {
    }
}
