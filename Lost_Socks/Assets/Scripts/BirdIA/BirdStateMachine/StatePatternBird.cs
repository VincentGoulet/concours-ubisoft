﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatePatternBird : MonoBehaviour
{

    public float searchingTurnSpeed = 120f;
    public float searchingDuration = 4f;
    public float sightRange = 20f;
    public float speedInit = 5f;
    public float speed = 5f;
    public int stamina = 100;
    public int angerMax = 5;
    public int anger = 0;
    public int terrier_sock;
    public string sock_type;
    public GameObject[] spawn_points;
    public Transform present_waypoint;
    public float turnSpeed = 5f;
    public GameObject nestObject;
    public bool rest = false;
    public bool IsBack = false;

    public IBirdState currentState;
    public Transform chaseTarget;

    public PatrolState patrolState;
    public AttackState attackState;
    public BackNestState backNestState;
    public RestState restState;
    public IdleState idleState;
    public ChaseState chaseState;
    public AttackPlayerState attackPlayerState;
    public SoundManager sounds;

    public NavMeshAgent navMeshAgent;
    public GameObject _target;
    public Collider[] _colliders;
    public Zone_System zone_system;
    public bool isLanded = false;
    private int time = 0;
    private Object_Dictionary obj_dict;
    
    private HuntTrigger huntTriggerBox;
    private BirdCollider birdCollider;
    private Nest nest;

    public GameObject gameObjectWayPoints;
    public WayPoint currentWayPoint;
    public WayPoint previousWayPoint;

    public Animator anim;

    private void Awake()
    {
        //  obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        _colliders = GetComponents<Collider>();
        //zone_system = GameObject.Find("Zone_System 3").GetComponent<Zone_System>();
        nest = nestObject.GetComponent<Nest>();
        nestTarget = nestObject.transform.GetChild(0).gameObject;
        gameObjectWayPoints = GameObject.Find("FlyingWaypoints");
        patrolState = new PatrolState(this);
        attackState = new AttackState(this);
        backNestState = new BackNestState(this);
        restState = new RestState(this);
        idleState = new IdleState(this);
        chaseState = new ChaseState(this);

    }

    // Use this for initialization
    void Start()
    {
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        //  spawn_points = obj_dict.Get_Spawn_Points();
        huntTriggerBox = this.transform.GetChild(1).GetComponent<HuntTrigger>();
        birdCollider = this.transform.GetChild(2).GetComponent<BirdCollider>();
        grounded = this.transform.GetChild(3).gameObject;
        attack = this.transform.GetChild(4).gameObject;
        currentState = patrolState;
        StartCoroutine("DecreaseStamina");
        navMeshAgent.enabled = false;
        SetCurrentWayPoint();
        anim = GetComponent<Animator>();
        _target_position = patrolState._target_position;
        StartCoroutine(DeFreeze());
    }

    public GameObject grounded;
    public GameObject attack;

    public GameObject nestTarget;
    public bool IsIdle = false;
    public bool IsAngry = false;
    public bool IsGrounded = false;
    public bool IsAttacking = false;
    public bool has_socks = false;
    public bool IsResting = false;
    public bool IsBacking = false;
    public bool IsStunned = false;
    public bool me = false; 
    public bool PlayMusic = false;

    void SetCurrentWayPoint()
    {
        Transform wayPoint = gameObjectWayPoints.transform.GetChild(0);
        foreach (Transform child in gameObjectWayPoints.transform)
        {
            if (Vector3.Distance(transform.position, child.position) <= Vector3.Distance(transform.position, wayPoint.position))
            {
                wayPoint = child;
            }
        }
        currentWayPoint = wayPoint.gameObject.GetComponent<WayPoint>();
        patrolState._target_position.Push(wayPoint.gameObject);
    }

    public Stack<GameObject> _target_position;

    void Update()
    {
        if (IsStunned == false)
        {
            if (stamina <= 0 && IsBack == false)
            {
                if (navMeshAgent.enabled == true)
                {
                    navMeshAgent.Stop();
                    navMeshAgent.enabled = false;
                    IsGrounded = false;
                }
                if (IsBacking == false)
                {
                    anim.SetBool("Marche", false);
                    IsBacking = true;
                    backNestState._target_position.Push(currentWayPoint.gameObject);
                }
            //    Debug.Log("back to nest 2");
                currentState = backNestState;
            }
            else
            {
                if (IsAngry == true && stamina > 0)
                {
                    currentState = chaseState;
              //      Debug.Log("Chase");
                }
                else
                {
                    if (IsIdle == true)
                    {
                        currentState = idleState;
                        //         Debug.Log("Idle");
                    }
                    else
                    {
                        if (IsBack == true)
                            currentState = restState;
                        else
                        {
                            if (has_socks == false && nest.IsFull() == false && huntTriggerBox.IsInventorySocksEmpty() == false)
                            {
                                if (huntTriggerBox.inventory_socks[0] != null)
                                {
                                    var socksController = huntTriggerBox.inventory_socks[0].GetComponent<Socks_Controller>();
                                    attackState.target_position = huntTriggerBox.inventory_socks[0].transform.position;
                                    currentState = attackState;
                                    
                                    
                                }

                                //     Debug.Log("Attack");
                            }
                            else if (has_socks == true && nest.IsFull() == false)
                            {
                                if (IsBacking == false)
                                {
                                    IsBacking = true;
                                    //anim.SetBool("Idle", true);
                                    backNestState._target_position.Push(currentWayPoint.gameObject);
                                }
                                //       Debug.Log("Back");
                                currentState = backNestState;
                            }
                            else
                            {
                                //     Debug.Log("Patrol");
                                currentState = patrolState;
                            }
                        }
                    }
                }
            }
       //     Debug.Log(currentState);
            currentState.UpdateState();
            freezer++;
        }

    }

    int freezer = 0;
    IEnumerator DeFreeze()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (freezer < 0)
            {
                currentState = patrolState;
                if (freezer < -10)
                {
                    currentState = idleState;
                }
            }
            freezer--;
        }
    }

    IEnumerator DecreaseStamina()
    {
        // suspend execution for 5 seconds
        while (true)
        {
            yield return new WaitForSeconds(1);
            stamina -= 1;
        }
    }

    IEnumerator IncreaseStamina()
    {
        // suspend execution for 5 seconds
        while (true)
        {
            yield return new WaitForSeconds(1);
            stamina += 5;
        }
    }

    public void UpStamina()
    {
        StopCoroutine("DecreaseStamina");
        StartCoroutine("IncreaseStamina");
    }

    public void DownStamina()
    {
        StartCoroutine("DecreaseStamina");
        StopCoroutine("IncreaseStamina");
    }
    // Update is called once per frame



    private Quaternion _starting_Angle = Quaternion.AngleAxis(-40, Vector3.up);
    private Quaternion _step_Angle = Quaternion.AngleAxis(5, Vector3.up);

    Vector3 previousTargetPost;

    public void Set_Present_Waypoint(Transform closer_zone)
    {
        present_waypoint = closer_zone;
    }

    public int GetSock()
    {
        return birdCollider.sock;
    }

    public int GetSockTerrier()
    {
        return birdCollider.terrier;
    }

    public string GetSockType()
    {
        return birdCollider.type;
    }

    public void ResetSock()
    {
        birdCollider.sock = -1;
    }

    public void SetRest(bool _rest)
    {
        rest = _rest;
    }
    
}
