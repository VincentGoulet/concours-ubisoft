﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Nest : MonoBehaviour
{

    // Use this for initialization
    public GameObject owner;
    public SpawnManager spawnManager;

    public Stack<string> inventory;
    public Stack<int> terrier;

    [HideInInspector]
    public GameObject target;

    public float deSpawTimer = 5f;
    public float timer = 0.0f;
    public int capacity = 10;
    private StatePatternBird _bird;
    private bool startTimer = false;

    private GameObject FlyingWaypoints;
    public WayPoint nearestWaypoint;

    public void Awake()
    {
        obj_dict = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        inventory = new Stack<string>();
        terrier = new Stack<int>();
    }

    public void Start()
    {
        _bird = owner.transform.parent.GetComponent<StatePatternBird>();
        FlyingWaypoints = GameObject.Find("FlyingWaypoints");
        SetNearestWayPoint();
        spawnManager = obj_dict.Get_Spawn_Manager();
        spawn_points = obj_dict.Get_Spawn_Points();
    }

    public void SetNearestWayPoint()
    {
        Transform nearestWaypointObj = FlyingWaypoints.transform.GetChild(0);
        foreach (Transform child in FlyingWaypoints.transform)
        {
            if (Vector3.Distance(transform.position, child.position) <= Vector3.Distance(transform.position, nearestWaypointObj.position))
            {
                nearestWaypointObj = child;
            }
        }
        nearestWaypoint = nearestWaypointObj.GetComponent<WayPoint>();
        nearestWaypoint.nest = this.transform.gameObject;
        nearestWaypoint.IsNested = true;
    }

    public void Update()
    {
        if (_bird.IsBack == true)
        {
            
        }

        if (startTimer == true)
        {
            if (timer >= deSpawTimer)
            {
                DeSpawnInventory();
                timer = 0;
                startTimer = false;
            }
            timer += Time.deltaTime;
        }
    }

    public void Set_Terrier(string other, Spawn_Dictionnary dict, string signe)
    {
        if (signe == "positif")
        {
            switch (other)
            {
                case "Verte":
                    dict.sock_counterF += 1;
                    break;
                case "Jaune":
                    dict.sock_counterM += 1;
                    break;
                case "Rouge":
                    dict.sock_counterH += 1;
                    break;
                case "Mauve":
                    break;
                case "orange":
                    break;
                case "doré":
                    break;
                default:
                    print("You have a bad name for your prefab");
                    break;
            }
        }
        if (signe == "negatif")
        {
            switch (other)
            {
                case "Verte":
                    dict.sock_counterF -= 1;
                    break;
                case "Jaune":
                    dict.sock_counterM -= 1;
                    break;
                case "Rouge":
                    dict.sock_counterH -= 1;
                    break;
                case "Mauve":
                    break;
                case "orange":
                    break;
                case "doré":
                    break;
                default:
                    print("You have a bad name for your prefab");
                    break;
            }
        }


    }

    public Spawn_Dictionnary spawnDictionnary;
    public GameObject[] spawn_points;
    public Object_Dictionary obj_dict;

    public void DeSpawnInventory()
    {
        spawnDictionnary = spawn_points[terrier.Peek()].GetComponent<Spawn_Dictionnary>();
        Set_Terrier(inventory.Peek(), spawnDictionnary, "negatif");
        inventory.Pop();
        terrier.Pop();

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hammer")
        {
            if (inventory.Count != 0)
            {
                GameObject sock = spawnManager.sock_type(inventory.Peek());
                GameObject newSock = Instantiate(sock, transform.GetChild(2).transform.position, Quaternion.identity) as GameObject;
                inventory.Pop();
                terrier.Pop();
                timer = 0;
                startTimer = false;
            }
            // TODO KILL SOCK
            //  newSock.GetComponent<Socks_Controller>().is_alive = false;
        }
        if (other.gameObject.name == owner.name)
        {
            if (inventory.Count < capacity && _bird.has_socks == true)
            {
                inventory.Push(_bird.GetSockType());
                terrier.Push(_bird.GetSockTerrier());
                _bird.ResetSock();
                _bird.has_socks = false;
                _bird.anim.SetBool("HasSock", false);
                startTimer = true;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == owner.name)
        {
            _bird.IsBack = false;
            _bird.patrolState._target_position.Pop();
            _bird.patrolState.SetNearestWayPoint();
        }
    }

    public bool IsEmpty()
    {
        if (inventory.Count == 0)
            return true;
        return false;
    }

    public bool IsFull()
    {

        if (inventory.Count >= capacity)
        {
            return true;

        }
        return false;
    }
}
