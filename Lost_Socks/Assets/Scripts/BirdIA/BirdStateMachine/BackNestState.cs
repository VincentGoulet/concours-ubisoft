﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackNestState : IBirdState
{

    private readonly StatePatternBird _bird;
    public Vector3 targetPosition;
    public Stack<GameObject> _target_position;
    public GameObject nestTarget;

    public void UpdateState()
    {
        BackToNest();
    }

    public void ToIdleState()
    {

    }
    public void OnCollisionEnter(Collision collision)
    {

    }

    public BackNestState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
        nestTarget = _bird.nestTarget;
        _target_position = new Stack<GameObject>();
        FlyingWaypoints = GameObject.Find("FlyingWaypoints");
    }

    public void ToPatrolState()
    {

    }
    public void ToAttackState()
    {

    }

    public void ToBackNestState()
    {

    }

    public bool IsGoingToNest = false;

    private void BackToNest()
    {
        if (IsGoingToNest == true)
            GoToNest();
        else
        {
            if (_target_position.Count == 0)
                GetNearestWayPointFromNest();
            if (Vector3.Distance(_bird.transform.position, _target_position.Peek().transform.position) <= 1f)
            {
                if (_bird.currentWayPoint.IsNested == true && _bird.currentWayPoint.nest.transform.GetChild(0).position == nestTarget.transform.position)
                {
                    IsGoingToNest = true;
                    _target_position.Push(_bird.nestTarget);
                }
                else
                {
                    _bird.currentWayPoint = _target_position.Peek().GetComponent<WayPoint>();
                    _target_position.Pop();
                }
            }
            else
            {
                var q = Quaternion.LookRotation(_target_position.Peek().transform.position - _bird.transform.position);
                q.x = 0;
                q.z = 0;
                _bird.transform.rotation = Quaternion.Slerp(_bird.transform.rotation, q, _bird.speed * Time.deltaTime);
                _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, _target_position.Peek().transform.position, _bird.speed * Time.deltaTime);
            }
        }
    }
    /*
 if (IsGoingToNest == true)
            GoToNest();
        else
        {
            if (_target_position.Count == 0)
                GetNearestWayPointFromNest();
            if (Vector3.Distance(_bird.transform.position, _target_position.Peek().transform.position) <= 1f)
            {
                if (_bird.currentWayPoint.IsNested == true && _bird.currentWayPoint.nest.transform.GetChild(0).position == nestTarget.transform.position)
                {
                    IsGoingToNest = true;
                    _target_position.Push(_bird.nestTarget);
                }
                _bird.currentWayPoint = _target_position.Peek().GetComponent<WayPoint>();
                _target_position.Pop();
            }
            else
            {
                var q = Quaternion.LookRotation(_target_position.Peek().transform.position - _bird.transform.position);
                q.x = 0;
                q.z = 0;
                _bird.transform.rotation = Quaternion.Slerp(_bird.transform.rotation, q, _bird.speed * Time.deltaTime);
                _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, _target_position.Peek().transform.position, _bird.speed * Time.deltaTime);
            }
        }
    */


    void GoToNest()
    {
        if (Vector3.Distance(_bird.transform.position, _bird.nestTarget.transform.position) <= 0.1f)
        {
            _bird.IsBacking = false;
            _bird.IsBack = true;
            _bird.restState.StartC();
            if (_target_position.Count > 0)
                _target_position.Pop();
        }
        else
        {
            var q = Quaternion.LookRotation(_bird.nestTarget.transform.position - _bird.transform.position);
            q.x = 0;
            q.z = 0;
            _bird.transform.rotation = Quaternion.Slerp(_bird.transform.rotation, q, _bird.speed * Time.deltaTime);
            _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, _bird.nestTarget.transform.position, _bird.speed * Time.deltaTime);
        }
    }

    void GetNearestWayPointFromNest()
    {
        WayPoint wayPoint = _bird.currentWayPoint;
        GameObject target = wayPoint.gameObjectWayPoints[0];

        foreach (GameObject point in wayPoint.gameObjectWayPoints)
        {
            if (Vector3.Distance(point.transform.position, nestTarget.transform.position) < Vector3.Distance(target.transform.position, nestTarget.transform.position))
                target = point;
        }
        _target_position.Push(target);
    }

    private GameObject FlyingWaypoints;

    public void SetNearestWayPoint()
    {
        Transform nearestWaypointObj = FlyingWaypoints.transform.GetChild(0);
        foreach (Transform child in FlyingWaypoints.transform)
        {
            if (Vector3.Distance(_bird.transform.position, child.position) < Vector3.Distance(_bird.transform.position, nearestWaypointObj.position))
                nearestWaypointObj = child;
        }
        _target_position.Push(nearestWaypointObj.gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {

    }
    public void ToRestState()
    {
    }

    public void ToChaseSate()
    {
    }

    public void OnTriggerExit(Collider other)
    {
    }
}
