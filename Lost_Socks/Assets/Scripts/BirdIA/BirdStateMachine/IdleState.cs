﻿using UnityEngine;
using System.Collections;

public class IdleState : IBirdState
{

    private readonly StatePatternBird _bird;
    private float timerMax = 5f;
    private float timer = 0;


    public void UpdateState()
    {
        if (timer > timerMax)
        {
            timer = 0f;
            _bird.IsIdle = false;
            //if (Vector3.Distance(_bird.transform.position, _bird.nestTarget.transform.position) <= 0)
             //   _bird.IsBack = true;
           // _bird.backNestState.targetPosition = _bird.nestObject.transform.GetChild(0).transform.position;
            _bird.anim.SetBool("Idle", false);
            //Debug.Log("end");
            if (_bird.backNestState._target_position.Count > 0)
                _bird.backNestState._target_position.Pop();
            else
                _bird.backNestState.SetNearestWayPoint();
      //      _bird.currentState = _bird.backNestState;
        }
        else
            timer += Time.deltaTime;
    }

    public IdleState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
    }

    public void OnTriggerEnter(Collider other)
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {

    }

    public void ToPatrolState()
    {
        _bird.isLanded = false;
        _bird.currentState = _bird.patrolState;
    }

    public void ToAttackState()
    {

    }

    public void ToBackNestState()
    {
        _bird.isLanded = false;
        _bird._target = GameObject.Find("Nest/Target");
        _bird.currentState = _bird.backNestState;
    }

    public void ToRestState()
    {

    }

    public void ToIdleState()
    {

    }

    public void ToChaseSate()
    {
    }

    public void OnTriggerExit(Collider other)
    {
    }
}
