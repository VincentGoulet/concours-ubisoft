﻿using UnityEngine;
using System.Collections;
using System;

public class AttackState : IBirdState
{


    private readonly StatePatternBird _bird;
    public Vector3 target_position;

    public AttackState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
      
        
        
    }

    

    public void ToIdleState()
    {
        _bird.isLanded = true;
        _bird.currentState = _bird.idleState;
    }

    public void UpdateState()
    {
        /*Debug.Log(_bird.PlayMusic);
        if (_bird.currentState == _bird.attackState && !_bird.PlayMusic)
        {
            _bird.PlayMusic = true;
            StartCoroutine(_bird.sounds.play_birdMusic(_bird.gameObject));
        }*/
        if ((target_position - _bird.transform.position) == Vector3.zero)
        {
            _bird.currentState = _bird.patrolState;
            _bird.PlayMusic = false;
        }
        else
            Chase();
    }

 

    public void OnTriggerEnter(Collider other)
    {


    }
    public void OnCollisionEnter(Collision collision)
    {

    }
    public void ToBackNestState()
    {

    }
    public void ToRestState()
    {

    }
    public void ToPatrolState()
    {

    }

    public void ToAttackState()
    {
    }


    private GameObject wall;

    private void Chase()
    {
        var q = Quaternion.LookRotation(target_position - _bird.transform.position);
        q.x = 0;
        q.z = 0;
        _bird.transform.rotation = Quaternion.Slerp(_bird.transform.rotation, q, _bird.speed * Time.deltaTime);
        _bird.transform.position = Vector3.MoveTowards(_bird.transform.position, target_position, _bird.speed * Time.deltaTime);
    }

    private void Attack()
    {

    }

    public void ToChaseSate()
    {
    }

    public void OnTriggerExit(Collider other)
    {
    }
}
