﻿using UnityEngine;
using System.Collections;

public interface IBirdState{

    void UpdateState();
    void OnTriggerEnter(Collider other);
    void OnCollisionEnter(Collision collision);
    void ToPatrolState();
    void ToAttackState();

    void ToBackNestState();
    void ToRestState();
    void ToIdleState();
    void ToChaseSate();
    void OnTriggerExit(Collider other);


}
