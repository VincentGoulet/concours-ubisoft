﻿using UnityEngine;
using System.Collections;
using System;

public class AttackPlayerState : IBirdState {


    private readonly StatePatternBird _bird;

    public float attackCoolDown = 2f;
    public float attackTime = 2f;
    Player_Health vie;
    private GameObject _player;

    public bool attacking = false;
    public AttackPlayerState(StatePatternBird statePatternBird)
    {
        _bird = statePatternBird;
        _player = GameObject.Find("Player");
        vie = _player.GetComponent<Player_Health>();

    }

    public void UpdateState()
    {
        if (attacking)
        {
            
            if (attackTime >= attackCoolDown)
            {
                _bird.sounds.play_birdAttack(_bird.gameObject.GetComponent<AudioSource>());
                attackTime = 0f;
                vie.TakeDamage(0.5f);
            }
            attackTime += Time.deltaTime;
        }
        

    }

    private void StartCoroutine(IEnumerator enumerator)
    {
        throw new NotImplementedException();
    }

    public void OnCollisionEnter(Collision collision)
    {
    }

    public void OnTriggerEnter(Collider other)
    {
    }

    public void OnTriggerExit(Collider other)
    {
    }

    public void ToAttackState()
    {
    }

    public void ToBackNestState()
    {
    }

    public void ToChaseSate()
    {
    }

    public void ToIdleState()
    {
    }

    public void ToPatrolState()
    {
    }

    public void ToRestState()
    {
    }


}
