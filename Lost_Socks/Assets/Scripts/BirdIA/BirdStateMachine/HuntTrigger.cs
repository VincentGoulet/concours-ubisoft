﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HuntTrigger : MonoBehaviour {

    //[HideInInspector]
    public List<GameObject> inventory_socks;
    private StatePatternBird _bird;


    void Awake()
    {
        inventory_socks = new List<GameObject>();
    }

    // Use this for initialization
    void Start () {
        _bird = this.transform.parent.GetComponent<StatePatternBird>();

    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            if (other.GetComponent<Socks_Controller>().type == "Yellow" || other.GetComponent<Socks_Controller>().type == "Red" || other.GetComponent<Socks_Controller>().type == "Green")
            {
                inventory_socks.Add(other.gameObject);
            }

        }
        if (other.gameObject.tag == "Spray")
        {
            _bird.anger += 5;
            if (_bird.anger >= _bird.angerMax)
            {
                _bird.IsAngry = true;
                _bird.grounded.SetActive(true);
                //  _bird.attack.SetActive(true);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Socks")
        {
            if (other.GetComponent<Socks_Controller>().type == "Yellow" || other.GetComponent<Socks_Controller>().type == "Red" || other.GetComponent<Socks_Controller>().type == "Green")
            {
                inventory_socks.Remove(other.gameObject);
            }
        }
    }

    public bool IsInventorySocksEmpty()
    {
        if (inventory_socks.Count == 0)
            return true;
        return false;
    }
    public void IncAnger(GameObject sock)
    {
    }

    void myprint()
    {
        inventory_socks.ForEach(delegate (GameObject t)
        {
        });
    }
}
