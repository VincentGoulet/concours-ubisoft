﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour
{
    private StatePatternBird _bird;
    public float attackCoolDown = 2f;
    public float attackTime = 2f;
    Player_Health vie;
    private GameObject _player;
    int attackHash;

    float distance = 10f;
    // Use this for initialization
    void Start()
    {
        _bird = this.transform.parent.GetComponent<StatePatternBird>();
        _player = GameObject.Find("Player");
        vie = _player.GetComponent<Player_Health>();
        attackHash = Animator.StringToHash("AttackPlayer");

    }

    // Update is called once per frame
    void Update()
    {
        if (_bird.IsGrounded && _bird.IsStunned == false)
        {

            attackTime += Time.deltaTime;
        }
        
        if (playerout == true)
        {
            _bird.IsAttacking = false;
            _bird.navMeshAgent.enabled = true;
            _bird.navMeshAgent.ResetPath();
            _bird.navMeshAgent.SetDestination(_player.transform.position);
            _bird.currentState = _bird.chaseState;
            attackTime = 2f;
            playerout = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && _bird.IsStunned == false)
        {
            if (_bird.IsGrounded)
            {
                _bird.IsAttacking = true;
                _bird.navMeshAgent.Stop();
                _bird.navMeshAgent.enabled = false;
                distance = Vector3.Distance(_bird.transform.position, _player.transform.position);
                

                //Debug.Log("Attack");
            }
        }
        
    }
    

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (_bird.IsGrounded && _bird.IsStunned == false)
            {
                if (_bird.IsAttacking)
                {
                    if (attackTime >= attackCoolDown)
                    {
                        _bird.sounds.play_birdAttack(_bird.gameObject.GetComponent<AudioSource>());
                        _bird.anim.SetTrigger(attackHash);
                        StartCoroutine(WaitForAttack(_bird.anim.GetCurrentAnimatorStateInfo(0).length - 0.2f));
                        
                        
                        attackTime = 0f;
                    }
                }
            }
        }
    }

    IEnumerator WaitForAttack(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        vie.TakeDamage(1f);
    }

    public bool playerout = false;
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (_bird.IsGrounded)
            {
                if (_bird.IsStunned == false)
                {
                    _bird.IsAttacking = false;
                    _bird.navMeshAgent.enabled = true;
                    _bird.navMeshAgent.ResetPath();
                    _bird.navMeshAgent.SetDestination(_player.transform.position);
                    _bird.currentState = _bird.chaseState;
                    attackTime = 2f;
                }
                else
                    playerout = true;
            }
        }

    }
}
