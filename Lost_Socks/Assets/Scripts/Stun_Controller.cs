﻿using UnityEngine;
using System.Collections;

public class Stun_Controller : MonoBehaviour {

    private Collider sphere_collider;
    Vector3 max_scale = Vector3.one;
    Vector3 min_scale = Vector3.zero;

    // Use this for initialization
    void Start ()
    {
        sphere_collider = GetComponent<SphereCollider>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}
    IEnumerator Stun()
    {
        float pop_time = 0.2f;

        transform.localScale = max_scale / 10.0f;
        float timer = 0.0f;

        while (timer < pop_time)
        {
            yield return null;
            transform.localScale = Vector3.Lerp(min_scale, max_scale, timer / pop_time);
            timer += Time.deltaTime;
        }

        transform.localScale = max_scale;
        yield return null;
        transform.localScale = min_scale;
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.tag == "Socks")
        {
            other.GetComponent<Socks_Controller>().Pop_And_Stun();
        }
    }
}
