﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Zone_Controller : MonoBehaviour
{
    public bool hideout_zone;
    public bool is_underwater;
    public bool player_zone;
    public bool cave_zone;

    public float zone_size;

    public List<Transform> zone_connected = new List<Transform>();

    public Transform hideout_transform;

    public int socks_default_value;
    public int birds_default_value;

    public int security_value = 1;
    public int socks_variable_value = 0;
    public int birds_variable_value = 0;

    private int fruit_number = 0;
    private int index;

    // Use this for initialization
    void Start()
    {
        zone_size = GetComponent<SphereCollider>().radius;
    }

    public void Setup(int i)
    {
        index = i;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Initiate_Birds_Value()
    {
        for (int i = 0; i < zone_connected.Count; i++)
        {
            Zone_Controller other_zone_controller = zone_connected[i].GetComponent<Zone_Controller>();
            if (other_zone_controller.birds_default_value < birds_default_value - 1)
            {
                if (other_zone_controller.birds_default_value >= 0)
                {
                    other_zone_controller.birds_default_value = birds_default_value - 1;
                    other_zone_controller.Initiate_Birds_Value();
                }
                if (other_zone_controller.birds_default_value < 1)
                {
                    other_zone_controller.birds_default_value = 1;
                }
            }
        }
    }

    public void Initiate_Security_Value()
    {
        for (int i = 0; i < zone_connected.Count; i++)
        {
            Zone_Controller other_zone_controller = zone_connected[i].GetComponent<Zone_Controller>();
            if (player_zone)
            {
                other_zone_controller.security_value -= 3;
            }
            if (hideout_zone)
            {
                other_zone_controller.security_value = security_value - 4;
                other_zone_controller.Initiate_Security_Value();
            }
            else if (other_zone_controller.security_value < security_value - 1)
            {
                if (other_zone_controller.security_value > 1)
                {
                    other_zone_controller.security_value = security_value - 1;
                    other_zone_controller.Initiate_Security_Value();
                }
                if (other_zone_controller.security_value <= 1)
                {
                    other_zone_controller.security_value = 1;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        GameObject object_detected = other.gameObject;
        switch (object_detected.tag)
        {
            case "Socks":
                Socks_Controller socks_controller = object_detected.GetComponent<Socks_Controller>();
                socks_variable_value += 1;
                birds_variable_value += 2;
                socks_controller.present_zone = gameObject.transform;
                if (socks_controller.state == Socks_Controller.State.is_fleeing)
                {
                    if (hideout_zone)
                    {
                        print(hideout_transform);
                        socks_controller.nav.SetDestination(hideout_transform.position);
                    }
                }
                break;

            case "Fruits":
                Add_Fruits();
                break;

            case "Birds":
                object_detected.GetComponent<StatePatternBird>().present_waypoint = gameObject.transform;
                socks_variable_value -= 2;
                birds_variable_value -= 1;
                break;

            case "Player":
                player_zone = true;
                security_value -= 5;
                Initiate_Security_Value();
                object_detected.GetComponent<Player_Movement>().present_zone = gameObject.transform;
                break;

           /*
            case "Terrier":
                hideout_zone = true;
                hideout_transform = object_detected.transform.FindChild("Zone_entree");
                break;
               */
        }
    }

    void OnTriggerExit(Collider other)
    {
        GameObject object_detected = other.gameObject;
        switch (object_detected.tag)
        {
            case "Socks":
                Del_Socks();
                break;
            case "Fruits":
                Del_Fruits();
                break;
            case "Birds":
                socks_variable_value += 2;
                birds_variable_value += 1;
                break;
            case "Player":
                security_value += 5;
                player_zone = false;
                for (int i = 0; i < zone_connected.Count; i++)
                {
                    zone_connected[i].GetComponent<Zone_Controller>().security_value += 3;
                }
                break;
        }
    }
    public void Del_Socks()
    {
        socks_variable_value -= 1;
        birds_variable_value -= 2;
    }
    void Add_Fruits()
    {
        socks_variable_value += 3;
        for (int i = 0; i < zone_connected.Count; i++)
        {
            zone_connected[i].GetComponent<Zone_Controller>().socks_variable_value += 1;
        }
    }
    void Del_Fruits()
    {
        socks_variable_value -= 3;
        for (int i = 0; i < zone_connected.Count; i++)
        {
            zone_connected[i].GetComponent<Zone_Controller>().socks_variable_value -= 1;
        }
    }
    /*
	Vector3 Get_Fruit_Position()
	{
        return fruit_position;
	}
    */
    public int Get_Security_Value()
    {
        if (security_value < 1)
        {
            return 1;
        }
        else
        {
            return security_value;
        }
    }

    public int Get_Socks_Value()
    {
        int socks_added_values = 0;
        socks_added_values = socks_default_value + socks_variable_value;
        if (hideout_zone)
        {
            socks_added_values += 2;
        }

        if (socks_added_values < 1)
        {
            return 1;
        }
        return socks_added_values;
    }

    public int Get_Birds_Value()
    {
        int birds_added_values = 0;
        birds_added_values = birds_default_value + birds_variable_value;
        if (birds_added_values < 1)
        {
            return 1;
        }
        return birds_added_values;
    }
}