﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player_Health : MonoBehaviour
{
    public int starting_health = 12;
    public float current_health;
    private Image health_slider1;
    private Image health_slider2;
    private Image health_slider3;
    private Image damage_image;
    
    public float flash_speed = 5f;
    public Color flash_colour = new Color(1f, 0f, 0f, 0.1f);
    
    Animator anim;
    private DoneHashIDs hash;
    
    Player_Movement player_movement;
    bool isDead;
    bool damaged;
    bool health;

    Object_Dictionary dictionary;
    SoundManager sounds;

    void Awake()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();

        //anim = GetComponent<Animator> ();
        try
        {
            damage_image = GameObject.Find("DamageImage").GetComponent<Image>();
        }
        catch
        {
            throw new System.Exception("Il manque le DamageImage");
        }
        health_slider1 = GameObject.Find("HealthSlider1").GetComponent<Image>();
        health_slider2 = GameObject.Find("HealthSlider2").GetComponent<Image>();
        health_slider3 = GameObject.Find("HealthSlider3").GetComponent<Image>();
        player_movement = GetComponent<Player_Movement> ();
        health_slider1.fillAmount = 4;
        health_slider2.fillAmount = 4;
        health_slider3.fillAmount = 4;

        current_health = starting_health;
    }
    void Start()
    {
        anim = GetComponent<Animator>();
        hash = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneHashIDs>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    void Update()
    {
        if(damaged)
        {
            damage_image.color = flash_colour;
        }
        else
        {
            damage_image.color = Color.Lerp (damage_image.color, Color.clear, flash_speed* Time.deltaTime);
        }
        damaged = false;
    }


    public void TakeDamage(float amount)
    {
        sounds.play_playerHurt();
        damaged = true;
        Set_Health(current_health - amount);
    }


    public void Death()
    {
        sounds.play_playerDie();
        anim.SetTrigger ("Dead");
        player_movement.enabled = false;
        dictionary.Get_UI_Manager().Toggle("gameover");
   }

   public void RestartLevel()
   {
   }

    public void Take_Health(float amount)
    {
        Set_Health(current_health + amount);
    }


   public void Set_Health(float amount)
   {
        current_health = (int)Mathf.Round(Mathf.Clamp(amount, 0, 12));
        if (current_health <= 4)
        {
            health_slider1.fillAmount = (current_health) / 4f;
            health_slider2.fillAmount = 0;
            health_slider3.fillAmount = 0;
        }
        if (current_health <= 8 && (current_health > 4))
        {
            health_slider1.fillAmount = 1;
            health_slider2.fillAmount = (current_health - 4) / 4f;
            health_slider3.fillAmount = 0;
        }
        if (current_health <= 12 && (current_health > 8))
        {
            health_slider1.fillAmount = 1;
            health_slider2.fillAmount = 1;
            health_slider3.fillAmount = (current_health - 8) / 4f;
        }

        if (current_health == 0 && !isDead)
        {
            Death();
        }
    }
}