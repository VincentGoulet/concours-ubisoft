﻿using UnityEngine;
using System.Collections;

public class Shader_Changer : MonoBehaviour {
    Fruit_Controller fruit;
    Material material;
    Color base_color;
	// Use this for initialization
	void Start ()
    {
        fruit = GetComponentInParent<Fruit_Controller>();
        material = GetComponent<MeshRenderer>().material;
        base_color = new Color(195, 197, 195, 255);
        //material.color = base_color;


    }
	
	// Update is called once per frame
	void Update ()
    {
        if (fruit.is_Poisoned)
        {
            StartCoroutine(fruit_poisColor(fruit.is_Poisoned));
        }
        if (!fruit.is_Poisoned)
        {
            StopCoroutine(fruit_poisColor(fruit.is_Poisoned));
            
        }
	
	}
    IEnumerator fruit_poisColor(bool is_true)
    {
        if (is_true)
        {
            material.color = new Color(0, 255, 2, 255);
        }
        if (!is_true)
        {
            material.color = base_color;
        }
        
        yield return null;
    }
}
