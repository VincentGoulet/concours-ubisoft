﻿using UnityEngine;
using System.Collections;

public class Rock_Controller : MonoBehaviour {
    GameObject particles_rock;
    
    GameObject rocklet;

    SoundManager sounds;
    AudioSource mysource;


    // Use this for initialization
    void Start () {
        
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        particles_rock = (GameObject)Resources.Load("Prefabs/Particles/Particles_Rock", typeof(GameObject));
        rocklet = (GameObject)Resources.Load("Prefabs/Particles/Rocklet", typeof(GameObject));
        mysource = GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    private float fade_time = 5f;

    public IEnumerator Crash_Rock()
    {
        //Pop_Rocklets();
        
        GameObject particles = (GameObject)Instantiate(particles_rock, transform.position, Quaternion.identity);
        //particles.transform.parent = transform;
        //transform.localScale = transform.localScale * transform.parent.localScale.x;
        /**float alpha = 1;
        float timer = 0.0f;*/

        /*while (timer < fade_time)
        {
            yield return null;
            print("in while");
            alpha = Mathf.Lerp(1, 0, timer / fade_time);
            print(alpha);
            Utilities.Set_Transparency(renderer.material, alpha);
            timer += Time.deltaTime;

            //print(timer);
            //print(fade_time);
        }*/
        yield return null;
        if(gameObject != null)
        {
            Destroy(gameObject);
        }
        yield return new WaitForSeconds(0.1f);
        sounds.play_rocks_destroy(mysource);
    }

    int number_rocklets = 4;
    float rocklet_speed = 20;
    float rocklet_rotation = 5;
    void Pop_Rocklets()
    {
        Vector3 direction;
        for (int i = 0; i < number_rocklets; i++)
        {
            float random = Random.value * 2 * Mathf.PI;
            direction = new Vector3(Mathf.Cos(random), Mathf.Sin(Mathf.PI / 3 * 2), Mathf.Sin(random));
            direction = new Vector3(direction.x, Mathf.Abs(direction.y), direction.z);
            GameObject rocklet_clone = (GameObject)Instantiate(rocklet, transform.position + direction * 4, Quaternion.identity);
            Rigidbody rb = rocklet.GetComponent<Rigidbody>();
            rb.velocity = direction * rocklet_speed;
            rb.angularVelocity = Random.insideUnitSphere * rocklet_rotation;
        }
    }
}
