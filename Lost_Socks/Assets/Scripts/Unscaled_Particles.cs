﻿using UnityEngine;
using System.Collections;

public class Unscaled_Particles : MonoBehaviour {
    ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.timeScale < 0.01f)
        {
            ps.Simulate(Time.unscaledDeltaTime, true, false);
        }
    }
}
