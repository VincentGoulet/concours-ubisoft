﻿using UnityEngine;
using System.Collections;

public class End_Trigger : MonoBehaviour {
    Game_Controller game_controller;

    // Use this for initialization
    void Start () {
        game_controller = GameObject.Find("Game Controller").GetComponent<Game_Controller>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
        if (other.tag == "Player")
        {
            if (game_controller.is_end)
            {
                game_controller.End_Signal();
            }
        }
	}
}
